﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL;
using bdmirror24.DAL.DAL_Functions.Implementations;
using bdmirror24.DAL.mappings;
using System.Collections;
using bdmirror24.couchbase;
namespace bdmirror24
{
    public partial class _default : System.Web.UI.Page
    {
        private Utility obutility = new Utility();
        private static int i;
        public static string language;
        public void load_mostread()
        {
            string query =(language=="en")? @"select news.id from news where published='1' and title_en!=''  ORDER BY mostread desc LIMIT 0,5":
                 @"select news.id from news where published='1' and title_bn!='' ORDER BY mostread desc LIMIT 0,5";
            DataTable dtTOp = new DataTable("dssmostread");
            dtTOp.Columns.Add("title_en", typeof(string));
            dtTOp.Columns.Add("title_bn", typeof(string));
            dtTOp.Columns.Add("news_id", typeof(string));
            dtTOp.Columns.Add("name_en", typeof(string));
            dtTOp.Columns.Add("name_bn", typeof(string));
            dtTOp.Columns.Add("content_en", typeof(string));
            dtTOp.Columns.Add("content_bn", typeof(string));
            dtTOp.Columns.Add("img_url", typeof(string));
            dtTOp.Columns.Add("img_text", typeof(string));
            IList<int> ids = ndao.GetCategoryNews(query);
            IList<bdmirrorDB.news> NEWZ = getIlistNews(ids);
            foreach (bdmirrorDB.news nz in NEWZ)
            {
                bdmirrorDB.images img = get_img(nz);
                dtTOp.Rows.Add(new string[] { nz.title_en, nz.title_bn, nz.news_id.ToString(), nz.category_id.name_en, nz.category_id.name_bn, nz.content_en, nz.content_bn,img.link,img.caption_en });
            }
            rptmostread.DataSource = dtTOp;
            rptmostread.DataBind();
        }

        public void rpt_larstBind()
        {
            IList<bdmirrorDB.news> lstLatest = new NewsDAO().GetLatestNews();
            DataTable dtcat = new DataTable("dtlatest");
            dtcat.Columns.Add("title_en");
            dtcat.Columns.Add("title_bn");
            dtcat.Columns.Add("id");
            dtcat.Columns.Add("name_bn");
            dtcat.Columns.Add("name_en");
            DataTable dtTOp = new DataTable("dsslatest2");
            dtTOp.Columns.Add("title_en", typeof(string));
            dtTOp.Columns.Add("title_bn", typeof(string));
            dtTOp.Columns.Add("news_id", typeof(string));
            dtTOp.Columns.Add("name_en", typeof(string));
            dtTOp.Columns.Add("name_bn", typeof(string));
            dtTOp.Columns.Add("img_url", typeof(string));
            dtTOp.Columns.Add("img_text", typeof(string));
            dtTOp.Columns.Add("content_en", typeof(string));
            dtTOp.Columns.Add("content_bn", typeof(string));
            //IList<bdmirrorDB.categories> allcat = new CategoryDAO().All();
            
            IList<int> latestNews = (language == "bn") ?
                ndao.GetCategoryNews(@"SELECT id from news where title_en!='' order by  date desc limit 0,5") :
                ndao.GetCategoryNews(@"SELECT id from news where title_bn!='' order by  date desc limit 0,5");
            IList<bdmirrorDB.news> NEWZ = getIlistNews(latestNews);
            foreach(bdmirrorDB.news nz in NEWZ)
            {
                bdmirrorDB.images img = get_img(nz);
                dtTOp.Rows.Add(new string[] { nz.title_en, nz.title_bn, nz.news_id.ToString(), nz.category_id.name_en, nz.category_id.name_bn, img.link, img.caption_en, nz.content_en, nz.content_bn });

            }

            rptLatestRight.DataSource = dtTOp;
            rptLatestRight.DataBind();
            foreach (bdmirrorDB.news nz in lstLatest)
            {
                
                dtcat.Rows.Add(new string[] { nz.title_en, nz.title_bn,nz.news_id.ToString(),nz.category_id.name_bn,nz.category_id.name_en });
            }
            rptlatest.DataSource = dtcat;
            rptlatest.DataBind();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //new test().insert_cat("saad");
            //Response.Write(new test().getcat_test("name_en","saad").cat_id);
            if (!IsPostBack)
            {
                if (Request.QueryString["lang"] == null)
                {
                    Session.Add("lang", "en");
                    language = "en";
                    lnklang.Text = " English Version ";
                    lnklang.Font.Size = new FontUnit(12);
                   // lnklang.NavigateUrl = "http://localhost:1316.default.aspx?lang=bn";
                }
                if (Request.QueryString["lang"] != "bn")
                {
                    language = "en";
                    lnklang.Font.Size = new FontUnit(10);

                    lnklang.Text = " English Version ";
                     //lnklang.NavigateUrl = "http://localhost:1316.default.aspx?lang=bn";
                }
                else
                {
                    Session["lang"] = "bn";
                    lnklang.Text = " বাংলা সংস্করণ ";
                    lnklang.Font.Size = new FontUnit(12);

                    language = "bn";
                    //lnklang.NavigateUrl = "http://localhost:1316.default.aspx?lang=en";
                }
                carousel_image_load();

                DataTable dtcat = new DataTable("dtcat");
                dtcat.Columns.Add("name_en");
                dtcat.Columns.Add("name_bn");
                IList<bdmirrorDB.categories> allcat = new CategoryDAO().All();
                foreach (bdmirrorDB.categories cat in allcat)
                {
                    dtcat.Rows.Add(new string[] { cat.name_en, cat.name_bn });
                }
                rptcat.DataSource = dtcat;
                rptcat.DataBind();
                mainNewsLoad();
                rightNewsLoad();
                rpt_larstBind();
                load_mostread();
                i = 0;


                #region TopAdd
                IList<bdmirrorDB.all_add> ads = new All_addDAO().getAddbyTitle("Add. in Top");
                foreach (bdmirrorDB.all_add ad in ads)
                {
                    if (ad.addorder == 1)
                    {
                        hpladtop1.NavigateUrl = ad.image_url;
                        imgadtop1.ImageUrl = ad.image_link;
                    }
                    else
                    {
                        hpladtop2.NavigateUrl = ad.image_url;
                        imgadtop2.ImageUrl = ad.image_link;
                    }
                }
                #endregion

                #region MidAdd
                ads = new All_addDAO().getAddbyTitle("Add. in Middle(Home)");
                IList<DataTable> dts = new List<DataTable>();
                int p = 0, count = 0;
                for (int j = 0; j < 4; j++)
                {
                    DataTable dt = new DataTable("advertise" + p);
                    p++;
                    dt.Columns.Add("image_url");
                    dt.Columns.Add("image_link");
                    dts.Add(dt);

                }
                foreach (bdmirrorDB.all_add ad in ads)
                {

                    if (count < 4)
                    {
                        dts[0].Rows.Add(new string[] { ad.image_url, ad.image_link });
                    }
                    else if (count >= 4 && count < 8)
                    {
                        dts[1].Rows.Add(new string[] { ad.image_url, ad.image_link });
                    }
                    else if (count >= 8)
                    {
                        dts[2].Rows.Add(new string[] { ad.image_url, ad.image_link });
                    }
                    count++;

                }
                rptmidadd1.DataSource = dts[0];
                rptmidadd1.DataBind();
                rptmidadd2.DataSource = dts[1];
                rptmidadd2.DataBind();
                rptmidAdd3.DataSource = dts[2];
                rptmidAdd3.DataBind();
                #endregion

                #region RightAdd
                IList<bdmirrorDB.all_add> adsright = new All_addDAO().getAddbyTitle("Add. in Right Column(Home)");
                IList<DataTable> dtright = new List<DataTable>();
                count = 0;
                p = 0;
                for (int j = 0; j < 2; j++)
                {
                    DataTable dtr = new DataTable("RightAdvertise" + p);
                    p++;
                    dtr.Columns.Add("image_url");
                    dtr.Columns.Add("image_link");
                    dtright.Add(dtr);

                }
                foreach (bdmirrorDB.all_add ad in adsright)
                {
                    if (count < 5)
                    {
                        dtright[0].Rows.Add(new string[] { ad.image_url, ad.image_link });
                    }
                    else
                    {
                        dtright[1].Rows.Add(new string[] { ad.image_url, ad.image_link });
                    }

                    count++;

                }
                rptimgaddright1.DataSource = dtright[0];
                rptimgaddright1.DataBind();
                rptimgaddright2.DataSource = dtright[1];
                rptimgaddright2.DataBind();


                #endregion
                midrpt_load();


            }

        }
        protected void carousel_image_load()
        {
            string query = (language != "bn") ? @"select id from news where title_en!='' and published='1' and top_news='1' order by rand() limit 0,6;" :
                @"select id from news where title_bn!='' and published='1' and top_news='1' order by rand() limit 0,6;";
            IList<bdmirrorDB.news> topnews = getIlistNews(ndao.GetCategoryNews(query));
            DataTable dtCar = new DataTable("dtCar");
            dtCar.Columns.Add("id");
            dtCar.Columns.Add("caption_link");
            dtCar.Columns.Add("caption_text");
            dtCar.Columns.Add("img_link");
            dtCar.Columns.Add("img_text");
            foreach (bdmirrorDB.news nz in topnews)
            {
                bdmirrorDB.images img = get_img(nz);
                if(language!="bn")
                    dtCar.Rows.Add(new string[] { nz.news_id.ToString(),"~/news/"+language+"/"+nz.category_id.name_en+"/"+nz.news_id+"/"+obutility.replaceSpace(nz.title_en),nz.title_en,img.link,img.caption_en});
                else
                    dtCar.Rows.Add(new string[] { nz.news_id.ToString(), "~/news/" + language + "/" + nz.category_id.name_bn + "/" + nz.news_id + "/" + obutility.replaceSpace(nz.title_bn), nz.title_bn, img.link, img.caption_bn });
                    
            }
            rptCarousal.DataSource = dtCar;
            rptCarousal.DataBind();
        }
        protected void change_lang(object sender, EventArgs e)
        {
            if (Session["lang"] == null || Session["lang"].ToString() == "bn")
            {
                
                Response.Redirect("http://www.bdmirror24.com/default.aspx?lang=bn");
            }
            else
            {
                Response.Redirect("http://www.bdmirror24.com/default.aspx?lang=en");
            }
            //Response.Write("sess" + Session["lang"]);
        }
        ImageDAO imgdao= new ImageDAO();
        private bdmirrorDB.images get_img(bdmirrorDB.news nz)
        {
            bdmirrorDB.images img = imgdao.GetImagebyNewsID(nz);
            if (img == null)
            {
                img = new bdmirrorDB.images();
                img.link = "";
                img.caption_en = "";
            }
            return img;
        }
        protected void mainNewsLoad()
        {
            int count = 0;
            string selected_lang;
            if (language != "bn")
            {
                selected_lang = "en";
            }
            else
            {
                selected_lang = "bn";
            }
            IList<int> res = ndao.getMainNews(selected_lang);
            IList<bdmirrorDB.news> news_list = getIlistNews(res);
            IList<DataTable> dtList = new List<DataTable>(3);
            for (int i = 0; i < 3; i++)
            {
                DataTable dtable = new DataTable("mainnews" + i);
                //DataTable dt =dtList[i];
                //en-bn check,date/date link check
                dtable.Columns.Add("id");
                dtable.Columns.Add("title");
                dtable.Columns.Add("title_link");
                dtable.Columns.Add("img_url");
                dtable.Columns.Add("img_text");
                dtable.Columns.Add("content");
                dtable.Columns.Add("date_pub");
                dtable.Columns.Add("date_pub_link");
                dtList.Add(dtable);
            }
            string title_link = string.Empty;
            DataTable dtTOp = new DataTable("dsstopnews");
            dtTOp.Columns.Add("title_en", typeof(string));
            dtTOp.Columns.Add("title_bn", typeof(string));
            dtTOp.Columns.Add("news_id", typeof(string));
            dtTOp.Columns.Add("name_en", typeof(string));
            dtTOp.Columns.Add("name_bn", typeof(string));
            dtTOp.Columns.Add("img_url", typeof(string));
            dtTOp.Columns.Add("img_text", typeof(string));
            dtTOp.Columns.Add("content_en", typeof(string));
            dtTOp.Columns.Add("content_bn", typeof(string));
            int topcount = 0;
            foreach (bdmirrorDB.news nz in news_list)
            {
                topcount++;
                if (topcount > 5) break;
                bdmirrorDB.images img = get_img(nz);
                dtTOp.Rows.Add(new string[] { nz.title_en, nz.title_bn, nz.news_id.ToString(), nz.category_id.name_en, nz.category_id.name_bn,img.link,img.caption_en, nz.content_en, nz.content_bn });
                
            }
            rptTopNews.DataSource = dtTOp;
            rptTopNews.DataBind();
            foreach (bdmirrorDB.news nz in news_list)
            {
                bdmirrorDB.images img = get_img(nz);
                if (selected_lang != "bn")                //that means en
                {
                    title_link = "/news/" + selected_lang + "/" +
                                           nz.category_id.name_en + "/" +
                                           nz.news_id + "/" + obutility.replaceSpace(nz.title_en);
                }
                else
                {
                    title_link = "/news/" + selected_lang + "/" +
                                            nz.category_id.name_bn + "/" +
                                            nz.news_id + "/" + obutility.replaceSpace(nz.title_bn);
                }

                string date_link;
                string date_visible;
                string[] date = nz.date.ToString().Split(' ')[0].Split('/');
                //string[] date = all[0].Split('/');
                date_visible = date[1] + "/";
                //string strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(int.Parse(date[0]));
                date_visible += date[0];
                date_visible += "/" + date[2];
                date_link = "Date/" + selected_lang + "/" +
                                                date[1] + "-" + date[0] + "-" + date[2] + "/";


                if (count < 4)
                {
                    if (selected_lang != "bn")
                        dtList[0].Rows.Add(new string[] { nz.news_id.ToString(), nz.title_en, title_link,img.link,img.caption_en, nz.content_en + "...", date_visible, date_link });
                    else
                        dtList[0].Rows.Add(new string[] { nz.news_id.ToString(), nz.title_bn, title_link, img.link, img.caption_en, nz.content_bn + "...", date_visible, date_link });

                }
                else if (count < 6)
                {
                    //en-bn check,date/date link check
                    if (selected_lang != "bn")
                        dtList[1].Rows.Add(new string[] { nz.news_id.ToString(), nz.title_en, title_link, img.link, img.caption_en, nz.content_en + "...", date_visible, date_link });
                    else
                        dtList[1].Rows.Add(new string[] { nz.news_id.ToString(), nz.title_bn, title_link, img.link, img.caption_en, nz.content_bn + "...", date_visible, date_link });

                }
                else
                {
                    if (selected_lang != "bn")
                        dtList[2].Rows.Add(new string[] { nz.news_id.ToString(), nz.title_en, title_link, img.link, img.caption_en, nz.content_en + "...", date_visible, date_link });
                    else
                        dtList[2].Rows.Add(new string[] { nz.news_id.ToString(), nz.title_bn, title_link, img.link, img.caption_en, nz.content_bn + "...", date_visible, date_link });

                }
                count++;
            }
            rptmainnews.DataSource = dtList[0];
            rptmainnews.DataBind();
            rptmain2news.DataSource = dtList[1];
            rptmain2news.DataBind();
            rptmain3news.DataSource = dtList[2];
            rptmain3news.DataBind();
        }
        protected void rightNewsLoad()
        {
            string selected_lang = "en";
            if (language != "bn")
            {
                selected_lang = "en";
            }
            else
            {
                selected_lang = "bn";
            }
            IList<int> res = ndao.getRightNews(selected_lang);
            IList<bdmirrorDB.news> news_list = getIlistNews(res);
            IList<DataTable> dtlist = new List<DataTable>(4);
            for (int i = 0; i < 4; i++)
            {
                DataTable dt = new DataTable("RightNews" + i);
                dt.Columns.Add("title_link");
                dt.Columns.Add("title");
                dt.Columns.Add("img_url");
                dt.Columns.Add("img_text");
                dt.Columns.Add("content");
                dtlist.Add(dt);
            }
            int count = 0;

            foreach (bdmirrorDB.news nz in news_list)
            {
                bdmirrorDB.images img = get_img(nz);

                string title_link = "/news/" + selected_lang + "/" +
                                            nz.category_id.name_en + "/" +
                                            nz.news_id + "/" + obutility.replaceSpace(nz.title_en);
                string title = string.Empty;
                string content = "";                
                if (selected_lang != "bn")
                {
                    title = nz.title_en;
                    content = nz.content_en;
                }
                else
                {
                    if (nz.title_bn != null)
                    {
                        title = nz.title_bn;
                        content = nz.content_bn;
                    }
                    else
                    {
                        content = nz.content_en;
                        title = nz.title_en;
                    }
                }

                if (nz.category_id.cat_id == 3)
                {
                    dtlist[0].Rows.Add(new string[] { title_link, title,img.link,img.caption_en, content });
                }
                else if (nz.category_id.cat_id == 1)
                {
                    dtlist[1].Rows.Add(new string[] { title_link, title, img.link, img.caption_en, content });
                }

                else if (nz.category_id.cat_id == 6)
                {
                    dtlist[2].Rows.Add(new string[] { title_link, title, img.link, img.caption_en, content });
                }

                else if (nz.category_id.cat_id == 2)
                {
                    dtlist[3].Rows.Add(new string[] { title_link, title, img.link, img.caption_en, content });
                }
                count++;

            }
            rptinternational.DataSource = dtlist[0];
            rptinternational.DataBind();
            rptnational.DataSource = dtlist[1];
            rptnational.DataBind();
            rptsports.DataSource = dtlist[2];
            rptsports.DataBind();
            rptpolitics.DataSource = dtlist[3];
            rptpolitics.DataBind();
        }
        protected string GetLink()
        {
            Random rnd = new Random();
            string[] arr = { "alink", "blink", "clink", "dlink" };
            return arr[new Random().Next(0, 3)];
        }

        protected IList<bdmirrorDB.news> getIlistNews(IList<int> res)
        {
            //IList<int> res = ndao.getHomeNews("en");
            couchbasemain cbm = new couchbasemain();
            IList<bdmirrorDB.news> news_list = new List<bdmirrorDB.news>();

            foreach (int id in res)
            {
                bdmirrorDB.news news = cbm.getNews(id);
                if (news == null)
                {
                    news = ndao.pickbyID(id); cbm.setnews(news);

                }
                news_list.Add(news);
            }
            return news_list;
        }
        NewsDAO ndao = new NewsDAO();
        couchbasemain cbm = new couchbasemain();
        protected void midrpt_load()
        {

            IList<int> res = (language == "en") ? ndao.getHomeNews("en") : ndao.getHomeNews("bn");
            
            IList<bdmirrorDB.news> news_list = new List<bdmirrorDB.news>();
            IList<DataTable> dtlist = new List<DataTable>(6);

            foreach (int id in res)
            {
                bdmirrorDB.news news = cbm.getNews(id);
                if (news == null)
                {
                    news = ndao.pickbyID(id); cbm.setnews(news);

                }
                news_list.Add(news);
            }

            for (int i = 0; i < 6; i++)
            {
                DataTable dtable = new DataTable("midnews" + i);
                //DataTable dt =dtList[i];
                //en-bn check,date/date link check
                dtable.Columns.Add("category");
                dtable.Columns.Add("cat_link");
                dtable.Columns.Add("id");
                dtable.Columns.Add("title");
                dtable.Columns.Add("title_link");
                dtable.Columns.Add("content");
                dtable.Columns.Add("date_pub");
                dtable.Columns.Add("date_pub_link");
                dtable.Columns.Add("img_url");
                dtable.Columns.Add("img_text");
                dtable.Columns.Add("next_ids");
                dtlist.Add(dtable);
            }

            int count = 0;
            string title_link = string.Empty;
            int index = 0;
            foreach (bdmirrorDB.news nz in news_list)
            {
                if (news_list.IndexOf(nz) != 0)
                {
                    if (nz.category_id.cat_id == news_list[(news_list.IndexOf(nz) - 1)].category_id.cat_id) continue;
                }

                if (language != "bn")
                {
                    title_link = "/news/" + "en" + "/" +
                                       nz.category_id.name_en + "/" +
                                       nz.news_id + "/" + obutility.replaceSpace(nz.title_en);
                }
                else
                {
                    title_link = "/news/" + "bn" + "/" +
                                     nz.category_id.name_bn + "/" +
                                     nz.news_id + "/" + obutility.replaceSpace(nz.title_bn);
                }


                string date_link;
                string date_visible;
                string[] date = nz.date.ToString().Split(' ')[0].Split('/');
                //string[] date = all[0].Split('/');
                date_visible = date[0] + "/";
                //string strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(int.Parse(date[1]));
                date_visible += date[1];
                date_visible += "/" + date[2];
                date_link = "/date/" + language + "/" +
                                                date[1] + "-" + date[0] + "-" + date[2] + "/";
                string cat_link = string.Empty;
                if (language != "bn")
                {
                    cat_link = "/news/" + "en" + "/" + nz.category_id.name_en + "/";
                }
                else
                {
                    cat_link = "/news/" + "bn" + "/" + nz.category_id.name_bn + "/";
                }
                string next_id = string.Empty;
                if (index+5 <= news_list.Count)
                {
                    next_id = news_list[index + 1].news_id.ToString() +
                        "," + news_list[index + 2].news_id.ToString() +
                        "," + news_list[index + 3].news_id.ToString() +
                        "," + news_list[index + 4].news_id.ToString();
                    index += 5;
                }
                bdmirrorDB.images img = get_img(nz);
                if (count < 4)
                {
                    if (language != "bn")
                        dtlist[0].Rows.Add(new string[] { nz.category_id.name_en, cat_link, nz.news_id.ToString(), nz.title_en, title_link, nz.content_en + "...", date_visible, date_link,img.link, img.caption_en,next_id });
                    else
                        dtlist[0].Rows.Add(new string[] { nz.category_id.name_bn, cat_link, nz.news_id.ToString(), nz.title_bn, title_link, nz.content_bn + "...", date_visible, date_link, img.link, img.caption_en, next_id });
                }
                else if (count < 8)
                {
                    if (language != "bn")
                        dtlist[1].Rows.Add(new string[] { nz.category_id.name_en, cat_link, nz.news_id.ToString(), nz.title_en, title_link, nz.content_en + "...", date_visible, date_link, img.link, img.caption_en, next_id });
                    else
                        dtlist[1].Rows.Add(new string[] { nz.category_id.name_bn, cat_link, nz.news_id.ToString(), nz.title_bn, title_link, nz.content_bn + "...", date_visible, date_link, img.link, img.caption_en, next_id });
                }
                else if (count < 12)
                {
                    if (language != "bn")
                        dtlist[2].Rows.Add(new string[] { nz.category_id.name_en, cat_link, nz.news_id.ToString(), nz.title_en, title_link, nz.content_en + "...", date_visible, date_link, img.link, img.caption_en, next_id });
                    else
                        dtlist[2].Rows.Add(new string[] { nz.category_id.name_bn, cat_link, nz.news_id.ToString(), nz.title_bn, title_link, nz.content_bn + "...", date_visible, date_link, img.link, img.caption_en, next_id });
                }
                else if (count < 16)
                {
                    if (language != "bn")
                        dtlist[3].Rows.Add(new string[] { nz.category_id.name_en, cat_link, nz.news_id.ToString(), nz.title_en, title_link, nz.content_en + "...", date_visible, date_link, img.link, img.caption_en, next_id });
                    else
                        dtlist[3].Rows.Add(new string[] { nz.category_id.name_bn, cat_link, nz.news_id.ToString(), nz.title_bn, title_link, nz.content_bn + "...", date_visible, date_link, img.link, img.caption_en, next_id });
                }
                else if (count < 20)
                {
                    if (language != "bn")
                        dtlist[4].Rows.Add(new string[] { nz.category_id.name_en, cat_link, nz.news_id.ToString(), nz.title_en, title_link, nz.content_en + "...", date_visible, date_link, img.link, img.caption_en, next_id });
                    else
                        dtlist[4].Rows.Add(new string[] { nz.category_id.name_bn, cat_link, nz.news_id.ToString(), nz.title_bn, title_link, nz.content_bn + "...", date_visible, date_link, img.link, img.caption_en, next_id });
                }
                //else if (count < 18)
                //{
                //    dtlist[5].Rows.Add(new string[] { nz.category_id.name_en, cat_link, nz.news_id.ToString(), nz.title_en, title_link, nz.content_en, date_visible, date_link });
                //}

                count++;


            }
            rptmidnews1.DataSource = dtlist[0];
            rptmidnews1.DataBind();
            rptmidnews2.DataSource = dtlist[1];
            rptmidnews2.DataBind();
            rptmidnews3.DataSource = dtlist[2];
            rptmidnews3.DataBind();
            rptmidnews4.DataSource = dtlist[3];
            rptmidnews4.DataBind();
            rptmidnews5.DataSource = dtlist[4];
            rptmidnews5.DataBind();
        }
        public string getLinkFromTitle(bdmirrorDB.news nz)
        {
            bool is_english = (language == "en") ? true : false;
            if (is_english)
            {
                return  "news/" + language + "/" +nz.category_id.name_en
                    + "/" +nz.news_id + "/" + obutility.replaceSpace(nz.title_en);
            }
            else
            {
                return  "news/" + language + "/" + nz.category_id.name_bn + "/" +nz.news_id
                    + "/" + obutility.replaceSpace(nz.title_bn);
            }
        }
        #region repeater
        protected void rptmostread_bound(object sender, RepeaterItemEventArgs e)
        {
            bool is_english = (language == "en") ? true : false;

            HyperLink toplink = (HyperLink)e.Item.FindControl("hpltoptitle");
            if (toplink != null)
            {
                // bool is_english = (language == "en") ? true : false;
                if (is_english)
                {
                    toplink.NavigateUrl = "news/" + language + "/" + DataBinder.Eval(e.Item.DataItem, "name_en").ToString() + "/" + DataBinder.Eval(e.Item.DataItem, "news_id").ToString() + "/" + obutility.replaceSpace(DataBinder.Eval(e.Item.DataItem, "title_en").ToString());
                }
                else
                {
                    toplink.NavigateUrl = "news/" + language + "/" + DataBinder.Eval(e.Item.DataItem, "name_bn").ToString() + "/" + DataBinder.Eval(e.Item.DataItem, "news_id").ToString() + "/" + obutility.replaceSpace(DataBinder.Eval(e.Item.DataItem, "title_bn").ToString());
                }

                toplink.Text = (language == "bn") ? DataBinder.Eval(e.Item.DataItem, "title_bn").ToString()
                                   : DataBinder.Eval(e.Item.DataItem, "title_en").ToString();
            }

            Label lblcontent = (Label)e.Item.FindControl("lblcontent");
            if (lblcontent != null)
            {
                lblcontent.Text = (language == "bn") ? DataBinder.Eval(e.Item.DataItem, "content_bn").ToString() : DataBinder.Eval(e.Item.DataItem, "content_en").ToString();
            }
        }
        protected void rptlatest_bound(object sender, RepeaterItemEventArgs e)
        { 
            HyperLink hplLink = (HyperLink)e.Item.FindControl("hplLink");

            // HyperLink hplLink = (HyperLink) e.Item.FindControl("hplLink");
            if (hplLink != null)
            {
                string lang = "en";
                if (language != "bn") lang = "en";
                else lang = "bn";
                hplLink.Text = (lang != "bn")
                                    ? DataBinder.Eval(e.Item.DataItem, "title_en").ToString()
                                    : DataBinder.Eval(e.Item.DataItem, "title_bn").ToString();
                string cat = (lang != "bn") ? DataBinder.Eval(e.Item.DataItem, "name_en").ToString() :
                    DataBinder.Eval(e.Item.DataItem, "name_bn").ToString();
                hplLink.NavigateUrl = "/news/" + lang + "/" + cat +
                    "/" + DataBinder.Eval(e.Item.DataItem, "id") + "/" + obutility.replaceSpace(hplLink.Text);
            }
        }
        protected void rptCat_bound(object sender, RepeaterItemEventArgs e)
        {
            HyperLink hplLink = (HyperLink)e.Item.FindControl("hplLink");

            // HyperLink hplLink = (HyperLink) e.Item.FindControl("hplLink");
            if (hplLink != null)
            {
                string lang = "en";
                if (language != "bn") lang = "en";
                else lang = "bn";
                hplLink.Text = (lang != "bn")
                                    ? DataBinder.Eval(e.Item.DataItem, "name_en").ToString()
                                    : DataBinder.Eval(e.Item.DataItem, "name_bn").ToString();
                if((lang != "bn"))
                    hplLink.NavigateUrl = "/news/" + lang + "/" + 
                                     DataBinder.Eval(e.Item.DataItem, "name_en").ToString()+ "/";
                else
                    hplLink.NavigateUrl = "/news/" + lang + "/" +
                                    DataBinder.Eval(e.Item.DataItem, "name_bn").ToString() + "/";
                hplLink.Font.Size = (lang == "bn") ? new FontUnit(10) : new FontUnit(12);
            }



        }
        protected void rptimg_bound(object sender, RepeaterItemEventArgs e)
        {
            HyperLink caption = (HyperLink)e.Item.FindControl("hplcap");
            string category = string.Empty;
            string title = string.Empty;
            int news_id = 0;
            
                category = DataBinder.Eval(e.Item.DataItem, "cat_en").ToString() == null ? "NO Category".ToString() : DataBinder.Eval(e.Item.DataItem, "cat_en").ToString();
            
                news_id = int.Parse(DataBinder.Eval(e.Item.DataItem, "news_id").ToString());
           
                title =(language=="en")? DataBinder.Eval(e.Item.DataItem, "title_en").ToString():DataBinder.Eval(e.Item.DataItem, "title_bn").ToString();
          
            if (caption != null)
            {
                caption.NavigateUrl = "news/" + language + "/" + category + "/" + news_id + "/" + obutility.replaceSpace(title);
                caption.Text = title;
            }
            

        }
        protected void loadrepeater()
        {
            //DataTable dt = new DataTable("Rows");
            //dt.Columns.Add("title_en", typeof(string));
            ////dt.Columns.Add("limit2", typeof(string));
            //for (Int32 r = 0; r < 9 + 0; r += 3)
            //{
            //    dt.Rows.Add(new string[] { r.ToString()});

            //}
            //rptmainnews.DataSource = dt;

        }
        protected void rptmidnews1_bound(object sender, RepeaterItemEventArgs e)
        {
            Repeater rptinn =(Repeater) e.Item.FindControl("rptmidnews1inn");
            if (rptinn != null)
            {
                string[] tmp = (DataBinder.Eval(e.Item.DataItem, "next_ids").ToString()).Split(',');
                IList<int> idint = new List<int>();
                foreach (string id in tmp)
                {
                    if (id == "") return;
                    idint.Add(int.Parse(id));
                }

                if (idint.Count!=1)
                {
                    
                    IList<bdmirrorDB.news> nlist = getIlistNews(idint);
                    DataTable dtTOp = new DataTable("dsstopnews");
                    dtTOp.Columns.Add("title", typeof(string));
                    dtTOp.Columns.Add("title_link", typeof(string));
                    //dtTOp.Columns.Add("title_bn_link", typeof(string));
                    dtTOp.Columns.Add("news_id", typeof(string));
                    dtTOp.Columns.Add("cat", typeof(string));
                    dtTOp.Columns.Add("content", typeof(string));
                    foreach (bdmirrorDB.news nz in nlist)
                    {
                        if (language == "en")
                            dtTOp.Rows.Add(new string[] { nz.title_en, getLinkFromTitle(nz), nz.news_id.ToString(), nz.category_id.name_en, nz.content_en });
                        else
                            dtTOp.Rows.Add(new string[] { nz.title_bn, getLinkFromTitle(nz), nz.news_id.ToString(), nz.category_id.name_bn, nz.content_bn });

                    }
                    rptinn.DataSource = dtTOp;
                    rptinn.DataBind();
                }
            }

        }
        protected void rpttop_bound(object sender, RepeaterItemEventArgs e)
        {
            bool is_english = (language == "en") ? true : false;

            HyperLink toplink = (HyperLink)e.Item.FindControl("hpltoptitle");
            if (toplink != null)
            {
                // bool is_english = (language == "en") ? true : false;
                if (is_english)
                {
                    toplink.NavigateUrl = "news/" + language + "/" + DataBinder.Eval(e.Item.DataItem, "name_en").ToString() + "/" + DataBinder.Eval(e.Item.DataItem, "news_id").ToString() + "/" + obutility.replaceSpace(DataBinder.Eval(e.Item.DataItem, "title_en").ToString());
                }
                else
                {
                    toplink.NavigateUrl = "news/" + language + "/" + DataBinder.Eval(e.Item.DataItem, "name_bn").ToString() + "/" + DataBinder.Eval(e.Item.DataItem, "news_id").ToString() + "/" + obutility.replaceSpace(DataBinder.Eval(e.Item.DataItem, "title_bn").ToString());
                }

                toplink.Text = (language == "bn") ? DataBinder.Eval(e.Item.DataItem, "title_bn").ToString()
                                   : DataBinder.Eval(e.Item.DataItem, "title_en").ToString();
            }

            Label lblcontent = (Label)e.Item.FindControl("lblcontent");
            if (lblcontent != null)
            {
                lblcontent.Text = (language == "bn") ? DataBinder.Eval(e.Item.DataItem, "content_bn").ToString() : DataBinder.Eval(e.Item.DataItem, "content_en").ToString();
            }
        }

        #endregion
    }
}