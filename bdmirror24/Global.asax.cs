﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Routing;
namespace bdmirror24
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes(RouteTable.Routes);
        }
        public static void RegisterRoutes(RouteCollection routes)
        {


            //routes.Ignore("{resource}.js");
            //routes.Ignore("{resource}.png/{*pathInfo}");
            //routes.Ignore("Bin/{*path}");
            //routes.Add(new Route("*.js", new StopRoutingHandler()));
            //routes.Add(new Route("{resource}.axd/{*pathInfo}", new StopRoutingHandler()));
            //routes.Add(new Route("{resource}.dll/{*pathInfo}", new StopRoutingHandler()));
            //routes.Add(new Route("*.js", new StopRoutingHandler()));
            //routes.Add(new Route("{folder}/{*pathInfo}", new { folder = "content" }, new StopRoutingHandler()));
            
            //routes.Add(new Route("{resource}.png/{*pathInfo}", new StopRoutingHandler()));

            //routes.Ignore("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
            //routes.Ignore("{folder}/{*pathInfo}", new { folder = "References" });
            //routes.Ignore("{folder}/{*pathInfo}", new { folder = "Bin" });

            //routes.Ignore("{resource}.axd/{*pathInfo}");
            //routes.Ignore("Bin/{*path}");
            //routes.Add(new Route("{resource}.axd/{*pathInfo}", new StopRoutingHandler()));
            //routes.Add(new Route("{resource}.dll/{*pathInfo}", new StopRoutingHandler()));

            routes.Ignore("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
            //routes.Ignore("{folder}/{*pathInfo}", new { folder = "References" });
            //routes.Ignore("{folder}/{*pathInfo}", new { folder = "Bin" });
            routes.Ignore("{folder}/{*pathInfo}", new { folder = "js" });

            //routes.Ignore("{file}.js");
            //routes.Ignore("{file}.css");
            //routes.MapPageRoute("", "gunner/{year}", "~/test.aspx");
            routes.MapPageRoute("", "{lang}/", "~/default.aspx");
            routes.MapPageRoute("", "User/Home/{catid}/", "~/User/Home.aspx");
            routes.MapPageRoute("", "User/Home/", "~/User/Home.aspx");
            routes.MapPageRoute("", "User/video/", "~/User/video.aspx");
            routes.MapPageRoute("", "User/Category/", "~/User/category.aspx");
            routes.MapPageRoute("", "User/Upload/", "~/User/UploadFile.aspx");
            routes.MapPageRoute("", "User/login/", "~/User/login.aspx");
            routes.MapPageRoute("", "User/logout/", "~/User/login.aspx");
            routes.MapPageRoute("", "User/CA/", "~/User/CurrentAffairs.aspx");
            routes.MapPageRoute("", "User/Advertise/", "~/User/Advertise.aspx");
            routes.MapPageRoute("", "livecricket/", "~/LiveCricket.aspx");
            routes.MapPageRoute("", "livesports/", "~/LiveSports.aspx");
            routes.MapPageRoute("", "liveradio/", "~/LiveFootball.aspx");
            routes.MapPageRoute("", "BanglaTV/", "~/LivebdTV.aspx");
            routes.MapPageRoute("", "Date/{lang}/{date}", "~/Date.aspx");
            routes.MapPageRoute("", "Gallery/{lang}/{cat}", "~/Gallery.aspx");
            routes.MapPageRoute("", "CurrentAffairs/{lang}", "~/Current.aspx");
            routes.MapPageRoute("", "Advertise/List", "~/advlist.aspx");
            
            routes.MapPageRoute("", "news/{lang}/{category}/{id}/{title}", "~/details.aspx");
            routes.MapPageRoute("", "news/{lang}/{category}", "~/news.aspx");


            ////routes.MapPageRoute("", "User/add_news/", "~/User/popups/edit_news.aspx");
            //routes.MapPageRoute("", "User/edit_news/{id}", "~/User/popups/edit_news.aspx");

            ////routes.MapPageRoute("","tinymce.js","~/tinymce/jscripts/tiny_mce/tiny_mce.js");
            //routes.MapPageRoute("", "User/add_news/{cat}/", "~/User/popups/edit_news.aspx");

        }
        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}