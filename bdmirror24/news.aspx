﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bdmirror.Master" AutoEventWireup="true"
    CodeBehind="news.aspx.cs" Inherits="bdmirror24.news" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="span12">
        <div class="page-header row-fluid">
            <h1>
                News <small>
                    <asp:Label ID="lblcat" runat="server" Text="" /></small></h1>
            <br />
        </div>
        <div class="row">
            <!--page body -->
            <div class="span8">
                <!--span8 start-->
                <div id="dvNews">
                    <ul class="thumbnails">
                        <li>
                            <div>
                                <div class="news_content">
                                    <div class="wellbackground">
                                        <div id="myCarousel" class="carousel slide">
                                            <div class="carousel-inner">
                                                <asp:Repeater runat="server" ID="rptCarousal">
                                                    <ItemTemplate>
                                                        <div class="item">
                                                            <asp:Image src='<%# Eval("img_link") %>' Height="250px" runat="server" ID="imgCar" alt='<%# Eval("img_text") %>' />
                                                            <div class="carousel-caption">
                                                                <p>
                                                                    <asp:HyperLink ID="hplcap" NavigateUrl='<%# Eval("caption_link") %>' Text='<%# Eval("caption_text") %>'
                                                                        runat="server"></asp:HyperLink>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <asp:SqlDataSource ID="dssimg" runat="server" ProviderName="System.Data.Odbc" ConnectionString="<%$ ConnectionStrings:bdMirrorDB %>">
                                                </asp:SqlDataSource>
                                            </div>
                                            <a class="left carousel-control" data-slide="prev" href="#myCarousel">‹</a>
                                            <a class="right carousel-control" data-slide="next" href="#myCarousel">›</a>
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                                    <p>
                                        <h3>
                                            <asp:HyperLink ID="hplmaintitle" runat="server"></asp:HyperLink>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3>
                                    </p>
                                    <br />
                                    <p class="toppadding">
                                        <asp:HyperLink ID="hplwritermain" CssClass="leftpadding" runat="server"></asp:HyperLink>
                                        <br />
                                        <asp:Label ID="lblmainnews" runat="server" Text="" CssClass="newscontent12"></asp:Label></p>
                                    <asp:HyperLink ID="hplmainrd" runat="server" CssClass="lightsilver">Read More</asp:HyperLink>
                                    <p class="pull-right">
                                        <asp:HyperLink ID="hplmaindatepub" runat="server" Text="" CssClass="lightsilver"></asp:HyperLink></p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="span4">
                <!--span4 start-->
                <div class="wellbackground centered">
                    <div class="tabbable">
                        <!--tab menu-->
                        <ul class="nav nav-tabs" id="Ul1">
                            <li><a href="#most" class="active" data-toggle="tab">Latest News</a></li>
                            <li><a href="#topnews" data-toggle="tab">Top News</a></li>
                            <li><a href="#latest" data-toggle="tab">Most Read</a></li>
                        </ul>
                        <div class="tab-content row-fluid">
                            <!--tab-->
                            <div class="tab-pane active" id="most">
                                <div id="accordion4" class="accordionsaad" style="width: 100%">
                                    <asp:Repeater ID="rptLatestRight" runat="server" OnItemDataBound="rpttop_bound">
                                        <ItemTemplate>
                                            <h5>
                                                <asp:HyperLink ID="hpltoptitle" runat="server"></asp:HyperLink></h5>
                                            <div>
                                                <p>
                                                    <asp:Image ID="Image117" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                        AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" /><br>
                                                    <asp:Label ID="lblcontent" runat="server" Text="" CssClass="newscontent12"></asp:Label>
                                                </p>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                            <div class="tab-pane" id="topnews">
                                <div id="accordion5" class="accordionsaad" style="width: 100%">
                                    <asp:Repeater ID="rptTopNews" runat="server" OnItemDataBound="rpttop_bound">
                                        <ItemTemplate>
                                            <h5>
                                                <asp:HyperLink ID="hpltoptitle" runat="server"></asp:HyperLink></h5>
                                            <div>
                                                <p>
                                                    <asp:Image ID="Image117" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                        AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" /><br>
                                                    <asp:Label ID="lblcontent" runat="server" Text=""></asp:Label>
                                                </p>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                            <div class="tab-pane" id="latest">
                                <div id="accordion7" class="accordionsaad" style="width: 100%">
                                    <asp:Repeater ID="rptmostread" runat="server" OnItemDataBound="rptmostread_bound">
                                        <ItemTemplate>
                                            <h5>
                                                <asp:HyperLink ID="hpltoptitle" runat="server"></asp:HyperLink></h5>
                                            <div>
                                                <p>
                                                    <asp:Image ID="Image117" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                        AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" /><br>
                                                    <asp:Label ID="lblcontent" runat="server" CssClass="newscontent12"></asp:Label>
                                                </p>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <!--tab-->
                    </div>
                    <!--tab menu-->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="">
                <!--Rows news start-->
                <%--
                <asp:UpdatePanel ID="upPanel" UpdateMode="Conditional" runat="server">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="IntervalTimer" EventName="Tick" />
                    </Triggers>
                    <ContentTemplate>--%>
                <div class="row">
                    <asp:Repeater runat="server" ID="rptRow" OnItemDataBound="rptinnNews_bound">
                        <ItemTemplate>
                            <ul class="row-fluid">
                                <asp:Repeater runat="server" ID="rptinnNews" OnItemDataBound="rptRow_bound">
                                    <ItemTemplate>
                                        <li class="span4 gwrap">
                                            <div class="thumbnails">
                                                <h4>
                                                    <asp:HyperLink class="alink" ID="hplnews" runat="server"></asp:HyperLink>
                                                </h4>
                                                <p>
                                                <table cellspacing="4" cellpadding="4"><tr><td>
                                                                <asp:Image ID="Image14" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                                    AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" />
                                                                </td><td colspan="2">
                                                    <asp:Label ID="lblnews" runat="server" Text="" CssClass="newscontent12"></asp:Label>
                                                </td></tr></table></p>
                                                <asp:HyperLink ID="hplnewsrd" runat="server" CssClass="lightsilver">Read More</asp:HyperLink>
                                                <asp:HyperLink ID="hplpub" CssClass="pull-right lightsilver" runat="server"></asp:HyperLink>
                                            </div>
                                        </li>
                                    </ItemTemplate>
                                     <AlternatingItemTemplate>
                                        <li class="span4 wellbackground gwrap">
                                            <div class="thumbnails">
                                                <h4>
                                                    <asp:HyperLink class="alink" ID="hplnews" runat="server"></asp:HyperLink>
                                                </h4>
                                                <p>
                                                    <asp:Label ID="lblnews" runat="server" Text="" CssClass="newscontent12"></asp:Label>
                                                </p>
                                                <asp:HyperLink ID="hplnewsrd" runat="server" CssClass="lightsilver">Read More</asp:HyperLink>
                                                <asp:HyperLink ID="hplpub" CssClass="pull-right lightsilver" runat="server"></asp:HyperLink>
                                            </div>
                                        </li>
                                    </AlternatingItemTemplate>
                                </asp:Repeater>
                                <asp:SqlDataSource ID="dssNews" EnableCaching="True" CacheDuration="20" runat="server"
                                    ProviderName="System.Data.Odbc" ConnectionString="<%$ ConnectionStrings:bdMirrorDB %>">
                                </asp:SqlDataSource>
                            </ul>
                        </ItemTemplate>
                    </asp:Repeater>
                    <%--  <img id="loader" alt="" src="http://www.bdmirror24.com/img/loading.gif" />
                    <asp:Button ID="Button1" runat="server" Text="Get More" OnClick="getmore_click" />--%>
                </div>
            </div>
            <!--thumbnails news endt-->
        </div>
    </div>
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
</asp:Content>
