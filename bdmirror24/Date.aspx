﻿<%@ Page Language="C#"  AutoEventWireup="true" MasterPageFile="~/bdmirror.Master" CodeBehind="Date.aspx.cs" Inherits="bdmirror24.Date" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
    <link href="http://www.bdmirror24.com/css/grid.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />
  <script type="text/javascript">
      $(function () {
          $(".txtTest").datepicker();
      });
</script>
<link rel="stylesheet" href="css/jquery-ui.css" />
       
 </asp:Content>
 <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div class="page-header row-fluid">
        <h1>
            Date Search <small>
                <asp:Label ID="lblcat" runat="server" Text="" ></asp:Label>
            </small>
        </h1>
        <br />
    </div>
    <div class="row-fluid span12">
        <!--page body -->
        <div class="span8">
            <!--span8 start-->
            <div>
                <ul class="thumbnails">
                    <li class="span12">
                        <div class="news_content">
                            <div class="hero-unit">
                                <div class="well">
                                    <asp:TextBox id="datepicker" type="text" runat="server" class="txtTest"/>
                                    <asp:Button CssClass="btn pull-right" ID="dbutton" runat="server" Text="search" OnClick="news_search"/>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div>
               <!-- grid here--->
                 <div class="thumbnails">
               <asp:GridView ID="grvNews" DataKeyNames="id" runat="server" Width="100%" AutoGenerateColumns="False"
                    CellPadding="4" HorizontalAlign="Left" OnPageIndexChanging="grvNews_PageIndexChanging"
                    ShowHeaderWhenEmpty="true" OnRowDataBound="grvNews_RowDataBound" OnRowCommand="grvNews_RowCommand"
                    EmptyDataText="No Records Found" CssClass="tablesorterBlue" DataSourceID="dssNews">
                    <Columns>
                      <%--  <asp:TemplateField HeaderText="#" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="2%">
                            <ItemTemplate>
                                <asp:Label ID="lblid" Text='<%# Eval("id") %>' runat="server" HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField> --%>
                        <asp:TemplateField HeaderText="News Title" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:HyperLink ID="lblentitle" Text='<%# (Eval("title")) %>' runat="server" HorizontalAlign="Left"/>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Category" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="2%">
                            <ItemTemplate>
                                <asp:HyperLink ID="lblcat" Text='<%# Eval("name") %>'  runat="server" HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
    
                        <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="6%">
                            <ItemTemplate>
                                <asp:Label ID="lbldate" Text='<%# Eval("date") %>' runat="server" HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    <%--    <asp:TemplateField ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink ID="hpledit" runat="server">Edit</asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField> --%> 
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Font-Bold="true" Font-Underline="false" BackColor="#e6EEEE" />
                    <AlternatingRowStyle BackColor="#E5EAE8" />
                    <EditRowStyle BackColor="#999999" />
                    <EmptyDataRowStyle ForeColor="#CC0000" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                </asp:GridView>
                     <asp:SqlDataSource ID="dssNews" runat="server" ProviderName="System.Data.Odbc"
                    ConnectionString="<%$ ConnectionStrings:bdMirrorDB %>"
                     >

                </asp:SqlDataSource>
                 </div>
            </div>
            <!--thumbnails news endt-->
        </div>
        <!--span8 end-->
        <div class="span4">
            <!--span4 start-->
           <div class="wellbackground centered">
                    <div class="tabbable">
                        <!--tab menu-->
                        <ul class="nav nav-tabs" id="Ul1">
                            <li><a href="#most" class="active" data-toggle="tab">Latest News</a></li>
                            <li><a href="#topnews" data-toggle="tab">Top News</a></li>
                            <li><a href="#latest" data-toggle="tab">Most Read</a></li>
                        </ul>
                        <div class="tab-content row-fluid">
                            <!--tab-->
                            <div class="tab-pane active" id="most">
                                <div id="accordion4" class="accordionsaad" style="width: 100%">
                                    <asp:Repeater ID="rptLatestRight" runat="server" OnItemDataBound="rpttop_bound">
                                        <ItemTemplate>
                                            <h5>
                                                <asp:HyperLink ID="hpltoptitle" runat="server"></asp:HyperLink></h5>
                                            <div>
                                                <p>
                                                    <asp:Image ID="Image117" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                        AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" /><br>
                                                    <asp:Label ID="lblcontent" runat="server" Text=""></asp:Label>
                                                </p>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                            <div class="tab-pane" id="topnews">
                                <div id="accordion5" class="accordionsaad" style="width: 100%">
                                    <asp:Repeater ID="rptTopNews" runat="server" OnItemDataBound="rpttop_bound">
                                        <ItemTemplate>
                                            <h5>
                                                <asp:HyperLink ID="hpltoptitle" runat="server"></asp:HyperLink></h5>
                                            <div>
                                                <p>
                                                    <asp:Image ID="Image117" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                        AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" /><br>
                                                    <asp:Label ID="lblcontent" runat="server" Text=""></asp:Label>
                                                </p>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                            <div class="tab-pane" id="latest">
                                <div id="accordion7" class="accordionsaad" style="width: 100%">
                                    <asp:Repeater ID="rptmostread" runat="server" OnItemDataBound="rptmostread_bound">
                                        <ItemTemplate>
                                            <h5>
                                                <asp:HyperLink ID="hpltoptitle" runat="server"></asp:HyperLink></h5>
                                            <div>
                                                <p>
                                                    <asp:Image ID="Image117" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                        AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" /><br>
                                                    <asp:Label ID="lblcontent" runat="server" Text="Label"></asp:Label>
                                                </p>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <!--tab-->
                    </div>
                    <!--tab menu-->
                </div>
        </div>
        <!--span4 end-->
    </div>
    <!--page end-->
    <br />
      
 </asp:Content>