﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace bdmirror24
{
    public partial class LiveCricket : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                iframecricket.Attributes.Add("src", "http://www.streamer247.com/max.php");
        }
        protected void channelchange(object sender, EventArgs e)
        {
            if (((Button)sender).ID.Equals("btnsony"))
            {
                iframecricket.Attributes.Add("src", "http://www.streamer247.com/max.php");
            }
            else if(((Button)sender).ID.Equals("btnstarsports"))
            {
                iframecricket.Attributes.Add("src", "http://www.streamer247.com/starsports.php");
            }
            else if (((Button)sender).ID.Equals("btnespn"))
            {
                iframecricket.Attributes.Add("src", "http://www.streamer247.com/espn.php");
            }
            else if (((Button)sender).ID.Equals("btntenccric"))
            {
                iframecricket.Attributes.Add("src", "http://www.streamer247.com/tencricket.php");
            }
            else if (((Button)sender).ID.Equals("btnstarcric"))
            {
                iframecricket.Attributes.Add("src", "http://www.streamer247.com/starcricket.php");
            }
            else if (((Button)sender).ID.Equals("btnHD"))
            {
                iframecricket.Attributes.Add("src", "http://www.streamer247.com/hd.php");
            }
        }
    }
}