﻿<%@ Page  Language="C#" MasterPageFile="~/bdmirror.Master" AutoEventWireup="true"
    CodeBehind="current.aspx.cs" Inherits="bdmirror24.current" Title="BdMirror24 :: Current Affairs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta name="description" content="All the Current Affairs of Bangladesh,helping you for General Knowledge." />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header row-fluid">
        <h1>
            Current <small>
                Affairs</small></h1>
        <br />
    </div>
    <div class="row-fluid">
        <!--page body -->
        <div class="span8">
            <!--span8 start-->
            <div>
                <ul class="thumbnails">
                    <li class="span12">
                        <div class="news_content">
                            <asp:GridView ID="grvNews" runat="server" Width="100%" AutoGenerateColumns="False"
                                CellPadding="4" HorizontalAlign="Left" OnPageIndexChanging="grvNews_PageIndexChanging"
                                ShowHeaderWhenEmpty="true"
                                EmptyDataText="No Records Found" CssClass="table table-bordered" AllowPaging="True"
                                PageSize="15" DataSourceID="dssNews">
                                <Columns>
                                    <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="2%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblid" Text='<%# Eval("insert_date") %>' runat="server" HorizontalAlign="Left" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Content" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblentitle" Text='<%# (Eval("content_en")) %>' runat="server" HorizontalAlign="Left" />
                                        </ItemTemplate>
                                        </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Right" Font-Bold="true" Font-Underline="false" BackColor="#e6EEEE" />
                                <AlternatingRowStyle BackColor="#E5EAE8" />
                                <EditRowStyle BackColor="#999999" />
                                <EmptyDataRowStyle ForeColor="#CC0000" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="dssNews" runat="server" ProviderName="System.Data.Odbc"
                            ConnectionString="<%$ ConnectionStrings:bdMirrorDB %>"
                                SelectCommand="select n.insert_date,n.content_en 
                                from current_affairs as n
                                order by n.insert_date desc">
                        </asp:SqlDataSource>


                        </div>
                    </li>
                </ul>
            </div>
            <!--thumbnails news endt-->
        </div>
        <!--span8 end-->
        <div class="span4">
            <!--span4 start-->
              <div class="wellbackground centered">
                    <div class="tabbable">
                        <!--tab menu-->
                        <ul class="nav nav-tabs" id="Ul1">
                            <li><a href="#most" class="active" data-toggle="tab">Top News</a></li>
                            <li><a href="#topnews" data-toggle="tab">Latest News</a></li>
                            <li><a href="#latest" data-toggle="tab">Most Read</a></li>
                        </ul>
                        <div class="tab-content row-fluid">
                            <!--tab-->
                            <div class="tab-pane active" id="most">
                                <div id="accordion4" class="accordionsaad" style="width: 100%">
                                 <asp:Repeater ID="rptTopNews" runat="server" OnItemDataBound="rpttop_bound">
                                        <ItemTemplate>
                                            <h5>
                                                <asp:HyperLink ID="hpltoptitle" runat="server"></asp:HyperLink></h5>
                                            <div>
                                                <p>
                                                    <asp:Image ID="Image117" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                        AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" /><br>
                                                    <asp:Label ID="lblcontent" runat="server" Text=""></asp:Label>
                                                </p>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                            <div class="tab-pane" id="topnews">
                                <div id="accordion5" class="accordionsaad" style="width: 100%">
                                   
                                    <asp:Repeater ID="rptLatestRight" runat="server" OnItemDataBound="rpttop_bound">
                                        <ItemTemplate>
                                            <h5>
                                                <asp:HyperLink ID="hpltoptitle" runat="server"></asp:HyperLink></h5>
                                            <div>
                                                <p>
                                                    <asp:Image ID="Image117" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                        AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" /><br>
                                                    <asp:Label ID="lblcontent" runat="server" Text=""></asp:Label>
                                                </p>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                            <div class="tab-pane" id="latest">
                                <div id="accordion7" class="accordionsaad" style="width: 100%">
                                    <asp:Repeater ID="rptmostread" runat="server" OnItemDataBound="rptmostread_bound">
                                        <ItemTemplate>
                                            <h5>
                                                <asp:HyperLink ID="hpltoptitle" runat="server"></asp:HyperLink></h5>
                                            <div>
                                                <p>
                                                    <asp:Image ID="Image117" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                        AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" /><br>
                                                    <asp:Label ID="lblcontent" runat="server" Text="Label"></asp:Label>
                                                </p>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <!--tab-->
                    </div>
                    <!--tab menu-->
                </div>
        </div>
        <!--span4 end-->
    </div>
    <!--page end-->
    <br />
    </div>
</asp:Content>
