﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.Manager;
using System.Data;
using bdmirror24.couchbase;
using bdmirror24.DAL.DAL_Functions.Implementations;
namespace bdmirror24
{
	public class Utility
	{
        NewsDAO ndao = new NewsDAO();
        public IList<bdmirrorDB.news> getIlistNews(IList<int> res)
        {
            //IList<int> res = ndao.getHomeNews("en");
            couchbasemain cbm = new couchbasemain();
            IList<bdmirrorDB.news> news_list = new List<bdmirrorDB.news>();

            foreach (int id in res)
            {
                bdmirrorDB.news news = cbm.getNews(id);
                if (news == null)
                {
                    news = ndao.pickbyID(id); cbm.setnews(news);

                }
                news_list.Add(news);
            }
            return news_list;
        }
        ////public BindRepeater(string lang,string hplheader,string content,string rm,RepeaterItemEventArgs e)
        //{
        //    HyperLink hplheader =    (HyperLink) e.Item.FindControl(hplheader);
        //    if(hplheader!=null)
        //    {
        //            hplheader.Text = (lang != "bn")
        //                               ? DataBinder.Eval(e.Item.DataItem, "name_en").ToString()
        //                               : DataBinder.Eval(e.Item.DataItem, "name_bn").ToString();
        //            hplheader.NavigateUrl = "/news/" + lang + "/" + hplheader.Text + "/";
        //    }
        //    Label lblcont = (Label) e.Item.FindControl(content);
        //    if(lblcont!=null)
        //    {
            
        //    }

        //}
        public string replaceSpace(string input)
        {
            
            if (input != "")
            {
                input = input.Replace(",", string.Empty);
                input = input.Replace("&", string.Empty);
                input = input.Replace("\'", string.Empty);
                input = input.Replace("\"", string.Empty);
                input = input.Replace(":", string.Empty);
                input = input.Replace("/", string.Empty);
                input = input.Replace(" ", "");
                if (input.LastIndexOf(".") > input.Length - 2) 
                    input =input.Remove(input.Length - 1, 1);
                return input;
            }
            return input;
        }
        public static void BindDivisionDDL(DropDownList ddl, int countryID)
        {
            IList<bdmirrorDB.admdivision> division;

            if (countryID > 0)
            {
                division = new DivisionManager().GetAllDivisionByCountry(countryID);
            }
            else
            {
                division = new DivisionManager().GetAllDivision();
                //division = new List<ADMDivision>();
            }

            if (division != null)
            {
                if(ddl.Items.Count>2)
                    ddl.Items.Clear();
                ddl.DataSource = division;
                ddl.DataTextField = "divName";
                ddl.DataValueField = "division_id";
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem("   <-- Select Division -->", "-1"));
            }
        }
        
        public static void BindDistrictDDL(DropDownList ddl, int divisionID)
        {
            IList<bdmirrorDB.admdistrict> district;

            if (divisionID > 0)
            {
                district = new DistrictManager().GetAllDistrictByDivision(divisionID);
            }
            else
            {
                district = new DistrictManager().GetAllDistrict();
                //district = new List<ADMDistrict>();
            }

            if (district != null)
            {
                ddl.Items.Clear();
                ddl.DataSource = district;
                ddl.DataTextField = "DistrictName";
                ddl.DataValueField = "DistrictID";
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem("   <-- Select District -->", "-1"));
            }
        }
        public static void BindCountryDDL(DropDownList ddl)
        {
            IList<bdmirrorDB.admcountry> country = new CountryManager().GetAllCountry();

            if (country != null)
            {
                ddl.Items.Clear();
                ddl.DataSource = country;
                ddl.DataTextField = "country_name";
                ddl.DataValueField = "country_id";
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem("   <-- Select Country -->", "-1"));
            }
        }
        public static void BindUpazillaPSDDL(DropDownList ddl, int districtID)
        {
            IList<bdmirrorDB.admupazilaps> upazilaPS;

            if (districtID > 0)
            {
                upazilaPS = new UpzillaManager().GetAllUpzillaByDistrict(districtID);
            }
            else
            {
                upazilaPS = new UpzillaManager().GetAllUpzilla();
                //upazilaPS = new List<ADMUpazilaPS>();
            }

            if (upazilaPS != null)
            {
                ddl.Items.Clear();
                ddl.DataSource = upazilaPS;
                ddl.DataTextField = "UpazilaPSName";
                ddl.DataValueField = "UpazilaPSID";
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem("   <-- Select Upazila -->", "-1"));
            }
        }
	}
}