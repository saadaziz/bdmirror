﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.DAL_Functions.Implementations;
using bdmirror24.DAL.mappings;

namespace bdmirror24
{
    public partial class Gallery : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String language;
            
            if (!IsPostBack)
            {
                //i = 0;
                if (Page.RouteData.Values["cat"] != null)
                    lblcat.Text = (Server.UrlDecode(Page.RouteData.Values["cat"].ToString()));
                if (Page.RouteData.Values["lang"] != null)
                {
                    language = Page.RouteData.Values["lang"].ToString();
                    hplallcat.NavigateUrl = hplallcat.NavigateUrl = "/Gallery/" + language + "/all";
                    try
                    {
                        loadControl(lblcat.Text, language);
                    }
                    catch (Exception ex)
                    {

                    }

                    //hpltitle.Text=(Server.UrlDecode(Page.RouteData.Values["id"].ToString()));
                }
                //if (Page.RouteData.Values["title"] != null)
                //    Response.Write( Server.UrlDecode(Page.RouteData.Values["title"].ToString()));

            }
        }
        private bdmirrorDB.categories cat = new bdmirrorDB.categories();
        //private IList<bdmirrorDB.news> news = new List<bdmirrorDB.news>();
        //private IList<bdmirrorDB.images> images=new List<bdmirrorDB.images>(); 
        protected void loadControl(String cat_name, String lang)
        {
            cat = new CategoryDAO().getcatbyName(Server.UrlDecode(Page.RouteData.Values["cat"].ToString()));
            //news = new NewsDAO().getAllNewsbyCatName(cat);

            if (Page.RouteData.Values["cat"].Equals("all"))
            {
                dssgallery.SelectCommand = @"select image_name,caption_en,caption_bn,link,height,width
                                        from images where news_id!="";";
                rptgallery.DataBind();
            }
            else
            {
                dssgallery.SelectCommand =@"select news.category_id,categories.id,image_name,caption_en,caption_bn,link,height,width
                                            from images
                                            INNER JOIN news on images.news_id=news.id
                                            INNER JOIN categories on news.category_id=categories.id
                                            where news.category_id=" + cat.cat_id;
                //images = new ImageDAO().GetImagesbyNewsID();
                rptgallery.DataBind(); 
            }


        }
        #region repeater
        protected void rptCat_bound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                HyperLink hplLink = (HyperLink)e.Item.FindControl("hplLink");
                if (hplLink != null)
                {
                    string lang = Page.RouteData.Values["lang"].ToString();
                    hplLink.Text = (lang != "bn")
                        ? DataBinder.Eval(e.Item.DataItem, "name_en").ToString()
                                       : DataBinder.Eval(e.Item.DataItem, "name_bn").ToString();
                    hplLink.NavigateUrl = "/Gallery/" + lang + "/"+ Server.UrlEncode(hplLink.Text)+"/";
                }

            }
            catch (Exception ex)
            { }
        }
        #endregion
    }

}

