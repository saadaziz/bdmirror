﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using MySql.Data.MySqlClient;
using System.Configuration;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
namespace bdmirror24
{
    public partial class testnews : System.Web.UI.Page
    {
        static IList<bdmirrorDB.news> newslist = new List<bdmirrorDB.news>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                newslist = new NewsDAO().All();
                //rptCustomers.DataSource = GetCustomersData(1);
                rptCustomers.DataSource = dtret();
                rptCustomers.DataBind();
            }
        }
        int index = 1;
        public DataTable dtret()
        {
            Trace.Write("dtret");
            DataTable dtnz = new DataTable("news");
            dtnz.Columns.Add("id");
            dtnz.Columns.Add("title");
            dtnz.Columns.Add("content");
            int ind = 1;
            foreach (bdmirrorDB.news nz in newslist)
            {
                if (ind % 60 == 0) break;
                dtnz.Rows.Add(new string[] { nz.news_id.ToString(), nz.title_en, nz.content_en });
                ind++;
            }
            return dtnz;
        }
        public static DataSet GetCustomersData(int pageIndex)
        {
         
            
            DataSet ds = new DataSet();
            //ds.f
            DataTable dtnz = new DataTable("news");
            dtnz.Columns.Add("id");
            dtnz.Columns.Add("title");
            dtnz.Columns.Add("content");
            int ind =new testnews().index ;
             ind  += (pageIndex*10+1);
            foreach (bdmirrorDB.news nz in newslist)
            {
                if (ind % 11 == 0) break;
                dtnz.Rows.Add(new string[] { nz.news_id.ToString(),nz.title_en, nz.content_en });
                ind++;
            }
            ds.Tables.Add(dtnz);
            DataTable dt = new DataTable("PageCount");
            dt.Columns.Add("PageCount");
            dt.Rows.Add();
            dt.Rows[0][0] = pageIndex;
            ds.Tables.Add(dt);
            return ds;
        }

        [WebMethod]
        public static string GetCustomers(int pageIndex)
        {
            return GetCustomersData(pageIndex).GetXml();
        }

    }
}