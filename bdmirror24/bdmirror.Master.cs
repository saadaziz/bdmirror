﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.Manager;
using bdmirror24.DAL.DAL_Functions.Implementations;
using System.Data;
namespace bdmirror24
{
    public partial class bdmirror : System.Web.UI.MasterPage
    {
        static string language = string.Empty;
        private Utility obutility = new Utility();
        public void rpt_larstBind()
        {
            IList<bdmirrorDB.news> lstLatest = new NewsDAO().GetLatestNews();
            DataTable dtcat = new DataTable("dtlatest");
            dtcat.Columns.Add("title_en");
            dtcat.Columns.Add("title_bn");
            dtcat.Columns.Add("id");
            dtcat.Columns.Add("name_bn");
            dtcat.Columns.Add("name_en");
            //IList<bdmirrorDB.categories> allcat = new CategoryDAO().All();
            foreach (bdmirrorDB.news nz in lstLatest)
            {
                dtcat.Rows.Add(new string[] { nz.title_en, nz.title_bn, nz.news_id.ToString(), nz.category_id.name_bn, nz.category_id.name_en });
            }
            rptlatest.DataSource = dtcat;
            rptlatest.DataBind();

        }
        protected void rptlatest_bound(object sender, RepeaterItemEventArgs e)
        {
            HyperLink hplLink = (HyperLink)e.Item.FindControl("hplLink");

            // HyperLink hplLink = (HyperLink) e.Item.FindControl("hplLink");
            if (hplLink != null)
            {
                string lang = "en";
                if (language != "bn") lang = "en";
                else lang = "bn";
                hplLink.Text = (lang != "bn")
                                    ? DataBinder.Eval(e.Item.DataItem, "title_en").ToString()
                                    : DataBinder.Eval(e.Item.DataItem, "title_bn").ToString();
                string cat = (lang != "bn") ? DataBinder.Eval(e.Item.DataItem, "name_en").ToString() :
                    DataBinder.Eval(e.Item.DataItem, "name_bn").ToString();
                hplLink.NavigateUrl = "/news/" + lang + "/" + cat +
                    "/" + DataBinder.Eval(e.Item.DataItem, "id") + "/" + obutility.replaceSpace(hplLink.Text);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                rpt_larstBind();
                DataTable dtcat = new DataTable("dtcat");
                dtcat.Columns.Add("name_en");
                dtcat.Columns.Add("name_bn");
                IList<bdmirrorDB.categories> allcat = new CategoryDAO().All();
                foreach (bdmirrorDB.categories cat in allcat)
                {
                    dtcat.Rows.Add(new string[] { cat.name_en, cat.name_bn });
                }
                #region TopAdd
                IList<bdmirrorDB.all_add> ads = new All_addDAO().getAddbyTitle("Add. in Top");
                foreach (bdmirrorDB.all_add ad in ads)
                {
                    if (ad.addorder == 1)
                    {
                        hpladtop1.NavigateUrl = ad.image_url;
                        imgadtop1.ImageUrl = ad.image_link;
                    }
                    else
                    {
                        hpladtop2.NavigateUrl = ad.image_url;
                        imgadtop2.ImageUrl = ad.image_link;
                    }
                }
                #endregion

                if (Session["lang"] == null)
                {
                    Session.Add("lang", "en");
                    language = "en";
                    lnklang.Text = " English Version ";
                    lnklang.Font.Size = new FontUnit(10);

                    // lnklang.NavigateUrl = "http://localhost:1316.default.aspx?lang=bn";
                }
                if (Session["lang"].ToString() != "bn")
                {
                    language = "en";
                    lnklang.Text = " English Version ";
                    lnklang.Font.Size = new FontUnit(10);

                    //lnklang.NavigateUrl = "http://localhost:1316.default.aspx?lang=bn";
                }
                else
                {
                    Session["lang"] = "bn";
                    lnklang.Text = " বাংলা সংস্করণ";
                    language = "bn";
                    lnklang.Font.Size = new FontUnit(10);

                    //lnklang.NavigateUrl = "http://localhost:1316.default.aspx?lang=en";
                }

                rptcat.DataSource = dtcat;
                rptcat.DataBind();
            }
           
          
            
        }
        #region repeater
        protected void rptCat_bound(object sender, RepeaterItemEventArgs e)
        {
            HyperLink hplLink = (HyperLink)e.Item.FindControl("hplLink");
            if (hplLink != null)
            {
                string lang = "en";
                if (language != "bn") lang = "en";
                else lang = "bn";
                hplLink.Text = (lang != "bn")
                                    ? DataBinder.Eval(e.Item.DataItem, "name_en").ToString()
                                    : DataBinder.Eval(e.Item.DataItem, "name_bn").ToString();
                if ((lang != "bn"))
                    hplLink.NavigateUrl = "/news/" + lang + "/" +
                                     DataBinder.Eval(e.Item.DataItem, "name_en").ToString() + "/";
                else
                    hplLink.NavigateUrl = "/news/" + lang + "/" +
                                    DataBinder.Eval(e.Item.DataItem, "name_bn").ToString() + "/";
                hplLink.Font.Size = (lang == "bn") ? new FontUnit(10) : new FontUnit(12);
                
            }



        }
        protected void change_lang(object sender, EventArgs e)
        {
            String url = HttpContext.Current.Request.Url.AbsolutePath;
            string lang = string.Empty;
            lang = (Page.RouteData.Values["lang"] != null) ? Page.RouteData.Values["lang"].ToString() : "en";
            if (lang != "en")
            {
                Session["lang"] = "en";
                lnklang.Text = " English Version ";
                Response.Redirect(url.Replace("/bn/", "/en/"));
                //hpllang.NavigateUrl = HttpContext.Current.Request.Url.AbsolutePath.Replace("/bn/", "/en/");

            }
            else
            {
                Session["lang"] = "bn";
                lnklang.Text = " বাংলা সংস্করণ";
                Response.Redirect(url.Replace("/en/", "/bn/"));
                //hpllang.NavigateUrl = HttpContext.Current.Request.Url.AbsolutePath.Replace("/en/", "/bn/");
            }

        }
        #endregion
    }
}