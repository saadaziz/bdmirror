﻿<%@ Page Language="C#" Title="Photo Gallery" MasterPageFile="~/bdmirror.Master" AutoEventWireup="true"
    CodeBehind="Gallery.aspx.cs" Inherits="bdmirror24.Gallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="http://www.bdmirror24.com/js/galleria-1.2.9.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
    <style>
        #galleria
        {
            height: 320px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header row-fluid">
        <h1>
            Photo Gallery <small>
                <asp:Label ID="lblcat" runat="server" Text=""></asp:Label>
            </small>
        </h1>
        <br />
    </div>
    <div class="row-fluid">
        <!--page body -->
        <div class="span8">
            <!--span8 start-->
            <div>
                <ul class="thumbnails">
                    <li class="span12">
                        <div class="news_content">
                            <div class="hero-unit">
                                <div class="well">
                                    <!-- here the images will show -->
                                    <div id="galleria">
                                        <asp:Repeater ID="rptgallery" DataSourceID="dssgallery" runat="server">
                                            <ItemTemplate>
                                                <a href='<%# Eval("link") %>'>
                                                    <img src='<%# Eval("link") %>' data-big='<%# Eval("link") %>' data-title='<%# Eval("caption_en") %>'
                                                        data-description='<%# Eval("image_name") %>' />
                                                </a>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <asp:SqlDataSource ID="dssgallery" runat="server" SelectCommand="select image_name,caption_en,caption_bn,link,height,width
                                        from images" ProviderName="System.Data.Odbc" ConnectionString="<%$ ConnectionStrings:bdMirrorDB %>">
                                        </asp:SqlDataSource>
                                        <%--<a href="http://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Biandintz_eta_zaldiak_-_modified2.jpg/800px-Biandintz_eta_zaldiak_-_modified2.jpg">
                <img 
                    src="http://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Biandintz_eta_zaldiak_-_modified2.jpg/1280px-Biandintz_eta_zaldiak_-_modified2.jpg",
                    data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Biandintz_eta_zaldiak_-_modified2.jpg/1280px-Biandintz_eta_zaldiak_-_modified2.jpg"
                    data-title="Biandintz eta zaldiak"
                    data-description="Horses on Bianditz mountain, in Navarre, Spain."
                >
            </a>
            <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Athabasca_Rail_at_Brule_Lake.jpg/800px-Athabasca_Rail_at_Brule_Lake.jpg">
                <img 
                    src="http://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Athabasca_Rail_at_Brule_Lake.jpg/100px-Athabasca_Rail_at_Brule_Lake.jpg",
                    data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Athabasca_Rail_at_Brule_Lake.jpg/1280px-Athabasca_Rail_at_Brule_Lake.jpg"
                    data-title="Athabasca Rail"
                    data-description="The Athabasca River railroad track at the mouth of Brulé Lake in Alberta, Canada."
                >
            </a>
            <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/Back-scattering_crepuscular_rays_panorama_1.jpg/1280px-Back-scattering_crepuscular_rays_panorama_1.jpg">
                <img 
                    src="http://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/Back-scattering_crepuscular_rays_panorama_1.jpg/100px-Back-scattering_crepuscular_rays_panorama_1.jpg",
                    data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/Back-scattering_crepuscular_rays_panorama_1.jpg/1400px-Back-scattering_crepuscular_rays_panorama_1.jpg"
                    data-title="Back-scattering crepuscular rays"
                    data-description="Picture of the day on Wikimedia Commons 26 September 2010."
                >
            </a>
            <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Interior_convento_3.jpg/800px-Interior_convento_3.jpg">
                <img 
                    src="http://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Interior_convento_3.jpg/120px-Interior_convento_3.jpg",
                    data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Interior_convento_3.jpg/1400px-Interior_convento_3.jpg"
                    data-title="Interior convento"
                    data-description="Interior view of Yuriria Convent, founded in 1550."
                >
            </a>
            <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Oxbow_Bend_outlook_in_the_Grand_Teton_National_Park.jpg/800px-Oxbow_Bend_outlook_in_the_Grand_Teton_National_Park.jpg">
                <img 
                    src="http://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Oxbow_Bend_outlook_in_the_Grand_Teton_National_Park.jpg/100px-Oxbow_Bend_outlook_in_the_Grand_Teton_National_Park.jpg",
                    data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Oxbow_Bend_outlook_in_the_Grand_Teton_National_Park.jpg/1280px-Oxbow_Bend_outlook_in_the_Grand_Teton_National_Park.jpg"
                    data-title="Oxbow Bend outlook"
                    data-description="View over the Snake River to the Mount Moran with the Skillet Glacier."
                >
            </a>
            <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Hazy_blue_hour_in_Grand_Canyon.JPG/800px-Hazy_blue_hour_in_Grand_Canyon.JPG">
                <img 
                    src="http://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Hazy_blue_hour_in_Grand_Canyon.JPG/100px-Hazy_blue_hour_in_Grand_Canyon.JPG",
                    data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Hazy_blue_hour_in_Grand_Canyon.JPG/1280px-Hazy_blue_hour_in_Grand_Canyon.JPG"
                    data-title="Hazy blue hour"
                    data-description="Hazy blue hour in Grand Canyon. View from the South Rim."
                >
            </a>
            <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/2909_vallon_moy_res.jpg/800px-2909_vallon_moy_res.jpg">
                <img 
                    src="http://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/2909_vallon_moy_res.jpg/100px-2909_vallon_moy_res.jpg",
                    data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/2909_vallon_moy_res.jpg/1280px-2909_vallon_moy_res.jpg"
                    data-title="Haute Severaisse valley"
                    data-description="View of Haute Severaisse valley and surrounding summits from the slopes of Les Vernets."
                >
            </a>
            <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Bohinjsko_jezero_2.jpg/800px-Bohinjsko_jezero_2.jpg">
                <img 
                    src="http://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Bohinjsko_jezero_2.jpg/100px-Bohinjsko_jezero_2.jpg",
                    data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Bohinjsko_jezero_2.jpg/1280px-Bohinjsko_jezero_2.jpg"
                    data-title="Bohinj lake"
                    data-description="Bohinj lake (Triglav National Park, Slovenia) in the morning."
                >
            </a>
            <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Bowling_Balls_Beach_2_edit.jpg/800px-Bowling_Balls_Beach_2_edit.jpg">
                <img 
                    src="http://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Bowling_Balls_Beach_2_edit.jpg/100px-Bowling_Balls_Beach_2_edit.jpg",
                    data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Bowling_Balls_Beach_2_edit.jpg/1280px-Bowling_Balls_Beach_2_edit.jpg"
                    data-title="Bowling Balls"
                    data-description="Mendocino county, California, USA."
                >
            </a>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div>
            </div>
            <!--thumbnails news endt-->
        </div>
        <!--span8 end-->
        <div class="span4">
            <!--span4 start-->
            <div class="well">
                <h4>
                    Categories</h4>
                <ul>
                    <li>
                        <asp:HyperLink ID="hplallcat" runat="server">ALL</asp:HyperLink>   
                    </li>
                    <asp:Repeater runat="server" ID="rptcat" DataSourceID="dssCat" OnItemDataBound="rptCat_bound">
                        <ItemTemplate>
                            <li>
                                <asp:HyperLink ID="hplLink" runat="server" /></li>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblEmptyData" Text="No Category are there to display" runat="server"
                                Visible="false">
                            </asp:Label>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:SqlDataSource ID="dssCat" runat="server" ProviderName="System.Data.Odbc" ConnectionString="<%$ ConnectionStrings:bdMirrorDB %>"
                        SelectCommand="select id as catid,name_en,name_bn from categories"></asp:SqlDataSource>
                </ul>
            </div>
        </div>
        <!--span4 end-->
    </div>
    <!--page end-->
    <br />
    <script>

        // Load the classic theme
        Galleria.loadTheme('http://www.bdmirror24.com/js/galleria.classic.min.js');

        // Initialize Galleria
        Galleria.run('#galleria');

    </script>
</asp:Content>
