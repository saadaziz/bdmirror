﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bdmirror.Master" AutoEventWireup="true"
    CodeBehind="details.aspx.cs" Inherits="bdmirror24.details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="http://ct1.addthis.com/static/r07/core068.js" />
    <script type="text/javascript">

        !function ($) {
            $(function () {
                $('#myCarousel').carousel({
                    interval: 2000
                });
            })
        } (window.jQuery)
    
    </script>
    <style>
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header row-fluid">
        <h1>
            News <small>
                <asp:Label ID="lblcat" runat="server" Text=""></asp:Label>
            </small>
        </h1>
        <br />
    </div>
    <div class="row-fluid">
        <!--page body -->
        <div class="span8">
            <!--span8 start-->
            <div>
                <div class="thumbnail">
                    <div class="news_content">
                        <ul class="thumbnails">
                            <li class="span3 "><strong>
                                <asp:Label ID="lbltime_name" runat="server" Text="Date of Publish"></asp:Label>
                            </strong><i class="icon-calendar"></i>
                                <asp:HyperLink ID="hpltime" CssClass="lightsilver" runat="server"></asp:HyperLink>
                            </li>
                            <li class="span3"><strong>
                                <asp:Label ID="lbl_wirter_tag" runat="server" Text="Writer"></asp:Label>
                            </strong>
                                <asp:Label ID="lblwriter" CssClass="lightsilver" runat="server" Text="Writer"></asp:Label>
                            </li>
                            <li class="span2"><strong>
                                <asp:Label ID="lbldis" runat="server" Text="District"></asp:Label>
                            </strong>
                                <asp:Label ID="lbldistrict" CssClass="lightsilver" runat="server" Text="Label"></asp:Label>
                            </li>
                            <li><strong>
                                <asp:Label ID="Label1" runat="server" Text="Country"></asp:Label>
                                <asp:HyperLink ID="hplcountry" CssClass="lightsilver" runat="server"></asp:HyperLink>
                            </strong></li>
                        </ul>
                        <h3 class="span12">
                            <asp:HyperLink ID="hpltitle" runat="server"></asp:HyperLink>
                        </h3>
                        <!-- AddThis Button BEGIN -->
                        <asp:Panel ID="pnlimage" runat="server">
                            <br />
                            <div class="well">
                                <div id="myCarousel" class="carousel slide">
                                    <!-- Carousel items -->
                                    <div class="pull-left carousel-inner">
                                        <asp:Repeater runat="server" ID="rptimg" DataSourceID="dssimg" OnItemDataBound="rptimg_bound">
                                            <ItemTemplate>
                                                <asp:Panel ID="pnlmain" runat="server">
                                                    <asp:Image ID="img" runat="server" />
                                                    <div class="carousel-caption">
                                                        <h4>
                                                            <asp:Label ID="lblcap" runat="server" Text="my caption"></asp:Label>
                                                        </h4>
                                                    </div>
                                                </asp:Panel>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblEmptyData" Text="No Images are there to display" runat="server"
                                                    Visible="false">
                                                </asp:Label>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                        <asp:SqlDataSource ID="dssimg" runat="server" ProviderName="System.Data.Odbc" ConnectionString="<%$ ConnectionStrings:bdMirrorDB %>">
                                        </asp:SqlDataSource>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <br />
                        <br />

                        <hr class="pull-right">
                    </div>
                    
                        <br />
                    <asp:Panel runat="server" ID="pnlcontents" CssClass="toppadding_details">
                        <asp:Label ID="lblcontent" runat="server" Text="" CssClass="toppadding_details newscontent_corbel"></asp:Label>
                    </asp:Panel>
                </div>
                <div class="fb-like" data-href='<%= HttpContext.Current.Request.Url.AbsoluteUri %>'
                    data-send="true" data-width="600" data-show-faces="true">
                </div>
                <br />
                <div class="addthis_toolbox addthis_default_style ">
                    <a class="addthis_button_email">Email</a> <a class="addthis_button_linkedin_counter">
                    </a><a class="addthis_button_tweet"></a><a class="addthis_button_pinterest_pinit">
                    </a><a class="addthis_counter addthis_pill_style"></a>
                </div>
                <script type="text/javascript">                    var addthis_config = { "data_track_addressbar": true };</script>
                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5146e8fb5cc4dcdf"></script>
                <!-- AddThis Button END -->
                <br />
                <%# HttpContext.Current.Request.Url.AbsoluteUri %>
                <div class="fb-comments" data-href='<%= HttpContext.Current.Request.Url.AbsoluteUri %>'
                    data-width="680" data-num-posts="10">
                </div>
            </div>
        </div>
        <!--thumbnails news endt-->
        
        <div class="span4">
            <!--span4 start-->
            <div class="wellbackground centered">
                <div class="tabbable">
                    <!--tab menu-->
                    <ul class="nav nav-tabs" id="Ul1">
                        <li><a href="#most" class="active" data-toggle="tab">Top News</a></li>
                        <li><a href="#topnews" data-toggle="tab">Latest News</a></li>
                        <li><a href="#latest" data-toggle="tab">Most Read</a></li>
                    </ul>
                    <div class="tab-content row-fluid">
                        <!--tab-->
                        <div class="tab-pane active" id="most">
                            <div id="accordion4" class="accordionsaad" style="width: 100%">
                                <asp:Repeater ID="rptTopNews" runat="server" OnItemDataBound="rpttop_bound">
                                    <ItemTemplate>
                                        <h5>
                                            <asp:HyperLink ID="hpltoptitle" runat="server"></asp:HyperLink></h5>
                                        <div>
                                            <p>
                                                <asp:Image ID="Image117" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                    AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" /><br>
                                                <asp:Label ID="lblcontent" runat="server" Text=""></asp:Label>
                                            </p>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        <div class="tab-pane" id="topnews">
                            <div id="accordion5" class="accordionsaad" style="width: 100%">
                                <asp:Repeater ID="rptLatestRight" runat="server" OnItemDataBound="rpttop_bound">
                                    <ItemTemplate>
                                        <h5>
                                            <asp:HyperLink ID="hpltoptitle" runat="server"></asp:HyperLink></h5>
                                        <div>
                                            <p>
                                                <asp:Image ID="Image117" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                    AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" /><br>
                                                <asp:Label ID="lblcontent" runat="server" Text=""></asp:Label>
                                            </p>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        <div class="tab-pane" id="latest">
                            <div id="accordion7" class="accordionsaad" style="width: 100%">
                                <asp:Repeater ID="rptmostread" runat="server" OnItemDataBound="rptmostread_bound">
                                    <ItemTemplate>
                                        <h5>
                                            <asp:HyperLink ID="hpltoptitle" runat="server"></asp:HyperLink></h5>
                                        <div>
                                            <p>
                                                <asp:Image ID="Image117" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                    AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" /><br>
                                                <asp:Label ID="lblcontent" runat="server" Text="Label"></asp:Label>
                                            </p>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                    <!--tab-->
                </div>
                <!--tab menu-->
            </div>
        </div>
        <!--span8 end-->
        <!--span4 end-->
        <!--page end-->
        <br />
        <link rel="stylesheet" href="css/jquery-ui.css" />
        <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
        <script type="text/javascript" src="http://www.bdmirror24.com/js/bootstrap-carousel.js"></script>
    </div>
</asp:Content>
