﻿<%@ Page Language="C#" MasterPageFile="~/bdmirror.Master" AutoEventWireup="true" CodeBehind="Test2.aspx.cs" Inherits="bdmirror24.Test2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="page-header">
        <h1>
            News <small>International</small></h1>
        <br />
    </div>
    <div class="row-fluid">
        <!--page body -->
        <div class="span8">
            <!--span8 start-->
            <div>
                <ul >
                    <li class="span12">
                        
                            <div class="news_content">

    <div class="hero-unit">
        <div id="myCarousel" class="carousel slide">
            <%--        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>--%>
            <!-- Carousel items -->
            <div class="row-fluid">
                <!--row fluid main-->
                <div class="">
                    <!--main left column-->
                    <div class="row-fluid">
                        <div class="">
                            <div class="carousel-inner">
                                <div class="active item">
                                    <img src="http://d30fl32nd2baj9.cloudfront.net/media/2013/02/25/pmphotobybss-kallol-2.jpg/ALTERNATES/w320/PM%2Bphoto%2Bby%2BBSS-Kallol-2.jpg"
                                        alt="" /><div class="carousel-caption">
                                            <p>
                                                news caption
                                            </p>
                                        </div>
                                </div>
                                <div class="item">
                                    <img src="http://d30fl32nd2baj9.cloudfront.net/media/2013/02/24/29_manikgonj-clash_240213.jpg1/ALTERNATES/w640/29_Manikgonj+Clash_240213.jpg"
                                        alt="" /><div class="carousel-caption">
                                            <p>
                                            </p>
                                        </div>
                                </div>
                                <div class="item">
                                    <img src="img/Lighthouse.jpg" alt="" /><div class="carousel-caption">
                                        <p>
                                            news caption
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Carousel nav -->
            <%-- <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
            <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>--%>
        </div>
    </div></div>
                        
                    </li>
                </ul>
            </div>
            <div>
                <ul class="thumbnails">
                    <li class="span12">
                        <div class="thumbnail">
                            <div class="news_content">
                                <ul>
                                    <li class="span2">
                                    <asp:Label ID="lbltime_name" runat="server" Text="Date of Publish"></asp:Label>
                                    <asp:Label ID="lbltime" runat="server" Text=""></asp:Label>
                                </li>
                                <li class="span2">
                                    <asp:Label ID="lblwriter" runat="server" Text="Writer"></asp:Label>
                                </li>
                                </ul>
                                <h3 class="span12">
                                    <asp:HyperLink ID="hpltitle" runat="server"></asp:HyperLink>
                                    </h3>
                                <p class="toppadding">
                                    <asp:Label ID="lblcontent" runat="server" Text=""></asp:Label></p>                              
                                </div>
                        </div>
                    </li>
                </ul>
            </div>
            
            <!--thumbnails news endt-->
        </div>
        <!--span8 end-->
        <div class="span4">
            <!--span4 start-->
            <div class="well">
                <h4>
                    Top News</h4>
                <ul>
                    <li><a href="detail.php?nid=1&amp;cid=1">Syrian jets bomb rebel bases near Turkey border</a></li>
                    <li><a href="detail.php?nid=3&amp;cid=1">Explosives found under car of prominent Pakistanijournalist Explosives fdhdh</a></li>
                    <li><a href="detail.php?nid=5&amp;cid=2">CYCLE RALLYorganized to raise voice against War Criminals</a></li>
                    <li><a href="detail.php?nid=36&amp;cid=2">Ctg flyover gets new project director</a></li>
                    <li><a href="detail.php?nid=47&amp;cid=6">Marines establish first operational squad of F-35 fighters</a></li>
                </ul>
            </div>
        </div>
        <!--span4 end-->
    </div>
    <!--page end-->
    <br />
    
        <script  type="text/javascript" src="http://localhost:1316/js/bootstrap-carousel.js"></script>
    <script type="text/javascript">

        !function ($) {
            $(function () {
                $('#myCarousel').carousel({
                    interval: 2000
                });
            })
        } (window.jQuery)
    
    </script>
</asp:Content>
