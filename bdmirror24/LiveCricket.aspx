﻿<%@ Page Language="C#" MasterPageFile="~/bdmirror.Master" Title="Live Cricket :: BdMirror24.com"
 AutoEventWireup="true" CodeBehind="LiveCricket.aspx.cs" Inherits="bdmirror24.LiveCricket" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta name="description" content="All the tv channels for watching Cricket." />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header row-fluid">
        <h1>
            Live <small>
                Cricket</small></h1>
        <br />
    </div>
    <div class="row-fluid">
        <!--page body -->
        <div class="span8">
            <!--span8 start-->
       <center>
           <asp:Button ID="btnsony" runat="server" Text="Sony Max(IPL)" OnClick="channelchange" CssClass="btn-info" />
           <asp:Button ID="btnstarsports" runat="server" Text="Star Sports" CssClass="btn-info" OnClick="channelchange"  />
           <asp:Button ID="btnespn" runat="server" Text="ESPN" CssClass="btn-info"  OnClick="channelchange"/>
           <asp:Button ID="btntenccric" runat="server" Text="Ten Cricket" CssClass="btn-info" OnClick="channelchange" />
           <asp:Button ID="btnstarcric" runat="server" Text="Star Cricket"  CssClass="btn-info" OnClick="channelchange"  />
           <asp:Button ID="btnHD" runat="server" Text="HD Coverage" CssClass="btn-info" OnClick="channelchange"  />
           <br />
           <br />
           <br />
       <iframe id="iframecricket" runat="server" FRAMEBORDER="0" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="NO" WIDTH="600" HEIGHT="420" ></iframe>
       </center>
            <!--thumbnails news endt-->
        </div>
        <!--span8 end-->
        <div class="span4">
            <!--span4 start-->
            <asp:Repeater ID="rptaddfooter" DataSourceID="dssimgaddfooter" runat="server">
                    <ItemTemplate>
                        
                            <a href='<%# Eval("image_url") %>'>
                                <img src='<%# Eval("image_link") %>' alt="Advertisements" />
                            </a>
                        <br />
                    </ItemTemplate>
                </asp:Repeater>
                <asp:SqlDataSource ID="dssimgaddfooter" runat="server" SelectCommand="select title,image_link,image_url,addorder
                                        from all_add where title='Add. in tv' order by addorder"
                    ProviderName="System.Data.Odbc" ConnectionString="<%$ ConnectionStrings:bdMirrorDB %>">
                </asp:SqlDataSource>
        </div>
        <!--span4 end-->
    </div>
    <!--page end-->
    <br />
    </div>
</asp:Content>
