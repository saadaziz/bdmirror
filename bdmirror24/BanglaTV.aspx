﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/bdmirror.Master"  Title="Bangla TV Channels :: Bdmirror24.com" CodeBehind="BanglaTV.aspx.cs" Inherits="bdmirror24.BanglaTV" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta name="description" content="All the tv channels for watching Bengali Televisions." />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header row-fluid">
        <h1>
            Bangla <small>
                TV Channels</small></h1>
        <br />
    </div>
    <div class="row-fluid">
        <!--page body -->
        <div class="span8">
            <!--span8 start-->
       <center>

           <asp:Button ID="btnstarsports" runat="server" Text="Star Sports" CssClass="btn-info" OnClick="channelchange"  />
           <asp:Button ID="btnespn" runat="server" Text="ESPN" CssClass="btn-info"  OnClick="channelchange"/>
           <asp:Button ID="btntensports" runat="server" Text="Ten Sports" CssClass="btn-info" OnClick="channelchange"  />

           <asp:Button ID="btnHD" runat="server" Text="HD Coverage" CssClass="btn-info" OnClick="channelchange"  />
           <br />
           <br />
           <br />
       <iframe id="iframecricket" runat="server" FRAMEBORDER="0" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="NO" WIDTH="600" HEIGHT="420" ></iframe>
       </center>
            <!--thumbnails news endt-->
        </div>
        <!--span8 end-->
        <div class="span4">
            <!--span4 start-->
            
        </div>
        <!--span4 end-->
    </div>
    <!--page end-->
    <br />
    </div>
</asp:Content>
