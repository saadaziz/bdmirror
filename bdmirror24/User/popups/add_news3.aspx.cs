﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
using System.IO;
namespace bdmirror24.User.popups
{
    public partial class add_news3 : System.Web.UI.Page
    {
        NewsDAO ndao = new NewsDAO();
        ImageDAO imgdao = new ImageDAO();
        static bdmirrorDB.news news = new bdmirrorDB.news();
        static bdmirrorDB.images contents = new bdmirrorDB.images();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    news = ndao.pickbyID(int.Parse(Request.QueryString["id"]));
                    IList<bdmirrorDB.images> images = imgdao.GetImagesbyNewsID(news);
                    foreach (bdmirrorDB.images img in images)
                        txtimages.Text += img.image_id + ",";
                }
            }
        }
        protected void proceed_next(object sender, EventArgs e)
        {
            try
            {
                string[] imagesstr = (txtimages.Text).Split(',');
                foreach (string str in imagesstr)
                {
                    if (str != "")
                    {
                        bdmirrorDB.images images = imgdao.pickbyID(int.Parse(str));
                        images.news_id = news;
                        imgdao.update(images);
                    }

                }
                hplNewsLink.NavigateUrl = "http://www.bdmirror24.com/news/en/" + news.category_id.name_en + "/" + news.news_id + "/" + new bdmirror24.Utility().replaceSpace(news.title_en);
                hplNewsLink.Text = (news.title_en != null) ? news.title_en : news.title_bn;
                hplmore.NavigateUrl = "~/User/popups/add_news1.aspx?cat=" + news.category_id.name_en;
                mulNews.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                mulNews.ActiveViewIndex = 2;
                lblerror_msg.Text = ex.Message;
            }

        }
        protected void UploadButton_Click(object sender, EventArgs e)
        {

            if (fileUpload.HasFile)
            {

                string filename = Path.GetFileName(fileUpload.FileName);
                lblimagename.Text = filename;
                if (lblimagename.Text != string.Empty)
                {
                    try
                    {
                        Utility obuitility = new Utility();
                        string fileExt = Path.GetExtension(fileUpload.FileName);
                        string path = Server.MapPath("../../Uploads") + "\\" + (filename);
                        fileUpload.SaveAs(path);
                        if (chkimage.Checked == true)
                        {
                            bdmirrorDB.images images = new bdmirrorDB.images();
                            images.caption_bn = txtcapbn.Text;
                            images.caption_en = txtcapen.Text;
                            images.image_name = lblimagename.Text;
                            images.link = "http://www.bdmirror24.com/" + "Uploads/" + (lblimagename.Text) ;
                            System.Drawing.Image objImage = System.Drawing.Image.FromFile(path);
                            images.width = objImage.Width;
                            images.height = objImage.Height;
                            images.time = DateTime.Now;

                            images = imgdao.Add_ret(images);
                            txtimages.Text += images.image_id + ",";


                        }
                        hpllink.Text = filename;
                        hpllink.NavigateUrl = "http://www.bdmirror24.com/" + "Uploads/" + filename;

                        StatusLabel.Text = "Upload status: File uploaded!";

                    }
                    catch (Exception ex)
                    {
                        StatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
                    }
                }
                else
                    StatusLabel.Text = "Please insert the name here";
            }
        }
    }
}