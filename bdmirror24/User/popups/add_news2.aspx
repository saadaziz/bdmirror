﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add_news2.aspx.cs" MasterPageFile="~/User/Admin_master.Master"
 Inherits="bdmirror24.User.add_news2"  ValidateRequest="false" %>

<asp:content runat="server" contentplaceholderid="head" id="idhead">
</asp:content>
<asp:content id="idcnt" contentplaceholderid="cphBody" runat="server">
    <div class="row-fluid">
                <div class="span2">
                    News English</div>
                <div class="span8">
                    <textarea id="elm1" name="elm1" rows="13" cols="180" style="width: 100%" runat="server"></textarea>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span2">
                    News Bangla</div>
                <div class="span8">
                    <textarea id="elm2" name="elm2" rows="13" cols="180" style="width: 100%" runat="server"></textarea>
                </div>
            </div>
            <br />
            <br />
             <div class="row-fluid pull-right">
                <asp:Button ID="Button1"  runat="server" CssClass="span2 btn-info pull-right" Text="Proceed" OnClick="SaveNews_click" /></div>
            
    <script type="text/javascript" src="../../tinymce/jscripts/tiny_mce/tinymce.min.js"></script>
   <script type="text/javascript">
       tinymce.init({
           selector: "textarea",
           plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
           toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
       });
</script>

</asp:content>
