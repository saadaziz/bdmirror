﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
using bdmirror24.DAL.Manager;
using System.IO;
namespace bdmirror24.User.popups
{
    public partial class edit_news : System.Web.UI.Page
    {
       
        KeywrdsDAO kdao= new KeywrdsDAO();
        NewsDAO newsdao =new NewsDAO();
        ImageDAO imgdao=new ImageDAO();
        protected void Page_Load(object sender, EventArgs e)
        {
            //txtdate.ToolTip += DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year;
            //var link = new HtmlGenericControl("script");
            //link.Attributes.Add("src", ResolveUrl("~/tinymce/jscripts/tiny_mce/tiny_mce.js"));
            //link.Attributes.Add("type", "text/javascript");


            if (!IsPostBack)
            {
                //Utility.BindCountryDDL(ddlcountry);
                //Utility.BindDivisionDDL(ddlDivision, 0);
               // Utility.BindDistrictDDL(ddlDistrict, 0);
                //ddlDistrict.Items.Insert(0, new ListItem("   <-- Select District -->", "-1"));
                //clndr.Focus();
                mulNews.ActiveViewIndex = 0;
                //Utility.BindUpazillaPSDDL(ddlUpzilla, 0);
                if (Request.QueryString["cat"] != null)
                    lblcategry.Text = Server.UrlDecode(Request.QueryString["cat"].ToString());
                //else
                //    Response.Redirect("http://www.bdmirror24.com/User/home");
                if (Request.QueryString["id"] != null)
                    LoadControlState(int.Parse(Request.QueryString["id"].ToString()));
            }
          

            
        }
        public void LoadControlState(int newsid)
        {
            if (newsid != 0)
            {
                bdmirrorDB.news news = newsdao.pickbyID(newsid);
                txttitle_en.Text = news.title_en;
                txttitle_bn.Text = news.title_bn;
                txtdate.Text =news.date.Day+"/" +news.date.Month+"/"+news.date.Year;
                txtwriter.Text = news.writer_name;
                chkannonymous.Checked = (news.anonymous == "1") ? true : false;
                chktopnews.Checked = (news.top_news == "1") ? true : false;
                chkpublished.Checked = (news.published == "1") ? true : false;
                lblcategry.Text = (news.category_id != null) ? news.category_id.name_en : "";
                ddlcountry.SelectedValue = (news.country_id != null) ?
                    new CountryDAO().pickbyID(news.country_id.country_id).country_id.ToString() : "-1";
                ddlDistrict.SelectedValue = (news.district_id != null) ?
                    new DistrictDAO().pickbyID(news.district_id.DistrictID).DistrictID.ToString() : "-1";
                
                bdmirrorDB.contents cont = new ContentDAO().GetContentbyNewsID(news);
                elm1.Value = cont.english_content;
                elm2.Value = cont.bangla_content;

                IList<bdmirrorDB.keywords> keys = kdao.GetKeywordsbyNewsID(news);
                foreach (bdmirrorDB.keywords key in keys)
                {
                    string delimit = (key.keyword_id == keys.First().keyword_id) ? "" : ",";
                    if (key.keyword != "")
                        txtkeyword_en.Text = key.keyword + delimit + txtkeyword_en.Text;
                }
                IList<bdmirrorDB.keywords_bn> keysbn = new KeywrdsbnDAO().GetKeywordsbyNewsID(news);

                foreach (bdmirrorDB.keywords_bn key in keysbn)
                {
                    string delimit = (key.keyword_id == keysbn.First().keyword_id) ? "" : ",";
                    if (key.keyword != "")
                        txtkeyword_en.Text = key.keyword + delimit + txtkeyword_en.Text;
                }
                //delete keywords here//
                new KeywordManager().deleteAllKeyword(newsid);

                IList<bdmirrorDB.images> images = imgdao.GetImagesbyNewsID(news);
                foreach (bdmirrorDB.images img in images)
                    txtimages.Text += img.image_id + ",";
            }
        }
        public string removehtml(string cont1)
        {
           // System.Text.RegularExpressions.Regex.Replace(cont1, "<.*?>", string.Empty);
            return System.Text.RegularExpressions.Regex.Replace(cont1, "<.*?>", string.Empty); ;
        }
        public string get_less(string cont)
        {

            int count = 0;
            cont = removehtml(cont);
            char[] arr = cont.ToCharArray();
            string cen = string.Empty;
            foreach (char VARIABLE in arr)
            {
                cen += VARIABLE;
                if (count > 320) break;
                count++;
            }
            return cen;
        }

        public void FormToObjectNews(long id,bdmirrorDB.news temp)
        {
            //bdmirrorDB.news temp = new bdmirrorDB.news();
            if (id > 0)
                temp.news_id = id;
            temp.content_en = get_less(elm1.Value);
            temp.content_bn = get_less(elm2.Value);
            temp.title_bn = txttitle_bn.Text;
            temp.title_en = txttitle_en.Text;
            temp.top_news = (chktopnews.Checked) ? "1" : "0";
            temp.published = (chkpublished.Checked) ? "1" : "0";
            temp.anonymous = (chkannonymous.Checked) ? "1" : "0";
            temp.latest =  "0";
            temp.insert_time = DateTime.Now;
            temp.writer_name = txtwriter.Text;
            temp.district_id = (ddlDistrict.SelectedValue != "-1" ) ? new DistrictDAO().pickbyID(int.Parse(ddlDistrict.SelectedValue)) : null;
            temp.country_id = (ddlcountry.SelectedValue != "-1" ) ? new CountryDAO().pickbyID(int.Parse(ddlcountry.SelectedValue)) : null;
            //temp.w
            temp.date = DateTime.ParseExact(txtdate.Text, "d/M/yyyy", CultureInfo.InvariantCulture);
            temp.category_id = new CategoryDAO().getcatbyName(lblcategry.Text);

           // return null;
        }
        static bdmirrorDB.news saved =new bdmirrorDB.news() ;
        protected void btnUpdate_click(object sender, EventArgs e)
        {
            //try
            //{
                int ret = (Request.QueryString["id"] != null) ? int.Parse(Request.QueryString["id"].ToString()) : 0;
                
                bdmirrorDB.contents cont_temp = null;
                if (ret != 0)
                {
                    saved = newsdao.pickbyID(ret);
                    FormToObjectNews(ret, saved);
                    newsdao.update(saved);
                    //
                    cont_temp = new ContentDAO().GetContentbyNewsID(saved);
                }
                else
                {
                    FormToObjectNews(ret, saved);
                     newsdao.Add_ret(saved);
                    cont_temp = new bdmirrorDB.contents();
                }
                if (saved.news_id != 0)
                {
                    //save contents
                    
                    cont_temp.english_content = elm1.Value;
                    cont_temp.bangla_content = elm2.Value;
                    cont_temp.news_id = saved;
                    if (cont_temp.content_id != 0)
                    {
                        new ContentDAO().update(cont_temp);
                        saved.contents = cont_temp;
                        newsdao.update(saved);
                    }
                    else
                    {
                        cont_temp = new ContentDAO().Add_ret(cont_temp);
                        saved.contents = cont_temp;
                        newsdao.update(saved);
                    }

                    //update images
                    
                    //save keywords
                    string[] keys = (txtkeyword_bn.Text).Split(',');
                    foreach (string str in keys)
                    {
                        if(str!="")
                        {
                            bdmirrorDB.keywords keywords = new bdmirrorDB.keywords();
                            keywords.keyword = str;
                            keywords.news_id = saved;
                            kdao.Add(keywords);
                        }
                        
                    }
                    keys = (txtkeyword_en.Text).Split(',');
                    foreach (string str in keys)
                    {
                        if (str != "")
                        {
                            bdmirrorDB.keywords keywords = new bdmirrorDB.keywords();
                            keywords.keyword = str;
                            keywords.news_id = saved;
                            kdao.Add(keywords);
                        }

                    }
                    hplNewsLink.NavigateUrl = "http://www.bdmirror24.com/news/en/" + saved.category_id.name_en + "/" + saved.news_id + "/" + new bdmirror24.Utility().replaceSpace(saved.title_en);
                    hplNewsLink.Text = (saved.title_en != null) ? saved.title_en : saved.title_bn;
                    //new bdmirror24.couchbase.couchbasemain().setnews(saved);
                    mulNews.ActiveViewIndex = 3;

                }
            //}
            //catch (Exception ex)
            //{
            //    mulNews.ActiveViewIndex = 2;
            //    lblerror_msg.Text = ex.Message;
            //}
        }
        protected void ddlindex_changed(object sender, EventArgs e)
        {
            //if (((DropDownList)sender).DataTextField.Equals("country_name"))
            //{
            //    if (((DropDownList)sender).SelectedItem.Text == "Bangladesh ")
            //    {
            //        Utility.BindDivisionDDL(ddlDivision, 0);
            //        Utility.BindDistrictDDL(ddlDistrict, 0);
            //    }
            //}
            //if (((DropDownList)sender).DataTextField.Equals("divName"))
            //{
            //}
            Utility.BindDistrictDDL(ddlDistrict, int.Parse(((DropDownList)sender).SelectedValue));
            
        }
        protected void UploadButton_Click(object sender, EventArgs e)
        {
            if (fileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(fileUpload.FileName);
                    string path = Server.MapPath("../../../Uploads") + "\\" + filename;
                    fileUpload.SaveAs(path);

                    if (chkimage.Checked == true)
                    {
                        bdmirrorDB.images images = new bdmirrorDB.images();
                        images.caption_bn = txtcapbn.Text;
                        images.caption_en = txtcapen.Text;
                        images.image_name = txtimagename.Text;
                        images.link = "http://www.bdmirror24.com/" + "Uploads/" + filename;
                        System.Drawing.Image objImage = System.Drawing.Image.FromFile(path);
                        images.width = objImage.Width;
                        images.height = objImage.Height;
                        images.time = DateTime.Now;

                        images=imgdao.Add_ret(images);
                        txtimages.Text += images.image_id + ",";

                        
                    }
                    hpllink.Text = filename;
                    hpllink.NavigateUrl = "http://www.bdmirror24.com/" + "Uploads/" + filename;

                    StatusLabel.Text = "Upload status: File uploaded!";

                }
                catch (Exception ex)
                {
                    StatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
                }
            }
        }
        #region Methods For Button
        string strSearch = string.Empty;
        protected void proceed_next(object sender, EventArgs e)
        {
            try
            {
                string[] imagesstr = (txtimages.Text).Split(',');
                foreach (string str in imagesstr)
                {
                    if (str != "")
                    {
                        bdmirrorDB.images images = imgdao.pickbyID(int.Parse(str));
                        images.news_id = saved;
                        imgdao.update(images);
                    }

                }
                mulNews.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                mulNews.ActiveViewIndex = 2;
                
            }

        }
        #endregion
       
    }
}