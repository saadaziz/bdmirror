﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
using bdmirror24.DAL.Manager;
using System.IO;
using System.Globalization;
namespace bdmirror24.User.popups
{
    public partial class add_news1 : System.Web.UI.Page
    {

        KeywrdsDAO kdao = new KeywrdsDAO();
        NewsDAO newsdao = new NewsDAO();
        ContentDAO contdao = new ContentDAO();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Utility.BindCountryDDL(ddlcountry);
                //Utility.BindDivisionDDL(ddlDivision, 0);
                // Utility.BindDistrictDDL(ddlDistrict, 0);
                //ddlDistrict.Items.Insert(0, new ListItem("   <-- Select District -->", "-1"));
                //clndr.Focus();
                //Utility.BindUpazillaPSDDL(ddlUpzilla, 0);
                if (Request.QueryString["cat"] != null)
                    lblcategry.Text = Server.UrlDecode(Request.QueryString["cat"].ToString());
                if (Request.QueryString["id"] != null)
                    LoadControlState(int.Parse(Request.QueryString["id"].ToString()));
            }
        }
        protected void ddlindex_changed(object sender, EventArgs e)
        {

            Utility.BindDistrictDDL(ddlDistrict, int.Parse(((DropDownList)sender).SelectedValue));

        }
        static bdmirrorDB.news saved = new bdmirrorDB.news();
        protected void SaveNews_click(object sender, EventArgs e)
        {
            int ret = (Request.QueryString["id"] != null) ? int.Parse(Request.QueryString["id"].ToString()) : 0;

            bdmirrorDB.contents contents = new bdmirrorDB.contents();
            
            if (ret != 0)
            {
                saved = newsdao.pickbyID(ret);
                FormToObjectNews(ret, saved);
                newsdao.update(saved);
                
                //
                //cont_temp = new ContentDAO().GetContentbyNewsID(saved);
            }
            else
            {
                FormToObjectNews(ret, saved);
                newsdao.Add_ret(saved);
                contents.news_id = saved;
                contents.english_content="";
                contents.bangla_content="";
                contdao.Add_ret(contents); saved.contents = contents;
                newsdao.update(saved);
                //cont_temp = new bdmirrorDB.contents();
            }


            //save keywords
            if (txtkeyword_bn.Text != string.Empty)
            {
                string[] keys = (txtkeyword_bn.Text).Split(',');
                foreach (string str in keys)
                {
                    if (str != "")
                    {
                        bdmirrorDB.keywords keywords = new bdmirrorDB.keywords();
                        keywords.keyword = str;
                        keywords.news_id = saved;
                        kdao.Add(keywords);
                    }

                }
            }
            if (txtkeyword_en.Text != string.Empty)
            {
                string[] keys = (txtkeyword_en.Text).Split(',');
                foreach (string str in keys)
                {
                    if (str != "")
                    {
                        bdmirrorDB.keywords keywords = new bdmirrorDB.keywords();
                        keywords.keyword = str;
                        keywords.news_id = saved;
                        kdao.Add(keywords);
                    }

                }
            }
            
            Response.Redirect("~/User/popups/add_news2.aspx?id=" + saved.news_id);
        }
        public void FormToObjectNews(long id, bdmirrorDB.news temp)
        {
            //bdmirrorDB.news temp = new bdmirrorDB.news();
            if (id > 0)
                temp.news_id = id;
            temp.title_bn = txttitle_bn.Text;
            temp.title_en = txttitle_en.Text;
            temp.top_news = (chktopnews.Checked) ? "1" : "0";
            temp.published = (chkpublished.Checked) ? "1" : "0";
            temp.anonymous = (chkannonymous.Checked) ? "1" : "0";
            temp.latest = "0";
            temp.insert_time = DateTime.Now;
            temp.writer_name = txtwriter.Text;
            temp.district_id = (ddlDistrict.SelectedValue != "-1") ? new DistrictDAO().pickbyID(int.Parse(ddlDistrict.SelectedValue)) : null;
            temp.country_id = (ddlcountry.SelectedValue != "-1") ? new CountryDAO().pickbyID(int.Parse(ddlcountry.SelectedValue)) : null;
            //temp.w
            temp.date = DateTime.ParseExact(txtdate.Text, "d/M/yyyy", CultureInfo.InvariantCulture);
            temp.category_id = new CategoryDAO().getcatbyName(lblcategry.Text);

            // return null;
        }
        public void LoadControlState(int newsid)
        {
            if (newsid != 0)
            {
                bdmirrorDB.news news = newsdao.pickbyID(newsid);
                txttitle_en.Text = news.title_en;
                txttitle_bn.Text = news.title_bn;
                txtdate.Text = news.date.Day + "/" + news.date.Month + "/" + news.date.Year;
                txtwriter.Text = news.writer_name;
                chkannonymous.Checked = (news.anonymous == "1") ? true : false;
                chktopnews.Checked = (news.top_news == "1") ? true : false;
                chkpublished.Checked = (news.published == "1") ? true : false;
                lblcategry.Text = (news.category_id != null) ? news.category_id.name_en : "";
                ddlcountry.SelectedValue = (news.country_id != null) ?
                    new CountryDAO().pickbyID(news.country_id.country_id).country_id.ToString() : "-1";
                ddlDistrict.SelectedValue = (news.district_id != null) ?
                    new DistrictDAO().pickbyID(news.district_id.DistrictID).DistrictID.ToString() : "-1";

                IList<bdmirrorDB.keywords> keys = kdao.GetKeywordsbyNewsID(news);
                foreach (bdmirrorDB.keywords key in keys)
                {
                    string delimit = (key.keyword_id == keys.First().keyword_id) ? "" : ",";
                    if (key.keyword != "")
                        txtkeyword_en.Text = key.keyword + delimit + txtkeyword_en.Text;
                }
                IList<bdmirrorDB.keywords_bn> keysbn = new KeywrdsbnDAO().GetKeywordsbyNewsID(news);

                foreach (bdmirrorDB.keywords_bn key in keysbn)
                {
                    string delimit = (key.keyword_id == keysbn.First().keyword_id) ? "" : ",";
                    if (key.keyword != "")
                        txtkeyword_en.Text = key.keyword + delimit + txtkeyword_en.Text;
                }
                //delete keywords here//
                new KeywordManager().deleteAllKeyword(newsid);
            }
        }
    }
}