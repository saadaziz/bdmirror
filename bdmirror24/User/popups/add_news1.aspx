﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/User/Admin_master.Master"
 CodeBehind="add_news1.aspx.cs" Inherits="bdmirror24.User.popups.add_news1" %>

<asp:Content runat="server" ContentPlaceHolderID="head" ID="idhead">

</asp:Content>
<asp:Content ID="idcnt" ContentPlaceHolderID="cphBody" runat="server">
    <asp:MultiView ID="mulNews" runat="server" ActiveViewIndex="0" >
        <asp:View ID="vinput" runat="server">
            <div class="row-fluid">
                <div class="span2">
                    Category</div>
                <div class="span3">
                    <asp:Label ID="lblcategry" runat="server"></asp:Label>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span2">
                    Title English</div>
                <div class="span3">
                    <asp:TextBox ID="txttitle_en" runat="server" Width="450px"></asp:TextBox>
                    <%-- <asp:RequiredFieldValidator ID="rfValidator1" runat="server" ControlToValidate="txttitle_en"
                CssClass="failureNotification" ErrorMessage="Title is required." 
                ValidationGroup="PartyValidationGroup"><img src="../../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>--%>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span2">
                    Title Bangla</div>
                <div class="span3">
                    <asp:TextBox ID="txttitle_bn" runat="server" Width="450px"></asp:TextBox>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span2">
                    Keyword English</div>
                <div class="span3">
                    <asp:TextBox ID="txtkeyword_en" runat="server" Width="450px"></asp:TextBox>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span2">
                    Keyword Bangla</div>
                <div class="span3">
                    <asp:TextBox ID="txtkeyword_bn" runat="server" Width="450px"></asp:TextBox>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span2">
                    Writer</div>
                <div class="span3">
                    <asp:TextBox ID="txtwriter" runat="server" Width="200px"></asp:TextBox>
                   <%-- <asp:RequiredFieldValidator ID="rfValidator1" runat="server" ControlToValidate="txtwriter"
                        CssClass="failureNotification" ErrorMessage="Writer is required." ValidationGroup="PartyValidationGroup"><img src="../../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>--%>
                </div>
                <div class="span2">
                    Annonymous</div>
                <div class="span3">
                    <asp:CheckBox ID="chkannonymous" runat="server" Enabled="false" />
                </div>
            </div>
                    <div class="row-fluid">
                        <div class="span2">
                            Country</div>
                        <div class="span2">
                            <asp:DropDownList ID="ddlcountry" runat="server" Width="180px">
                                <asp:ListItem Value="-1" Selected="True"> <-- Select Country --></asp:ListItem>
                                <asp:ListItem Value="2">United Arab Emirates </asp:ListItem>
                                <asp:ListItem Value="3">Afghanistan </asp:ListItem>
                                <asp:ListItem Value="11">Argentina </asp:ListItem>
                                <asp:ListItem Value="14">Australia </asp:ListItem>
                                <asp:ListItem Value="16">Azerbaijan </asp:ListItem>
                                <asp:ListItem Value="18">Barbados </asp:ListItem>
                                <asp:ListItem Value="19">Bangladesh </asp:ListItem>
                                <asp:ListItem Value="20">Belgium </asp:ListItem>
                                <asp:ListItem Value="26">Bermuda </asp:ListItem>
                                <asp:ListItem Value="28">Bolivia </asp:ListItem>
                                <asp:ListItem Value="29">Brazil </asp:ListItem>
                                <asp:ListItem Value="31">Bhutan </asp:ListItem>
                                <asp:ListItem Value="32">Bouvet Island </asp:ListItem>
                                <asp:ListItem Value="33">Botswana </asp:ListItem>
                                <asp:ListItem Value="36">Canada </asp:ListItem>
                                <asp:ListItem Value="41">Switzerland </asp:ListItem>
                                <asp:ListItem Value="44">Chile </asp:ListItem>
                                <asp:ListItem Value="45">Cameroon </asp:ListItem>
                                <asp:ListItem Value="46">China </asp:ListItem>
                                <asp:ListItem Value="47">Colombia </asp:ListItem>
                                <asp:ListItem Value="49">Cuba </asp:ListItem>
                                <asp:ListItem Value="52">Cyprus </asp:ListItem>
                                <asp:ListItem Value="53">Czech Republic </asp:ListItem>
                                <asp:ListItem Value="54">Germany </asp:ListItem>
                                <asp:ListItem Value="56">Denmark </asp:ListItem>
                                <asp:ListItem Value="59">Algeria </asp:ListItem>
                                <asp:ListItem Value="62">Egypt </asp:ListItem>
                                <asp:ListItem Value="65">Spain </asp:ListItem>
                                <asp:ListItem Value="66">Ethiopia </asp:ListItem>
                                <asp:ListItem Value="67">Finland </asp:ListItem>
                                <asp:ListItem Value="72">France </asp:ListItem>
                                <asp:ListItem Value="74">United Kingdom </asp:ListItem>
                                <asp:ListItem Value="76">Georgia </asp:ListItem>
                                <asp:ListItem Value="77">French Guiana </asp:ListItem>
                                <asp:ListItem Value="78">Ghana </asp:ListItem>
                                <asp:ListItem Value="85">Greece </asp:ListItem>
                                <asp:ListItem Value="91">Hong Kong </asp:ListItem>
                                <asp:ListItem Value="93">Honduras </asp:ListItem>
                                <asp:ListItem Value="94">Croatia </asp:ListItem>
                                <asp:ListItem Value="95">Haiti </asp:ListItem>
                                <asp:ListItem Value="96">Hungary </asp:ListItem>
                                <asp:ListItem Value="97">Indonesia </asp:ListItem>
                                <asp:ListItem Value="98">Ireland </asp:ListItem>
                                <asp:ListItem Value="99">Israel </asp:ListItem>
                                <asp:ListItem Value="100">India </asp:ListItem>
                                <asp:ListItem Value="102">Iraq </asp:ListItem>
                                <asp:ListItem Value="103">Iran, Islamic Republic Of </asp:ListItem>
                                <asp:ListItem Value="104">Iceland </asp:ListItem>
                                <asp:ListItem Value="105">Italy </asp:ListItem>
                                <asp:ListItem Value="106">Jamaica </asp:ListItem>
                                <asp:ListItem Value="107">Jordan </asp:ListItem>
                                <asp:ListItem Value="108">Japan </asp:ListItem>
                                <asp:ListItem Value="109">Kenya </asp:ListItem>
                                <asp:ListItem Value="111">Cambodia </asp:ListItem>
                                <asp:ListItem Value="113">Comoros </asp:ListItem>
                                <asp:ListItem Value="115">Korea, D.P.R. </asp:ListItem>
                                <asp:ListItem Value="116">Korea, Republic Of </asp:ListItem>
                                <asp:ListItem Value="117">Kuwait </asp:ListItem>
                                <asp:ListItem Value="121">Lebanon </asp:ListItem>
                                <asp:ListItem Value="124">Sri Lanka </asp:ListItem>
                                <asp:ListItem Value="134">Madagascar </asp:ListItem>
                                <asp:ListItem Value="138">Myanmar </asp:ListItem>
                                <asp:ListItem Value="147">Maldives </asp:ListItem>
                                <asp:ListItem Value="149">Mexico </asp:ListItem>
                                <asp:ListItem Value="150">Malaysia </asp:ListItem>
                                <asp:ListItem Value="151">Mozambique </asp:ListItem>
                                <asp:ListItem Value="152">Namibia </asp:ListItem>
                                <asp:ListItem Value="156">Nigeria </asp:ListItem>
                                <asp:ListItem Value="157">Nicaragua </asp:ListItem>
                                <asp:ListItem Value="158">Netherlands </asp:ListItem>
                                <asp:ListItem Value="159">Norway </asp:ListItem>
                                <asp:ListItem Value="160">Nepal </asp:ListItem>
                                <asp:ListItem Value="163">New Zealand </asp:ListItem>
                                <asp:ListItem Value="165">Panama </asp:ListItem>
                                <asp:ListItem Value="169">Philippines </asp:ListItem>
                                <asp:ListItem Value="170">Pakistan </asp:ListItem>
                                <asp:ListItem Value="171">Poland </asp:ListItem>
                                <asp:ListItem Value="175">Palestine </asp:ListItem>
                                <asp:ListItem Value="176">Portugal </asp:ListItem>
                                <asp:ListItem Value="178">Paraguay </asp:ListItem>
                                <asp:ListItem Value="179">Qatar </asp:ListItem>
                                <asp:ListItem Value="181">Romania </asp:ListItem>
                                <asp:ListItem Value="182">Russian Federation </asp:ListItem>
                                <asp:ListItem Value="183">Rwanda </asp:ListItem>
                                <asp:ListItem Value="184">Saudi Arabia </asp:ListItem>
                                <asp:ListItem Value="187">Sudan </asp:ListItem>
                                <asp:ListItem Value="188">Sweden </asp:ListItem>
                                <asp:ListItem Value="189">Singapore </asp:ListItem>
                                <asp:ListItem Value="191">Slovenia </asp:ListItem>
                                <asp:ListItem Value="192">Svalbard And Jan Mayen </asp:ListItem>
                                <asp:ListItem Value="193">Slovakia </asp:ListItem>
                                <asp:ListItem Value="194">Sierra Leone </asp:ListItem>
                                <asp:ListItem Value="196">Senegal </asp:ListItem>
                                <asp:ListItem Value="197">Somalia </asp:ListItem>
                                <asp:ListItem Value="204">Chad </asp:ListItem>
                                <asp:ListItem Value="207">Thailand </asp:ListItem>
                                <asp:ListItem Value="208">Tajikistan </asp:ListItem>
                                <asp:ListItem Value="214">Turkey </asp:ListItem>
                                <asp:ListItem Value="217">Taiwan </asp:ListItem>
                                <asp:ListItem Value="219">Ukraine </asp:ListItem>
                                <asp:ListItem Value="220">Uganda </asp:ListItem>
                                <asp:ListItem Value="222">United States </asp:ListItem>
                                <asp:ListItem Value="223">Uruguay </asp:ListItem>
                                <asp:ListItem Value="224">Uzbekistan </asp:ListItem>
                                <asp:ListItem Value="225">Vatican City </asp:ListItem>
                                <asp:ListItem Value="227">Venezuela </asp:ListItem>
                                <asp:ListItem Value="230">Vietnam </asp:ListItem>
                                <asp:ListItem Value="237">South Africa </asp:ListItem>
                                <asp:ListItem Value="238">Zambia </asp:ListItem>
                                <asp:ListItem Value="239">Zimbabwe </asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span2">
                            Division</div>
                        <div class="span3">
                            <asp:DropDownList ID="ddlDivision" runat="server" Width="180px"
                             AutoPostBack="true" OnSelectedIndexChanged="ddlindex_changed">
                                <asp:ListItem Value="-1" Selected="True"> <-- Select Division --></asp:ListItem>
                                <asp:ListItem Value="1">Dhaka</asp:ListItem>
                                <asp:ListItem Value="2">Barishal</asp:ListItem>
                                <asp:ListItem Value="3">Chittang</asp:ListItem>
                                <asp:ListItem Value="4">Rangpur</asp:ListItem>
                                <asp:ListItem Value="5">Rajshahi</asp:ListItem>
                                <asp:ListItem Value="6">Khulna</asp:ListItem>
                                <asp:ListItem Value="7">Sylhet</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="span2">
                            District</div>
                        <div class="span2">
                            <asp:DropDownList ID="ddlDistrict" runat="server" Width="180px">
                                <asp:ListItem Value="-1" Selected="True"> <-- Select Division --></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
            <div class="row-fluid">
                <div class="span2">
                    Published
                </div>
                <div class="span3">
                    <asp:CheckBox ID="chkpublished" runat="server"  Checked ="true"/>
                </div>
                <div class="span2">
                    Top News
                </div>
                <div class="span3">
                    <asp:CheckBox ID="chktopnews" runat="server" />
                </div>
            </div>
            <div class="row-fluid">
                <div class="span2">
                    Date Of Publish</div>
                <div class="span3">
                    <asp:TextBox ID="txtdate" runat="server"  ></asp:TextBox>
                </div>
            </div>
            <br />
            <div class="row-fluid pull-right">
                <asp:Button ID="Button1"  runat="server" CssClass="span2 btn-info pull-right" Text="Proceed" OnClick="SaveNews_click" /></div>
            </asp:View>
            </asp:MultiView>
            </asp:Content>
