﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
namespace bdmirror24.User
{
    public partial class add_news2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                    LoadControlState(int.Parse(Request.QueryString["id"].ToString()));
            }
        }
        NewsDAO ndao = new NewsDAO();
        ContentDAO cdao = new ContentDAO();
        static bdmirrorDB.news news = new bdmirrorDB.news();
        static bdmirrorDB.contents contents = new bdmirrorDB.contents();
        protected void SaveNews_click(object sender, EventArgs e)
        {
            if (news.news_id > 0)
            {
                contents.english_content = elm1.Value;
                contents.bangla_content = elm2.Value;

                contents.news_id = news;
                if (contents.content_id > 0)
                {
                    cdao.update(contents);
                }
                else
                    contents = cdao.Add_ret(contents);
                if (contents.bangla_content != string.Empty)
                    news.content_bn = get_less(contents.bangla_content);
                if (contents.english_content != string.Empty)
                    news.content_en = get_less(contents.english_content);
                news.contents = contents;
                ndao.update(news);
                Response.Redirect("~/User/popups/add_news3.aspx?id=" + news.news_id);

            }

        }
        public string removehtml(string cont1)
        {
            // System.Text.RegularExpressions.Regex.Replace(cont1, "<.*?>", string.Empty);
            return System.Text.RegularExpressions.Regex.Replace(cont1, "<.*?>", string.Empty); ;
        }
        public string get_less(string cont)
        {

            int count = 0;
            cont = removehtml(cont);
            char[] arr = cont.ToCharArray();
            string cen = string.Empty;
            foreach (char VARIABLE in arr)
            {
                cen += VARIABLE;
                if (count > 320) break;
                count++;
            }
            return cen;
        }
        protected void LoadControlState(int newsid)
        {
            
            if (newsid > 0)
            {
                news = ndao.pickbyID(newsid);
               // if(news.contents!=null)
               // {
                    contents=cdao.GetContentbyNewsID(news);
                    elm1.Value = contents.english_content;
                    elm2.Value = contents.bangla_content;
              //  }
                
            }
        }
    }
}