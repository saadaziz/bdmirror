﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add_news3.aspx.cs" MasterPageFile="~/User/Admin_master.Master"
 Inherits="bdmirror24.User.popups.add_news3" %>

<asp:Content runat="server" ContentPlaceHolderID="head" ID="idhead">
</asp:Content>
<asp:Content ID="idcnt" ContentPlaceHolderID="cphBody" runat="server">
<asp:MultiView ID="mulNews" runat="server" ActiveViewIndex="0">
<asp:View ID="vImage" runat="server">
            <div class="row-fluid">
                <div class="span2">
                    Upload:</div>
                <div class="span5">
                    <asp:FileUpload ID="fileUpload" runat="server" /></div>
                <div class="span3">
                    Image:
                    <asp:CheckBox ID="chkimage" runat="server" Checked="true" />
                    &nbsp; &nbsp;<asp:Button ID="Button2" runat="server" OnClick="UploadButton_Click"
                        Text="Upload" CssClass="btn-primary" Height="30px" />
                </div>
            </div>
            <div class="row-fluid">
                <div class="span2">
                    Caption Bangla</div>
                <div class="span3">
                    <asp:TextBox ID="txtcapbn" runat="server" Width="450px"></asp:TextBox>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span2">
                    Caption English</div>
                <div class="span3">
                    <asp:TextBox ID="txtcapen" runat="server" Width="450px"></asp:TextBox>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span2">
                    Image Name</div>
                <div class="span3">
                    <asp:Label ID="lblimagename" runat="server" Width="450px" placeholder="Must insert the name here"></asp:Label>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span2">
                    ImagesID</div>
                <div class="span3">
                    <asp:TextBox ID="txtimages" runat="server" ToolTip=", seperated"></asp:TextBox>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    <asp:Label ID="StatusLabel" runat="server" Text=""></asp:Label></div>
                <div class="span4">
                    <asp:HyperLink ID="hpllink" Target="_blank" runat="server" Text=""></asp:HyperLink></div>
            </div>
            <div class="row-fluid pull-right">
                <asp:Button ID="Button3" runat="server" Text="Proceed" OnClick="proceed_next" />
            </div>
        </asp:View>
        
        <asp:View ID="vSuccess" runat="server">
            <h2>
                Successfully inserted.</h2>
            <h4>
                <asp:HyperLink CssClass="blink" Target="_blank" ID="hplNewsLink" runat="server"></asp:HyperLink></h4>
                <br />
                <asp:HyperLink CssClass="blink" Target="_blank" ID="hplmore" runat="server" Text="Add More"></asp:HyperLink>
        </asp:View>
        <asp:View ID="vFailure" runat="server">
            <h2>
                Error occured.</h2>
            <asp:Label ID="lblerror_msg" runat="server" Text="Label"></asp:Label>
        </asp:View>
    </asp:MultiView>
</asp:Content>
