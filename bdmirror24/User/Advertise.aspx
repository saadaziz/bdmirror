﻿<%@ Page Language="C#" MasterPageFile="Admin_master.Master" AutoEventWireup="true"
    CodeBehind="Advertise.aspx.cs" Inherits="bdmirror24.User.Advertise" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content runat="server" ContentPlaceHolderID="head" ID="idhead">
</asp:Content>
<asp:Content ID="idcnt" ContentPlaceHolderID="cphBody" runat="server">
<asp:MultiView id="mulAd" runat="server">
<asp:View ID="vInput" runat="server">
    <div class="row-fluid">
        <div class="span2">
            <asp:Label ID="lblAddid" runat="server" Visible="true" Text="Add. ID:"></asp:Label>
             </div>
        <div class="span3">
            <asp:TextBox ID="txtAddid" runat="server" Visible="true" Width="450px"></asp:TextBox>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span2">
            Image Link:</div>
        <div class="span3">
            <asp:TextBox ID="txtimglink" runat="server" Width="450px"></asp:TextBox>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span2">
            Title</div>
        <div class="span3">
            <asp:DropDownList ID="ddltitle" runat="server" Width="180px">
                <asp:ListItem>Add. in Top</asp:ListItem>
                <asp:ListItem>Add. in Right Column(Home)</asp:ListItem>
                <asp:ListItem>Add. in Middle(Home) </asp:ListItem>
                <asp:ListItem>Add. in Footer </asp:ListItem>
                <asp:ListItem>Add. in tv</asp:ListItem>
                
            </asp:DropDownList>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span2">
            Image URL</div>
        <div class="span3">
            <asp:TextBox ID="txtimgurl" runat="server" Width="450px"></asp:TextBox>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span2">
            Order</div>
        <div class="span3">
            <asp:TextBox ID="txtimgorder" runat="server" Width="100px"></asp:TextBox>
        </div>
        <div class="span3 offset2">
            <asp:Button ID="btnaddup" runat="server" Text="Submit" CssClass="btn-primary" Height="30px" OnClick="upload_add" />
        </div>
    </div>
    </asp:View>
<asp:View ID="vSuccess" runat="server">
<h2>Successfully inserted</h2>
</asp:View>
<asp:View ID="vFailure" runat="server">
<h2>Successfully failed</h2>
    <asp:Label ID="lblmsg" runat="server" Text="Label"></asp:Label>
</asp:View>
<asp:View ID="vUpdate" runat="server">
<h2>Successfully Updated</h2>
</asp:View>
    </asp:MultiView>
    <hr />
    <asp:GridView ID="grvAdd" DataKeyNames="add_id" runat="server" Width="100%" AutoGenerateColumns="False"
                    CellPadding="4" HorizontalAlign="Left" 
                    ShowHeaderWhenEmpty="true" OnRowDataBound="grvAdd_RowDataBound" OnRowCommand="grvAdd_RowCommand"
                    EmptyDataText="No Records Found" CssClass="tablesorterBlue" AllowPaging="true"
                    PageSize="15"
                     DataSourceID="dssAdd">
                    <Columns>
                        <asp:TemplateField HeaderText="#" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="2%">
                            <ItemTemplate>
                                <asp:Label ID="lbladdid" Text='<%# Eval("add_id") %>' runat="server" HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Image Title" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblimgtitle" Text='<%# (Eval("title")) %>' runat="server" HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Image" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" 
                            ItemStyle-Width="15%">
                            <ItemTemplate >
                                <%--<asp:Label ID="lblimglink" Text='<%# Eval("image_link") %>'  runat="server" HorizontalAlign="Left" Font-Names="Shonar" />--%>
                                <asp:Image ID="imgAdd" Height="60" Width="200" ImageUrl='<%# Eval("image_link") %>'  runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Image Url" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" 
                            ItemStyle-Width="15%">
                            <ItemTemplate >
                                <asp:Label ID="lblimgurl" Text='<%# Eval("image_url") %>'  runat="server" HorizontalAlign="Left" Font-Names="Shonar" />
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Add. Order" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" 
                            ItemStyle-Width="15%">
                            <ItemTemplate >
                                <asp:Label ID="lblimglink" Text='<%# Eval("addorder") %>'  runat="server" HorizontalAlign="Left" Font-Names="Shonar" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgEdit" runat="server" CommandName="Select" ImageUrl="http://www.bdmirror24.com/img/Edit.png"
                                    ToolTip="Edit" />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <%--<asp:TemplateField ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgEdit" runat="server" CommandName="Select" ImageUrl="http://www.bdmirror24.com/img/Edit.png"
                                    ToolTip="Edit" />
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Font-Bold="true" Font-Underline="false" BackColor="#e6EEEE" />
                    <AlternatingRowStyle BackColor="#E5EAE8" />
                    <EditRowStyle BackColor="#999999" />
                    <EmptyDataRowStyle ForeColor="#CC0000" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                </asp:GridView>
                   <asp:SqlDataSource ID="dssAdd" runat="server" ProviderName="System.Data.Odbc"
                    ConnectionString="<%$ ConnectionStrings:bdMirrorDB %>" SelectCommand="Select add_id,title,image_link,image_url,addorder FROM all_add order by add_id desc"> 

                </asp:SqlDataSource>
</asp:Content>
