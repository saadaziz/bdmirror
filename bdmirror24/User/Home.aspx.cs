﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.DAL_Functions.Implementations;
using bdmirror24.DAL.mappings;
using System.Data;

namespace bdmirror24.User
{
    public partial class Home : System.Web.UI.Page
    {
        NewsDAO ndao = new NewsDAO();
        static DataTable dtList = new DataTable("dssnews");
        protected void init()
        {
            if (dtList.Rows.Count == 0)
            {
                dtList.Columns.Add("id", typeof(string));
                dtList.Columns.Add("title_en", typeof(string));
                dtList.Columns.Add("title_bn", typeof(string));
                dtList.Columns.Add("published", typeof(string));
                dtList.Columns.Add("top_news", typeof(string));
                dtList.Columns.Add("latest", typeof(string));
                dtList.Columns.Add("date", typeof(string));
                dtList.Columns.Add("content_en", typeof(string));
                dtList.Columns.Add("content_bn", typeof(string));
                dtList.Columns.Add("name_en", typeof(string));
                dtList.Columns.Add("name_bn", typeof(string));
            }
            else
            {
                dtList.Rows.Clear();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                init();
                if (Page.RouteData.Values["catid"] != null)
                {
                    bdmirrorDB.categories cat = new CategoryDAO().getcatbyName(Page.RouteData.Values["catid"].ToString());
                    IList<bdmirrorDB.news> allNews = ndao.getAllNewsbyCatName(cat); 
                    foreach (bdmirrorDB.news nz in allNews)
                    {
                        dtList.Rows.Add(new string[] { nz.news_id.ToString(), nz.title_en, nz.title_bn, nz.published,  nz.top_news, nz.latest, nz.date.ToShortDateString(), nz.content_en, nz.content_bn, nz.category_id.name_en, nz.category_id.name_bn });
                    }
                   
                    imgNew.CommandArgument = (Page.RouteData.Values["catid"]).ToString();
                }
                else
                {
                    
                    IList<bdmirrorDB.news> allNews = ndao.All();
                    foreach (bdmirrorDB.news nz in allNews)
                    {
                        dtList.Rows.Add(new string[] { nz.news_id.ToString(), nz.title_en, nz.title_bn, nz.published,  nz.top_news, nz.latest, nz.date.ToShortDateString(), nz.content_en, nz.content_bn, nz.category_id.name_en, nz.category_id.name_bn });
                    }
                    

                }
                grvNews.DataSource = dtList;
                grvNews.DataBind();
            }

        }
        protected void chkchanged(object sender, EventArgs e)
        {
            CheckBox chkbx = (CheckBox)sender;
            GridViewRow row = (GridViewRow)chkbx.NamingContainer;
            CheckBox chkchanged =(CheckBox) row.FindControl("chkchanged");
            chkchanged.Checked = true;
        }
        protected void update_latest(object sender, EventArgs e)
        {
            string[] ids=txtlatest.Text.Split(',');
            foreach(string id in ids)
            {
                string cmd = "update news set latest='1' where id="+id+";";
                new NewsDAO().updatelatest(cmd);
            }
        }
        string strSearch = string.Empty;
        #region Methods For Button
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text;
            init();
            IList<bdmirrorDB.news> allNews =new Utility().getIlistNews( ndao.GetCategoryNews("select id from news where title_en like '%"+strSearch+"%' or title_bn like '%"+strSearch+"%';"));
            foreach (bdmirrorDB.news nz in allNews)
            {
                dtList.Rows.Add(new string[] { nz.news_id.ToString(), nz.title_en, nz.title_bn, nz.published, nz.top_news, nz.latest, nz.date.ToShortDateString(), nz.content_en, nz.content_bn, nz.category_id.name_en, nz.category_id.name_bn });
            }
            grvNews.DataSource = dtList;
            grvNews.DataBind();
        }

        protected void imgRefresh_Click(object sender, ImageClickEventArgs e)
        {
            txtSearch.Text = string.Empty;
            grvNews.DataBind();
        }
        protected void btn_new(object sender, ImageClickEventArgs e)
        {
            //Guid guid = new Guid(((ImageButton)sender).CommandArgument);
            if(((ImageButton)sender).CommandArgument.ToString()!="")
                Response.Redirect("~/User/popups/add_news1.aspx?cat=" + ((ImageButton)sender).CommandArgument.ToString());
            else
                Response.Redirect("~/User/home/" );
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            grvNews.DataBind();
        }
        protected void update_grid(object sender, EventArgs e)
        {
            NewsDAO ndao= new NewsDAO();
            foreach (GridViewRow row in grvNews.Rows)
            {
                CheckBox chkchanged = (CheckBox)row.FindControl("chkchanged");
                if (chkchanged.Checked == true)
                {
                    Label newsid = (Label)row.FindControl("lblid");
                    CheckBox chklatest = (CheckBox)row.FindControl("chklatest");
                    CheckBox chktopnews = (CheckBox)row.FindControl("chktopnews");
                    CheckBox chkPublished = (CheckBox)row.FindControl("chkPublished");
                    bdmirrorDB.news nz = ndao.pickbyID(int.Parse(newsid.Text));
                    nz.published = (chkPublished.Checked == true) ? "1" : "0";
                    nz.top_news = (chktopnews.Checked == true) ? "1" : "0";
                    nz.latest = (chklatest.Checked == true) ? "1" : "0";
                    ndao.update(nz);
                }
            }
        }
        #endregion

        #region Methods For Grid
        protected void grvNews_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            //if (e.CommandName.Equals("Delete"))
            //{
            //    int intPartyID = Convert.ToInt32(e.CommandArgument.ToString());

            //    if (intPartyID > 0)
            //    {
            //        DeleteParty(intPartyID);
            //    }
            //}
        }

        protected void grvNews_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvNews.PageIndex = e.NewPageIndex;
            grvNews.DataSource = dtList;
            grvNews.DataBind();
        }
        Utility utiob= new Utility();
        protected void grvNews_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                HyperLink btnEdit = (HyperLink)e.Row.FindControl("hpledit");
                btnEdit.NavigateUrl = "~/User/popups/add_news1.aspx?id=" + DataBinder.Eval(e.Row.DataItem, "id").ToString();

                HyperLink lblentitle = (HyperLink)e.Row.FindControl("lblentitle");
                lblentitle.NavigateUrl = "~/news/en/" + DataBinder.Eval(e.Row.DataItem, "name_en").ToString()+"/"+
                    DataBinder.Eval(e.Row.DataItem, "id").ToString() + "/" + utiob.replaceSpace( DataBinder.Eval(e.Row.DataItem, "title_en").ToString());

                HyperLink lblbmtitle = (HyperLink)e.Row.FindControl("lblbmtitle");
                lblbmtitle.NavigateUrl = "~/news/bn/" + DataBinder.Eval(e.Row.DataItem, "name_bn").ToString() + "/" +
    DataBinder.Eval(e.Row.DataItem, "id").ToString() + "/" + utiob.replaceSpace(DataBinder.Eval(e.Row.DataItem, "title_bn").ToString());

                CheckBox chklatest = (CheckBox)e.Row.FindControl("chklatest");
                CheckBox chktopnews = (CheckBox)e.Row.FindControl("chktopnews");
                CheckBox chkPublished = (CheckBox)e.Row.FindControl("chkPublished");
                if (DataBinder.Eval(e.Row.DataItem, "published").ToString() == "1")
                    chkPublished.Checked = true;
                if (DataBinder.Eval(e.Row.DataItem, "latest").ToString() == "1")
                    chklatest.Checked = true;
                if (DataBinder.Eval(e.Row.DataItem, "top_news").ToString() == "1")
                    chktopnews.Checked = true;
            }
        }
        #endregion
    }
}