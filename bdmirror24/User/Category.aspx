﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeBehind="Category.aspx.cs"  MasterPageFile="Admin_master.Master" Inherits="bdmirror24.User.Category" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content runat="server" ContentPlaceHolderID="head" ID="idhead">
</asp:Content>
<asp:Content ID="idcnt" ContentPlaceHolderID="cphBody" runat="server">
    <div class="row-fluid">
        <div class="span1">
            Category(<asp:Label ID="lblcatid" runat="server" Text=""></asp:Label>):
        </div>
        <div class="span3">English:
            <asp:TextBox ID="txtcaten" runat="server"></asp:TextBox>
        </div>
        <div class="span3">Bangla:
            <asp:TextBox ID="txtcatbn" runat="server"></asp:TextBox>
        </div>
        <div class="row-fluid span6 pull-right">
            <asp:Button ID="butupdate" runat="server" CssClass="btn-primary" Text="Save" OnClick="btnupdate_click"></asp:Button>
        </div>
        <div class="pull-right">
                Search:  <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                   

                    <asp:Button ID="btnRefresh" runat="server" Text="Refresh" Style="display: none" OnClick="btnRefresh_Click" />
                    <asp:ImageButton ID="imgSearch" runat="server" CommandName="Search" ImageUrl="~/img/Search.png"
                        ToolTip="Search" Width="16px" Height="16px" OnClick="imgSearch_Click" />
                    <asp:ImageButton ID="imgRefresh" runat="server" CommandName="Refresh" ImageUrl="~/img/Refresh.png"
                        ToolTip="Refresh" Width="16px" Height="16px" OnClick="imgRefresh_Click" />
                </div>
                <asp:GridView ID="grvCat" DataKeyNames="id" runat="server" Width="100%" AutoGenerateColumns="False"
                    CellPadding="4" HorizontalAlign="Left" 
                    ShowHeaderWhenEmpty="true" OnRowDataBound="grvCat_RowDataBound" OnRowCommand="grvCat_RowCommand" 
                    EmptyDataText="No Records Found" CssClass="tablesorterBlue" 
                     DataSourceID="dssNews">
                    <Columns>
                        <asp:TemplateField HeaderText="#" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="2%">
                            <ItemTemplate>
                                <asp:Label ID="lblid" Text='<%# Eval("id") %>' runat="server" HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="English Title" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblentitle" Text='<%# (Eval("name_en")) %>' runat="server" HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Bangla Title" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" 
                            ItemStyle-Width="15%">
                            <ItemTemplate >
                                <asp:Label ID="lblbmtitle" Text='<%# Eval("name_bn") %>'  runat="server" HorizontalAlign="Left" Font-Names="Shonar Bangla" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgEdit" runat="server" CommandName="Select" ImageUrl="http://www.bdmirror24.com/img/Edit.png"
                                    ToolTip="Edit" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Font-Bold="true" Font-Underline="false" BackColor="#e6EEEE" />
                    <AlternatingRowStyle BackColor="#E5EAE8" />
                    <EditRowStyle BackColor="#999999" />
                    <EmptyDataRowStyle ForeColor="#CC0000" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                </asp:GridView>
                <asp:SqlDataSource ID="dssNews" runat="server" ProviderName="System.Data.Odbc"
                    ConnectionString="<%$ ConnectionStrings:bdMirrorDB %>"
                     SelectCommand="SELECT name_bn,name_en,id
                        from categories" 
                    FilterExpression="name_bn LIKE '%{0}%' OR name_en LIKE '%{1}%'">
                    <FilterParameters>
                        <asp:ControlParameter Name="name_bn" ControlID="txtSearch" PropertyName="Text" />
                        <asp:ControlParameter Name="name_en" ControlID="txtSearch" PropertyName="Text" />
                    </FilterParameters>
                </asp:SqlDataSource>
    </div>
</asp:Content>