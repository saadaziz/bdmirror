﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;

namespace bdmirror24.User
{
    public partial class Advertise : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                mulAd.ActiveViewIndex = 0;
            
        }
        protected void upload_add(object sender, EventArgs e)
        {
            try
            {
                int flag = 0;
                bdmirrorDB.all_add add1 = null;
                if (txtAddid.Text == "")
                {
                    add1 = new bdmirrorDB.all_add();
                    add1.image_link = txtimglink.Text;
                    add1.image_url = txtimgurl.Text;
                    add1.addorder = long.Parse(txtimgorder.Text);
                    add1.title = ddltitle.SelectedItem.Text;
                }
                else
                {
                    add1 = new All_addDAO().pickbyID(int.Parse(txtAddid.Text));
                    add1.add_id = int.Parse(txtAddid.Text);
                    add1.image_link = txtimglink.Text;
                    add1.image_url = txtimgurl.Text;
                    add1.addorder = long.Parse(txtimgorder.Text);
                    add1.title = ddltitle.SelectedItem.Text;
                    flag = 1;
                }


                if (new All_addDAO().Add(add1) && flag == 0)
                    mulAd.ActiveViewIndex = 1;
                else if (new All_addDAO().update(add1) && flag == 1)
                    mulAd.ActiveViewIndex = 3;
                else
                    mulAd.ActiveViewIndex = 2;
                flag = 0;
                grvAdd.DataBind();
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
                mulAd.ActiveViewIndex = 2;
            }
        }
        #region Methods For Grid
        protected void grvAdd_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            //if (e.CommandName.Equals("Delete"))
            //{
            //    int intPartyID = Convert.ToInt32(e.CommandArgument.ToString());

            //    if (intPartyID > 0)
            //    {
            //        DeleteParty(intPartyID);
            //    }
            //}
            if (e.CommandName.Equals("Select"))
            {
                string text = e.CommandArgument.ToString();
                bdmirrorDB.all_add add = new All_addDAO().pickbyID(int.Parse(text));
                txtAddid.Text = add.add_id.ToString();
                txtimglink.Text = add.image_link.ToString();
                txtimgurl.Text = add.image_url.ToString();
                txtimgorder.Text = add.addorder.ToString();
                ddltitle.SelectedItem.Text = add.title.ToString();
            }
        }

        protected void grvAdd_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvAdd.PageIndex = e.NewPageIndex;
            grvAdd.DataBind();
        }

        protected void grvAdd_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            //{
            //    HyperLink btnEdit = (HyperLink)e.Row.FindControl("hpledit");
            //    btnEdit.NavigateUrl = "~/User/edit_news/" + DataBinder.Eval(e.Row.DataItem, "id").ToString() + "/";
            //}
            //if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            //{
            //    HyperLink btncat = (HyperLink)e.Row.FindControl("lblcat");
            //    btncat.NavigateUrl = "~/news/" + language + "/" + Server.UrlEncode(DataBinder.Eval(e.Row.DataItem, "name").ToString()) + "/";
            //    HyperLink btncat1 = (HyperLink)e.Row.FindControl("lblentitle");
            //    btncat1.NavigateUrl = "~/news/" + language + "/" + Server.UrlEncode(DataBinder.Eval(e.Row.DataItem, "name").ToString()) + "/" +
            //                            DataBinder.Eval(e.Row.DataItem, "id").ToString() + "/"; ;
            //}
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                ImageButton btnEdit = (ImageButton)e.Row.FindControl("imgEdit");
                btnEdit.CommandArgument = ((Label)e.Row.FindControl("lbladdid")).Text;
            }
        }
        #endregion
    }
}