﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.DAL_Functions.Implementations;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions;

namespace bdmirror24.User
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HttpCookie cookie = Request.Cookies["UserInfo_bdmirror"];
                if (cookie != null)
                    /*if cookie exists, user is logged in, wants to logout, so kill cookie */
                {
                    //objckusername.Expires = DateTime.Now.AddDays(-1);
                    //Response.Cookies.Add(objckusername);

                    cookie.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(cookie);

                    Session.RemoveAll();
                    Session.Abandon();
                }
            }

        }
        protected void login_check(object sender, EventArgs e)
        {
            bdmirrorDB.user user = new userDAO().getuserbyname(txt_username.Text);
            if (user.password.Equals(txt_password.Text))
            {
                  //Response.Redirect("~/User/home/");
                Session.Add("userbdmirror", (bdmirrorDB.user)user);
               
                //HttpCookie cookie = new HttpCookie("UserInfo_freelancer");
                HttpCookie cookie = new HttpCookie("UserInfo_bdmirror");
                cookie["UserName_bdmirror"] = txt_username.Text;

                cookie.Expires = (chk_remember.Checked) ? DateTime.Now.AddDays(10) : DateTime.Now.AddHours(1);

                // query check1 = new query();
                // cookie["UserId"] = check1.GetUserID(uname, pass);
                //cookie.Expires = DateTime.Now.AddDays(1);
                Response.Cookies.Add(cookie);
                string useremail = user.email.ToString();
                Session.Add("UserName_bdmirror",(string)useremail); 
                Response.Redirect("~/User/home/");
            }
            else
            {
                lbl_msg.Text = "username or password mismatch";
                Response.Redirect("~/User/login.aspx");
            }
        }
    }
}