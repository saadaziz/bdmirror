﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
using bdmirror24.DAL.Manager;

namespace bdmirror24.User
{
    public partial class CurrentAffairs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var link = new HtmlGenericControl("script");
            link.Attributes.Add("src", ResolveUrl("~/tinymce/jscripts/tiny_mce/tiny_mce.js"));
            link.Attributes.Add("type", "text/javascript");

            Header.Controls.Add(link);
            if (!IsPostBack)
            {
                mulNews.ActiveViewIndex = 0;
            }
        }
        protected void btnUpdate_click(object sender, EventArgs e)
        {
            try {
                bdmirrorDB.current_affairs affair = new bdmirrorDB.current_affairs();
                affair.content_en = elm1.Value;
                affair.content_bn = elm2.Value;
                affair.insert_date = DateTime.Now;
                new CADAO().Add(affair);
                mulNews.ActiveViewIndex = 1;
            }
            catch(Exception ex)
            {
                mulNews.ActiveViewIndex = 2;
                lblerror_msg.Text = ex.Message;
            }
        }
        protected void grdpgechange(Object sender,GridViewPageEventArgs args)
        {
            gridca.PageIndex = args.NewPageIndex;
            gridca.DataBind();
        }
    }
}