﻿<%@ Page Language="C#" MasterPageFile="Admin_master.Master" AutoEventWireup="true"
    CodeBehind="Home.aspx.cs" Inherits="bdmirror24.User.Home" %>
    <%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>--%>
<asp:Content runat="server" ContentPlaceHolderID="head" ID="idhead">
 
</asp:Content>
<asp:Content ID="idcnt" ContentPlaceHolderID="cphBody" runat="server">
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>--%>
    <div>
               <div class="row-fluid">
               <div class="span2">Latest News ID</div>
               <div class="span4">
                    <asp:TextBox ID="txtlatest" runat="server"></asp:TextBox>
                    </div>
    <div class"span2>
        <asp:Button ID="Button1" runat="server" Text="Update" OnClick="update_latest" /></div>
        </div>
                <div class="pull-right">
                    <asp:Button ID="Button2" CssClass="btn-block" runat="server" Text="Update" OnClick="update_grid" />
                 <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                    <asp:ImageButton ID="imgNew" runat="server" OnClick="btn_new" ImageUrl="~/img/New.png"
                        ToolTip="New" Width="16px" Height="16px" />
                   

                    <asp:Button ID="btnRefresh" runat="server" Text="Refresh" Style="display: none" OnClick="btnRefresh_Click" />
                    <asp:ImageButton ID="imgSearch" runat="server" CommandName="Search" ImageUrl="~/img/Search.png"
                        ToolTip="Search" Width="16px" Height="16px" OnClick="imgSearch_Click" />
                    <asp:ImageButton ID="imgRefresh" runat="server" CommandName="Refresh" ImageUrl="~/img/Refresh.png"
                        ToolTip="Refresh" Width="16px" Height="16px" OnClick="imgRefresh_Click" />
                </div>
       <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>--%>
                <asp:GridView ID="grvNews" DataKeyNames="id" runat="server" Width="100%" AutoGenerateColumns="False"
                    CellPadding="4" HorizontalAlign="Left" OnPageIndexChanging="grvNews_PageIndexChanging"
                    ShowHeaderWhenEmpty="true" OnRowDataBound="grvNews_RowDataBound" OnRowCommand="grvNews_RowCommand"
                    EmptyDataText="No Records Found" CssClass="tablesorterBlue" AllowPaging="true" PageSize="20" >
                    <Columns>
                    <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="2%">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkchanged" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="#" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="2%">
                            <ItemTemplate>
                                <asp:Label ID="lblid" Text='<%# Eval("id") %>' runat="server" HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="English Title" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:HyperLink ID="lblentitle" Text='<%# (Eval("title_en")) %>' runat="server" HorizontalAlign="Left" ToolTip='<%# (Eval("content_en")) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Bangla Title" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:HyperLink ID="lblbmtitle" Text='<%# Eval("title_bn") %>' runat="server" HorizontalAlign="Left" ToolTip='<%# (Eval("content_bn")) %>'/>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Published" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="2%">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkPublished" runat="server" AutoPostBack="true" OnCheckedChanged="chkchanged" />

                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Category" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="2%">
                            <ItemTemplate>
                                <asp:Label ID="lblcat" Text='<%# Eval("name_en") %>' runat="server" HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Top news" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="4%">
                            <ItemTemplate>
                                <asp:CheckBox ID="chktopnews" runat="server" AutoPostBack="true" OnCheckedChanged="chkchanged"/>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Latest" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="4%">
                            <ItemTemplate>
                                <%--<asp:Label ID="lbllatest" Text='<%# Eval("latest") %>' runat="server" HorizontalAlign="Left" />--%>
                                <asp:CheckBox ID="chklatest" runat="server" AutoPostBack="true" OnCheckedChanged="chkchanged" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="6%">
                            <ItemTemplate>
                                <asp:Label ID="lbldate" Text='<%# Eval("date") %>' runat="server" HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink ID="hpledit" runat="server">Edit</asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Font-Bold="true" Font-Underline="false" BackColor="#e6EEEE" />
                    <AlternatingRowStyle BackColor="#E5EAE8" />
                    <EditRowStyle BackColor="#999999" />
                    <EmptyDataRowStyle ForeColor="#CC0000" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                </asp:GridView>
               
 <%--     </ContentTemplate>
        </asp:UpdatePanel>--%>
    </div>
</asp:Content>
