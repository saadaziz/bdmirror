﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
using bdmirror24.DAL.Manager;

namespace bdmirror24.User
{
    public partial class UploadFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void UploadButton_Click(object sender, EventArgs e)
        {
            if (fileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(fileUpload.FileName);
                    string path = Server.MapPath("../../Uploads") + "\\" + filename;
                    fileUpload.SaveAs(path);

                    if (chkimage.Checked == true)
                    {
                        bdmirrorDB.images images = new bdmirrorDB.images();
                        images.caption_bn = txtcapbn.Text;
                        images.caption_en = txtcapen.Text;
                        images.image_name = txtimagename.Text;
                        images.link = "http://www.bdmirror24.com/" + "Uploads/" + filename;
                        System.Drawing.Image objImage = System.Drawing.Image.FromFile(path);
                        images.width = objImage.Width;
                        images.height = objImage.Height;
                        images.time = DateTime.Now;

                        new ImageDAO().Add(images);

                    }
                    hpllink.Text =  filename;
                    hpllink.NavigateUrl = "http://www.bdmirror24.com/" + "Uploads/" + filename;
                    
                    StatusLabel.Text = "Upload status: File uploaded!";

                }
                catch (Exception ex)
                {
                    StatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
                }
            }
        }
        #region Methods For Button
        string strSearch = string.Empty;
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text;
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            grvNews.DataBind();
        }
        protected void imgRefresh_Click(object sender, ImageClickEventArgs e)
        {
            txtSearch.Text = string.Empty;
            grvNews.DataBind();
        }
        #endregion
        #region Methods For Grid
        protected void grvNews_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            //if (e.CommandName.Equals("Delete"))
            //{
            //    int intPartyID = Convert.ToInt32(e.CommandArgument.ToString());

            //    if (intPartyID > 0)
            //    {
            //        DeleteParty(intPartyID);
            //    }
            //}
        }

        protected void grvNews_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvNews.PageIndex = e.NewPageIndex;
            grvNews.DataBind();
        }

        protected void grvNews_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                //HyperLink btnEdit = (HyperLink)e.Row.FindControl("hpledit");
                //btnEdit.NavigateUrl = "~/User/edit_news/" + DataBinder.Eval(e.Row.DataItem, "id").ToString()+"/";
            }
        }
        #endregion
    }
}