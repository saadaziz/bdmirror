﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="Admin_master.Master"
CodeBehind="video.aspx.cs" Inherits="bdmirror24.User.video" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content runat="server" ContentPlaceHolderID="head" ID="idhead">
</asp:Content>
<asp:Content ID="idcnt" ContentPlaceHolderID="cphBody" runat="server">
<div class="span6">
    <table>
        <tr>
        <td>Caption English</td>
        <td>
    <asp:TextBox ID="txtcapen" runat="server"></asp:TextBox></td></tr><tr>
        <td>Caption bangla</td>
        <td>
    <asp:TextBox ID="txtbng" runat="server"></asp:TextBox></td></tr><tr>
        <td>Link</td>
        <td>
    <asp:TextBox ID="txtlink" runat="server"></asp:TextBox></td></tr>
    <tr>
    <td colspan="2" align="right">
        <asp:Button ID="Button1" runat="server" Text="Insert" OnClick="btn_insert" /></td></tr>
    </table>
</div>
<div class="pull-right">
                 <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                    
                    <%--<cc1:ModalPopupExtender ID="ModalPopupExtender1" BackgroundCssClass="ModalPopupBG"
                        runat="server" CancelControlID="ButtonNewCancel" OkControlID="ButtonNewDone"
                        TargetControlID="imgNew" PopupControlID="DivNewWindow" OnOkScript="NewOkayScript();">
                    </cc1:ModalPopupExtender>
                    <div class="popup_Buttons" style="display: none">
                        <input id="ButtonNewDone" value="Done" type="button" />
                        <input id="ButtonNewCancel" value="Cancel" type="button" />
                    </div>
                    <div id="DivNewWindow" style="display: none;" class="popupNews">
                        <iframe id="IframeNew" frameborder="0" width="870px" height="404px" src="~/User/popups/edit_news.aspx"
                            class="frameborder" scrolling="no"></iframe>
                    </div>--%>

                    <asp:Button ID="btnRefresh" runat="server" Text="Refresh" Style="display: none" OnClick="btnRefresh_Click" />
                    <asp:ImageButton ID="imgSearch" runat="server" CommandName="Search" ImageUrl="~/img/Search.png"
                        ToolTip="Search" Width="16px" Height="16px" OnClick="imgSearch_Click" />
                    <asp:ImageButton ID="imgRefresh" runat="server" CommandName="Refresh" ImageUrl="~/img/Refresh.png"
                        ToolTip="Refresh" Width="16px" Height="16px" OnClick="imgRefresh_Click" />
                </div>

 <asp:GridView ID="grvNews" DataKeyNames="video_id" runat="server" Width="100%" AutoGenerateColumns="False"
                    CellPadding="4" HorizontalAlign="Left" OnPageIndexChanging="grvNews_PageIndexChanging"
                    ShowHeaderWhenEmpty="true" OnRowDataBound="grvNews_RowDataBound" OnRowCommand="grvNews_RowCommand"
                    EmptyDataText="No Records Found" CssClass="tablesorterBlue" AllowPaging="True"
                    PageSize="15" DataSourceID="dssNews">
                    <Columns>
                        <asp:TemplateField HeaderText="#" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="2%">
                            <ItemTemplate>
                                <asp:Label ID="lblid" Text='<%# Eval("video_id") %>' runat="server" HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="English Title" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblentitle" Text='<%# (Eval("caption_en")) %>' runat="server" HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Bangla Title" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblbmtitle" Text='<%# Eval("caption_bn") %>' runat="server" HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Link" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:Label ID="lblpub" Text='<%# Eval("link") %>' runat="server" HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Time" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="2%">
                            <ItemTemplate>
                                <asp:Label ID="lblcat" Text='<%# Eval("time") %>' runat="server" HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                        
                     <%--   <asp:TemplateField ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink ID="hpledit" runat="server">Edit</asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Font-Bold="true" Font-Underline="false" BackColor="#e6EEEE" />
                    <AlternatingRowStyle BackColor="#E5EAE8" />
                    <EditRowStyle BackColor="#999999" />
                    <EmptyDataRowStyle ForeColor="#CC0000" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                </asp:GridView>
                <asp:SqlDataSource ID="dssNews" runat="server" ProviderName="System.Data.Odbc"
                    ConnectionString="<%$ ConnectionStrings:bdMirrorDB %>"
                     SelectCommand="select * from videos" 
                    FilterExpression="caption_en LIKE '%{0}%' OR caption_bn LIKE '%{1}%'">
                    <FilterParameters>
                        <asp:ControlParameter Name="title_en" ControlID="txtSearch" PropertyName="Text" />
                        <asp:ControlParameter Name="name_en" ControlID="txtSearch" PropertyName="Text" />
                    </FilterParameters>
                </asp:SqlDataSource>
</asp:Content>