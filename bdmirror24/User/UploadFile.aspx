﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="Admin_master.Master"
    CodeBehind="UploadFile.aspx.cs" Inherits="bdmirror24.User.UploadFile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content runat="server" ContentPlaceHolderID="head" ID="idhead">
</asp:Content>
<asp:Content ID="idcnt" ContentPlaceHolderID="cphBody" runat="server">
    <%--<iframe runat="server" ID="iframe1" src="Uploads/UploadsDefault.aspx"  frameborder="0"  width="100%" height="450px" scrolling="no" ></iframe>--%>
    <div class="row-fluid">
        <div class="span2">
            Upload:</div>
        <div class="span5">
            <asp:FileUpload ID="fileUpload" runat="server" /></div>
        <div class="span3">
            Image:
            <asp:CheckBox ID="chkimage" runat="server" />
            &nbsp; &nbsp;<asp:Button ID="Button1" runat="server" OnClick="UploadButton_Click"
                Text="Upload" CssClass="btn-primary" Height="30px" />
        </div>
    </div>
    <div class="row-fluid">
        <div class="span2">
            Caption Bangla</div>
        <div class="span3">
            <asp:TextBox ID="txtcapbn" runat="server" Width="450px"></asp:TextBox>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span2">
            Caption English</div>
        <div class="span3">
            <asp:TextBox ID="txtcapen" runat="server" Width="450px"></asp:TextBox>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span2">
            Image Name</div>
        <div class="span3">
            <asp:TextBox ID="txtimagename" runat="server" Width="450px"></asp:TextBox>
        </div>
    </div>
     <div class="row-fluid">
    <div class="span4">
        <asp:Label ID="StatusLabel" runat="server" Text=""></asp:Label></div>
    <div class="span4">
        <asp:HyperLink ID="hpllink" Target="_blank" runat="server" Text=""></asp:HyperLink></div>
    </div>
    <div class="row-fluid">
        <div class="span2">
            Search:
        </div>
        <div class="span6">
            <asp:TextBox ID="txtSearch" runat="server" Width="200px"></asp:TextBox>
            &nbsp;
            &nbsp;
             <asp:Button ID="btnRefresh" runat="server" Text="Refresh" Style="display: none" OnClick="btnRefresh_Click" />
                    <asp:ImageButton ID="imgSearch" runat="server" CommandName="Search" ImageUrl="~/img/Search.png"
                        ToolTip="Search" Width="16px" Height="16px" OnClick="imgSearch_Click" />
                    
            &nbsp;
            &nbsp;
            <asp:ImageButton ID="imgRefresh" runat="server" CommandName="Refresh" ImageUrl="~/img/Refresh.png"
                        ToolTip="Refresh" Width="16px" Height="16px" OnClick="imgRefresh_Click" />
        </div>
    </div>
    <asp:GridView ID="grvNews" DataKeyNames="image_id" runat="server" Width="100%" AutoGenerateColumns="False"
        CellPadding="4" HorizontalAlign="Left" OnPageIndexChanging="grvNews_PageIndexChanging"
        ShowHeaderWhenEmpty="true" OnRowDataBound="grvNews_RowDataBound" OnRowCommand="grvNews_RowCommand"
        EmptyDataText="No Records Found" CssClass="tablesorterBlue" AllowPaging="True"
        PageSize="15" DataSourceID="dssNews">
        <Columns>
            <asp:TemplateField HeaderText="#" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                ItemStyle-Width="2%">
                <ItemTemplate>
                    <asp:Label ID="lblid" Text='<%# Eval("image_id") %>' runat="server" HorizontalAlign="Left" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Bangla Caption" HeaderStyle-HorizontalAlign="Left"
                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                <ItemTemplate>
                    <asp:Label ID="lblentitle" Text='<%# (Eval("caption_en")) %>' runat="server" HorizontalAlign="Left" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="English Caption" HeaderStyle-HorizontalAlign="Left"
                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                <ItemTemplate>
                    <asp:Label ID="lblbmtitle" Text='<%# Eval("caption_bn") %>' runat="server" HorizontalAlign="Left" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="File Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                ItemStyle-Width="2%">
                <ItemTemplate>
                    <asp:Label ID="lblpub" Text='<%# Eval("image_name") %>' runat="server" HorizontalAlign="Left" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Link" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                ItemStyle-Width="7%">
                <ItemTemplate>
                    <asp:HyperLink ID="lblcat" Text='<%# Eval("link") %>' Target="_blank" NavigateUrl='<%# Eval("link") %>' runat="server" HorizontalAlign="Left" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Title En" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                ItemStyle-Width="4%">
                <ItemTemplate>
                    <asp:Label ID="lbltop" Text='<%# Eval("title_en") %>' runat="server" HorizontalAlign="Left" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Title BN" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                ItemStyle-Width="4%">
                <ItemTemplate>
                    <asp:Label ID="lbldate" Text='<%# Eval("title_bn") %>' runat="server" HorizontalAlign="Left" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle HorizontalAlign="Right" Font-Bold="true" Font-Underline="false" BackColor="#e6EEEE" />
        <AlternatingRowStyle BackColor="#E5EAE8" />
        <EditRowStyle BackColor="#999999" />
        <EmptyDataRowStyle ForeColor="#CC0000" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    </asp:GridView>
    <asp:SqlDataSource ID="dssNews" runat="server" ProviderName="System.Data.Odbc" ConnectionString="<%$ ConnectionStrings:bdMirrorDB %>"
        SelectCommand="select img.image_id, img.caption_bn,img.caption_en,img.image_name,img.link,nz.title_en,nz.title_bn
        from images as img
        Left JOIN news as nz on img.news_id=nz.id order by img.image_id desc "
        FilterExpression="image_name LIKE '%{0}%' OR caption_en LIKE '%{1}%'">
        <FilterParameters>
            <asp:ControlParameter Name="image_name" ControlID="txtSearch" PropertyName="Text" />
            <asp:ControlParameter Name="caption_en" ControlID="txtSearch" PropertyName="Text" />
        </FilterParameters>
    </asp:SqlDataSource>
</asp:Content>
