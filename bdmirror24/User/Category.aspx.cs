﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
using bdmirror24.DAL.Manager;
namespace bdmirror24.User
{
    public partial class Category : System.Web.UI.Page
    {
        private String strSearch = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
                grvCat.DataBind();
            //grvCat.Font.Name = "Shonar Bangla";
        }
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text;
        }

        protected void imgRefresh_Click(object sender, ImageClickEventArgs e)
        {
            txtSearch.Text = string.Empty;
            grvCat.DataBind();
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            grvCat.DataBind();
        }
        protected void btnupdate_click(object sender, EventArgs e)
        {
            if (lblcatid.Text != null)
            {
                bdmirrorDB.categories cat = new CategoryDAO().pickbyID(int.Parse(lblcatid.Text));
                //byte[] asciiBytes = Encoding.ASCII.GetBytes(txtcatbn.Text);
                
                //string bn = "";
                //foreach (byte asciiByte in asciiBytes)
                //{
                //    bn += "&#" + asciiByte;
                //}

                cat.name_en = txtcaten.Text;
                cat.name_bn = (txtcatbn.Text);
                new CategoryDAO().update(cat);
            }
            else
            {
                bdmirrorDB.categories cat = new bdmirrorDB.categories();
                
                byte[] asciiBytes = Encoding.ASCII.GetBytes(txtcatbn.Text);

                string bn = "";
                foreach (byte asciiByte in asciiBytes)
                {
                    bn += "&#" + asciiByte;
                }
                cat.name_en = txtcaten.Text;
                cat.name_bn = txtcatbn.Text;
                new CategoryDAO().Add(cat);
            }
            grvCat.DataBind();
        }
        public static string DecodeFromUtf8(string utf8String)
        {
            // copy the string as UTF-8 bytes.
            byte[] utf8Bytes = new byte[utf8String.Length];
            for (int i = 0; i < utf8String.Length; ++i)
            {
                //Debug.Assert( 0 <= utf8String[i] && utf8String[i] <= 255, "the char must be in byte's range");
                utf8Bytes[i] = (byte)utf8String[i];
            }

            return Encoding.UTF8.GetString(utf8Bytes, 0, utf8Bytes.Length);
        }
        #region Methods For Grid
        protected void grvCat_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Select"))
            {
                string text = e.CommandArgument.ToString();
                bdmirrorDB.categories cat = new CategoryDAO().pickbyID(int.Parse(text));
                txtcatbn.Text = cat.name_bn;
                txtcaten.Text = cat.name_en;
                lblcatid.Text = cat.cat_id.ToString();
            }
        }



        protected void grvCat_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                ImageButton btnEdit = (ImageButton)e.Row.FindControl("imgEdit");
                btnEdit.CommandArgument = ((Label)e.Row.FindControl("lblid")).Text;
            }
        }
        #endregion
    }
}