﻿<%@ Page Language="C#" MasterPageFile="Admin_master.Master" ValidateRequest="false"
AutoEventWireup="true" CodeBehind="CurrentAffairs.aspx.cs" Inherits="bdmirror24.User.CurrentAffairs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content runat="server" ContentPlaceHolderID="head" ID="idhead">
</asp:Content>

<asp:Content ID="idcnt" ContentPlaceHolderID="cphBody" runat="server">

<script type="text/javascript">
    tinyMCE.init({
        // General options
        mode: "textareas",
        theme: "advanced",
        plugins: "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",

        setup: function (ed) {
            //                ed.onKeyPress.add(
            //                    function(ed, evt) {
            //                        //                        alert(ed.selection.getNode);
            //                        p = ed.selection.getNode();
            //                        alert(p.style.backgroundColor);
            //                        //                        if (p = ed.dom.getParent(ed.selection.getNode(), 'span'))
            //                        //                            v = p.style.backgroundColor;

            //                        //alert(ed.selection.getContent());
            //                        //alert(ed.selection.getStart().childNodes.getContent());
            //                        //var s = $(ed.id + "_preview").getStyle()
            //                        //alert(s);


            //                        //alert(inputColor);
            //                        //                        f = tinyMCEPopup.getWindowArg('func')
            //                        //                        alert(f);
            //                        // alert("Editor-ID: " + ed.id + "\nEvent: " + evt + evt.keyCode.attr('color'));
            //                        // Do some great things here...
            //                    }
            //                );
        },
        // Theme options
        theme_advanced_buttons1: "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4: "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: true,

        // Example content CSS (should be your site CSS)
        content_css: "css/content.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url: "lists/template_list.js",
        external_link_list_url: "lists/link_list.js",
        external_image_list_url: "lists/image_list.js",
        media_external_list_url: "lists/media_list.js",

        // Style formats
        style_formats: [
			{ title: 'Bold text', inline: 'b' },
			{ title: 'Red text', inline: 'span', styles: { color: '#ff0000'} },
			{ title: 'Red header', block: 'h1', styles: { color: '#ff0000'} },
			{ title: 'Example 1', inline: 'span', classes: 'example1' },
			{ title: 'Example 2', inline: 'span', classes: 'example2' },
			{ title: 'Table styles' },
			{ title: 'Table row 1', selector: 'tr', classes: 'tablerow1' }
		],

        // Replace values for the template plugin
        template_replace_values: {
            username: "Some User",
            staffid: "991234"
        }
    });
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <asp:MultiView ID="mulNews" runat="server">
    <asp:View id="vinput" runat="server">
    <div class="row-fluid">
        <div class="span2">
            Current Affair English</div>
        <div class="span8">
            <textarea id="elm1" name="elm1" rows="13" cols="180" style="width: 100%" runat="server"></textarea>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span2">
            Current Affair Bangla</div>
        <div class="span8">
            <textarea id="elm2" name="elm2" rows="13" cols="180" style="width: 100%" runat="server"></textarea>
        </div>
    </div>
    <div class="row-fluid">
        <br />
        <br />
       <asp:Button ID="Button1" CssClass="btn info  pull-right" runat="server" Text="Submit" OnClick="btnUpdate_click" />
    </div>
    <div class="row-fluid">
        <asp:GridView runat="server" 
        AllowPaging="true" ID="gridca" DataSourceID="gridcasource" 
        CssClass="tablesorterBlue" AllowSorting="true" PageSize="10" OnPageIndexChanging="grdpgechange">
           <%-- <Columns>
                <asp:BoundField DataField="current_id" />
                <asp:BoundField DataField="content_bn" />
                <asp:BoundField DataField="content_bn" />
                <asp:BoundField DataField="idate" />
            </Columns>--%>
        </asp:GridView>
        <asp:SqlDataSource ID="gridcasource" runat="server" ProviderName="System.Data.Odbc"
                    ConnectionString="<%$ ConnectionStrings:bdMirrorDB %>"
                    SelectCommand="SELECT current_id,content_bn,content_en,DATE_FORMAT(insert_date,'%m-%d-%Y') as idate FROM current_affairs">
                    </asp:SqlDataSource>
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    </asp:View>
    <asp:View id="vSuccess" runat="server">
    <h2>Successfully inserted.</h2>
    </asp:View>
    <asp:View id="vFailure" runat="server">
    <h2>Error occured.</h2>
        <asp:Label ID="lblerror_msg" runat="server" Text="Label"></asp:Label>
    </asp:View>
    </asp:MultiView>
    <script type="text/javascript">
        $(document).ready(function () {
            $(":p > :span").each(
            function (i) {
                //alert($(this).attr('color'));
            }
            );

            $('#text1').keypress(function (e) {
                alert($('#' + e.target.id).attr("style"));
            });
        });

    </script>
</asp:Content>