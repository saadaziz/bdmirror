﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="bdmirror24.User.login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../css/bootstrap.css" rel="stylesheet" media="screen" />
    <title></title>
    <style type="text/css">
        body
        {
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #f5f5f5;
        }
        
        .form-signin
        {
            max-width: 300px;
            padding: 19px 29px 29px;
            margin: 0 auto 20px;
            background-color: #fff;
            border: 1px solid #e5e5e5;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            box-shadow: 0 1px 2px rgba(0,0,0,.05);
        }
        .form-signin .form-signin-heading, .form-signin .checkbox
        {
            margin-bottom: 10px;
        }
        .form-signin input[type="text"], .form-signin input[type="password"]
        {
            font-size: 16px;
            height: auto;
            margin-bottom: 15px;
            padding: 7px 9px;
        }
    </style>
</head>
<body>
    <div>
        <div class="span12">
            <div class="navbar navbar-fixed-top">
                <div class="navbar-inner">
                    <a class="brand" href="#">BdNewz Admin</a>
                    <ul class="nav">
                        <li><a class="offset11" href="logout.php"><i class="icon-user"></i><strong>Login</strong></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <form id="form1" runat="server">
    <div>
        <div class="container">
            <div class="form-signin">
                <h2 class="">
                    Please sign in</h2>
                <asp:TextBox ID="txt_username" CssClass="input-block-level" Text="0805071@gmail.com" runat="server" placeholder="Email address"></asp:TextBox>
                <asp:TextBox TextMode="Password" ID="txt_password" CssClass="input-block-level" Text="0805071" runat="server"
                    placeholder="Password"></asp:TextBox>
                <asp:CheckBox ID="chk_remember" Text="Remember me" runat="server" />
                <asp:Label ID="lbl_msg" runat="server" Text=""></asp:Label>
                <asp:Button CssClass="pull-right btn btn-large btn-danger" ID="btn_login" OnClick="login_check"
                    ForeColor="black" runat="server" Text="Login" />
            </div>
        </div>
    </div>
    </form>
</body>
</html>
