﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;

namespace bdmirror24.User
{
    public partial class video : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #region Methods For Button
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text;
        }

        protected void imgRefresh_Click(object sender, ImageClickEventArgs e)
        {
            txtSearch.Text = string.Empty;
            grvNews.DataBind();
        }
        protected void btn_insert(object sender, EventArgs e)
        {
            bdmirrorDB.videos vid = new bdmirrorDB.videos();
            vid.caption_bn = txtbng.Text;
            vid.caption_en = txtcapen.Text;
            vid.link = txtlink.Text;
            vid.time = DateTime.Now;
            new VideoDAO().Add(vid);
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            grvNews.DataBind();
        }
        #endregion

        #region Methods For Grid
        protected void grvNews_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            //if (e.CommandName.Equals("Delete"))
            //{
            //    int intPartyID = Convert.ToInt32(e.CommandArgument.ToString());

            //    if (intPartyID > 0)
            //    {
            //        DeleteParty(intPartyID);
            //    }
            //}
        }

        protected void grvNews_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvNews.PageIndex = e.NewPageIndex;
            grvNews.DataBind();
        }

        protected void grvNews_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                //HyperLink btnEdit = (HyperLink)e.Row.FindControl("hpledit");
                //btnEdit.NavigateUrl = "~/User/edit_news/" + DataBinder.Eval(e.Row.DataItem, "id").ToString() + "/";
            }
        }
        #endregion

        public string strSearch = string.Empty;
    }
}