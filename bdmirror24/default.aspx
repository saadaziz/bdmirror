﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="bdmirror24._default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <title>BDmirror24.com-Your Trusted News Source Online</title>
    <meta name="description" content="BDmirror24.com is the most Hi-Tech, socially covered & resourced newspaper of Bangladesh. Get great Experience with us." />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Lifeforce BD Ltd">
    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- feedback -->
    <%--<link rel="stylesheet" href="feedback/demo.css" type="text/css" />--%>
    <!-- Le styles -->

    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="css/docs.css" rel="stylesheet">
    <link href="css/example_jquerytools.css" rel="stylesheet" type="text/css" />
    <link href="css/ticker-style.css" rel="stylesheet" type="text/css" />
    <style>
        .row-fluid [class*="span"]
        {
            margin-left: 5px;
            min-height:40px;
        }
        .row [class*="span"]
        {
            margin-left: 5px;
            min-height:40px;
        }
         .row-fluid .span2point5
        {
            width:18.5%;
        }
    </style>
    <link rel="shortcut icon" href="http://bdmirror24.com/img/favicon.ico">
    <script type="text/javascript" src="http://www.bdmirror24.com/js/jquery.js"></script>
    <!--
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
-->
    <script src="http://www.bdmirror24.com/js/bootstrap-carousel.js" type="text/javascript"></script>
    <script src="http://www.bdmirror24.com/js/jquery.ticker.js" type="text/javascript"></script>
    <script type="text/javascript">

        !function ($) {
            $(function () {
                $('#myCarousel').carousel({
                    interval: 2000
                });
            })
        } (window.jQuery)
    
    </script>
    <script type="text/javascript">

        !function ($) {
            $(function () {
                $('#myvid').carousel({
                    interval: 2000
                });
            })
        } (window.jQuery)
    
    </script>
    <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
    <script type="text/javascript">
        $(function () {
            $(".accordionsaad").accordion({
                event: "hoverintent"
            });
        });

        $.event.special.hoverintent = {
            setup: function () {
                $(this).bind("mouseover", jQuery.event.special.hoverintent.handler);
            },
            teardown: function () {
                $(this).unbind("mouseover", jQuery.event.special.hoverintent.handler);
            },
            handler: function (event) {
                var currentX, currentY, timeout,
args = arguments,
target = $(event.target),
previousX = event.pageX,
previousY = event.pageY;
                function track(event) {
                    currentX = event.pageX;
                    currentY = event.pageY;
                };
                function clear() {
                    target
.unbind("mousemove", track)
.unbind("mouseout", clear);
                    clearTimeout(timeout);
                }
                function handler() {
                    var prop,
orig = event;
                    if ((Math.abs(previousX - currentX) +
Math.abs(previousY - currentY)) < 7) {
                        clear();
                        event = $.Event("hoverintent");
                        for (prop in orig) {
                            if (!(prop in event)) {
                                event[prop] = orig[prop];
                            }
                        }
                        // Prevent accessing the original event since the new event
                        // is fired asynchronously and the old event is no longer
                        // usable (#6028)
                        delete event.originalEvent;
                        target.trigger(event);
                    } else {
                        previousX = currentX;
                        previousY = currentY;
                        timeout = setTimeout(handler, 100);
                    }
                }
                timeout = setTimeout(handler, 100);
                target.bind({
                    mousemove: track,
                    mouseout: clear
                });
            }
        };
</script>
    <script type="text/javascript">
        $(function () {
            $('#js-news').ticker();
        });
</script>
</head>
<body class="preview" data-spy="scroll" data-target=".subnav" data-offset="80">
    <form id="form1" runat="server">
    <div>
        <div id="fb-root">
        </div>
        <script type="text/javascript">            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                fjs.parentNode.insertBefore(js, fjs);
            } (document, 'script', 'facebook-jssdk'));</script>
        <!-- Navbar
    ================================================== -->
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner" style="height: 28px;">
                <div class="container" style="height: 28px;">
                    <div class="row">
                        <div class="span9" style="padding-top: 2px; height: 25px;">
                            <ul id="js-news" class="js-hidden">
                                <asp:Repeater runat="server" ID="rptlatest" OnItemDataBound="rptlatest_bound">
                                    <ItemTemplate>
                                        <li>
                                            <asp:HyperLink CssClass="news-item" ID="hplLink" runat="server" /></li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                        <div class="span3 pull-right">
                           <script>
                               (function () {
                                   var cx = '009803441755343019336:e4z30ioq9vs';
                                   var gcse = document.createElement('script');
                                   gcse.type = 'text/javascript';
                                   gcse.async = true;
                                   gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
                                   var s = document.getElementsByTagName('script')[0];
                                   s.parentNode.insertBefore(gcse, s);
                               })();
</script>
<gcse:searchbox></gcse:searchbox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <!-- Masthead
================================================== -->
            <header class="jumbotron subhead" id="overview">

  <div class="row-fluid">
  <div class="span9">
  <div class="row-fluid">
    <div class="span6">


    <a href="http://www.bdmirror24.com"><img id="bdmirrorlogo" src="img/logo.png" style="margin-top:15px;"></a>
     
      <!--<h1><a id="bdmirror" href="index.php">BDmirror24.com</a></h1>-->
      <!--<p class="lead">bdmirror24.com.</p>-->
    </div>

    <div class="span3"> <!--add 1-->
      <div class="">
          <asp:HyperLink ID="hpladtop1" runat="server"><asp:Image ID="imgadtop1" runat="server"></asp:Image>
          </asp:HyperLink>
      </div>
    </div> <!--add 1 end-->
      <div class="span3"> <!--add 2-->
      <div class=" ">
         <asp:HyperLink ID="hpladtop2" runat="server"><asp:Image ID="imgadtop2" runat="server"></asp:Image>
          </asp:HyperLink>
      </div>
    </div> <!--add 2 end-->


     

  </div><!--row fluid end-->
  </div><!--span9-->
  <div class="span3">
   <div class="span6"> 

    <ul>
       <li><a href="#">Forex</a></li>     
       <li><a href="#">Library</a></li>
       <li><a href="http://www.bdmirror24.com/CurrentAffairs/en" target="_blank">Current Affairs</a></li>
       
    </ul>
  

    </div>

    <div class="span6">
      <div class="adv">
          <%--<a href="#" target="_blank">????? ???????</a>   <!--bangla-->--%>
          <asp:LinkButton CssClass="btn" ID="lnklang" Font-Size="14px" runat="server" OnClick="change_lang"></asp:LinkButton>
      </div>
    </div>

   </div><!--span3-->
  </div>

  <div class="subnav" id="highlight" >

        <ul class="nav nav-pills">
        <li><a ID="hplLink"  href="default.aspx">Home</a></li>

      <asp:Repeater runat="server" ID ="rptcat" OnItemDataBound="rptCat_bound"  >
                        
            <ItemTemplate>
                <li><asp:HyperLink ID="hplLink"  runat="server"  /></li>
            </ItemTemplate>
           <%-- <FooterTemplate>
                <asp:Label ID="lblEmptyData" Text="No Category are there to display" runat="server" Visible="false">
                </asp:Label>
            </FooterTemplate>--%>
        </asp:Repeater>
        <%--<asp:SqlDataSource ID="dssCat" runat="server" ProviderName="System.Data.Odbc" CacheDuration="Infinite" EnableCaching="true"
        ConnectionString="<%$ ConnectionStrings:bdMirrorDB %>"
            SelectCommand="select name_en,name_bn from categories"></asp:SqlDataSource>--%>
            <li><asp:HyperLink id="hplgallery" runat="server" NavigateUrl="~/gallery/en/all">GALLERY</asp:HyperLink></li>
            <li><asp:HyperLink id="hplstory" runat="server">STORY</asp:HyperLink></li>
            <li><asp:HyperLink id="hplblog" runat="server">BLOG</asp:HyperLink></li>
            <li><asp:HyperLink ID="hpladvertise" runat="server" NavigateUrl="http://bdmirror24.com/Advertise/list"  >ADVERTISE</asp:HyperLink></li>
            </ul>
  </div>


  <div id="subnav1" id="highlight2">

    <ul class="nav nav-pills" style="font-size:12px;">
       <li><a href="http://www.bdmirror24.com/livecricket.aspx" target="_blank">Live Cricket</a></li>
       <li><a href="http://www.bdmirror24.com/livefootball.aspx"  target="_blank">Live Football</a></li>
       <li><a href="http://www.bdmirror24.com/BanglaTV.aspx"  target="_blank">Live Television</a></li>
       <li><a href="http://www.bdmirror24.com/liveradio.aspx"  target="_blank">Live Radio</a></li>
       <li><a href="http://www.bdmirror24.com/livecricket.aspx"  target="_blank">Free Advertisement</a></li>
    </ul>
  </div>




</header>
            <!--start contactable -->
            <!--end contactable -->
            <div class="row-fluid">
                <!--row fluid main-->
                <div class="span14">
                    <!--main left column-->
                    <div class="row-fluid">
                        <!--row fluid 1-->
                        <div class="span6">
                            <!--news column-->
                            <div class="well">
                                <div id="myCarousel" class="carousel slide">
                                    <!--slide start-->
                                    <!-- Carousel items -->
                                    <div class="carousel-inner">
                                        <asp:Repeater runat="server" ID="rptCarousal"  >
                                            <ItemTemplate>
                                                <div class="item">
                                                    <asp:Image src='<%# Eval("img_link") %>' runat="server" ID="imgCar" alt='<%# Eval("img_text") %>' />
                                                    <div class="carousel-caption">
                                                        <p>
                                                            <asp:HyperLink ID="hplcap" NavigateUrl='<%# Eval("caption_link") %>' Text='<%# Eval("caption_text") %>' runat="server"></asp:HyperLink>
                                                        </p>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                   
                                    </div>
                                </div>
                                <!--slide end-->
                            </div>
                            <!--well 1-->
                            <div class="well">
                                <!--well 2-->
                                <div class="row-fluid span12">
                                    <asp:Repeater runat="server" ID="rptmainnews">
                                        <ItemTemplate>
                                            <div class="gwrap row-fluid wellbackground">
                                                <div class="thumbnails">
                                                    <h4>
                                                        <asp:HyperLink CssClass="alink" NavigateUrl='<%# Eval("title_link") %>' ID="hplmainnews"
                                                            runat="server">'<%# Eval("title") %>'</asp:HyperLink>
                                                    </h4>
                                                    
                                                    <table cellspacing="5" cellpadding="5">
                                                        <tr>
                                                            <td>
                                                                <asp:Image ID="Image1" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                                    AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblmainnews" CssClass="newscontent12" runat="server" Text='<%# Eval("content") %>' ></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="more">
                                                    <asp:HyperLink ID="hplmainnewsrd" NavigateUrl='<%# Eval("title_link") %>' CssClass="lightsilver"
                                                        runat="server">Read More</asp:HyperLink>
                                                    <asp:HyperLink ID="hplmainpub" CssClass="pull-right lightsilver" NavigateUrl='<%# Eval("date_pub_link") %>'
                                                        runat="server"><%# Eval("date_pub")%></asp:HyperLink>
                                                        </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <div class=" gwrap row-fluid span12">
                                                <div class="thumbnails">
                                                    <h4>
                                                        <asp:HyperLink CssClass="alink" NavigateUrl='<%# Eval("title_link") %>' ID="hplmainnews"
                                                            runat="server">'<%# Eval("title") %>'</asp:HyperLink>
                                                    </h4>
                                                    <table cellspacing="5" cellpadding="5">
                                                        <tr>
                                                            <td>
                                                                <asp:Image ID="Image1" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                                    AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblmainnews" runat="server" Text='<%# Eval("content") %>'  CssClass="newscontent12"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                      <div class="more">
                                                    <asp:HyperLink ID="hplmainnewsrd" NavigateUrl='<%# Eval("title_link") %>' runat="server"
                                                        CssClass="lightsilver">Read More</asp:HyperLink>
                                                    <asp:HyperLink ID="hplmainpub" CssClass="pull-right lightsilver" NavigateUrl='<%# Eval("date_pub_link") %>'
                                                        runat="server"><%# Eval("date_pub")%></asp:HyperLink>
                                                        </div>
                                                </div>
                                            </div>
                                        </AlternatingItemTemplate>
                                    </asp:Repeater>
                                </div>
                                <% int well = 0; %>
                                <div class="row-fluid span12">
                                    <asp:Repeater runat="server" ID="rptmain2news">
                                        <ItemTemplate>
                                            <div class="span6 gwrap">
                                                <h4>
                                                    <asp:HyperLink ID="hplmain2news" CssClass="alink" NavigateUrl='<%# Eval("title_link") %>'
                                                        runat="server"><%# Eval("title") %></asp:HyperLink></h4>
                                              
                                                            <asp:Image ID="Image1" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                                AlternateText='<%# Eval("img_text") %>' Height="220px" Width="220px" />
                                                                <br />
                                                                <br />
                                                       <p>
                                                            <asp:Label ID="lblmainnews" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent12"> </asp:Label>
                                                        </p>
                                                          <div class="more">
                                                <asp:HyperLink ID="hplmain2newsrm" NavigateUrl='<%# Eval("title_link") %>' runat="server"
                                                    CssClass="lightsilver">Read More</asp:HyperLink>
                                                <asp:HyperLink ID="hplmain2pub" CssClass="pull-right lightsilver" NavigateUrl='<%# Eval("date_pub_link") %>'
                                                    runat="server"><%# Eval("date_pub")%></asp:HyperLink>
                                                    </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                                <div class="row-fluid span12">
                                    <asp:Repeater runat="server" ID="rptmain3news">
                                        <ItemTemplate>
                                            <div class="span6 gwrap">
                                                <h4>
                                                    <asp:HyperLink ID="hplmain3news" CssClass="alink" NavigateUrl='<%# Eval("title_link") %>'
                                                        runat="server"><%# Eval("title") %></asp:HyperLink></h4>
                                               
                                                            <asp:Image ID="Image1" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                                AlternateText='<%# Eval("img_text") %>' Height="220px" Width="220px" />
                                                        <br />
                                                        <br />
                                                            <p><asp:Label ID="lblmainnews" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent12"></asp:Label>
                                                       </p>
                                                         <div class="more">
                                                <asp:HyperLink ID="hplmain3newsrm" NavigateUrl='<%# Eval("title_link") %>' runat="server"
                                                    CssClass="lightsilver">Read More</asp:HyperLink>
                                                <asp:HyperLink ID="hplmain3pub" CssClass="pull-right lightsilver" NavigateUrl='<%# Eval("date_pub_link") %>'
                                                    runat="server"><%# Eval("date_pub")%></asp:HyperLink>
                                                    </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                            <!--well 2-->
                            <%--<div class="advfixed">
                                <a href="#" target="_blank">
                                    <img src="images/Advertisements/fixed_adv/shabas_bangladesh.png" border="5" margin-top="10px"></a>
                            </div>--%>
                        </div>
                        <!--the side bar just before the find us on facebook bar-->
                        <div class="span4">
                            <!--others column-->
                            <div class="">
                                <!--well tab-->
                                <div class="tabbable wellbackground">
                                    <!--tab menu-->
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#home" class="alink" data-toggle="tab"><strong>International</strong></a></li>
                                        <li><a href="#foo" class="blink" data-toggle="tab"><strong>National</strong></a></li>
                                        <li><a href="#bar" class="clink" data-toggle="tab"><strong>Sports</strong></a></li>
                                        <li><a href="#pol" class="dlink" data-toggle="tab"><strong>Politics</strong></a></li>
                                    </ul>
                                    <div class="tab-content row-fluid" style="height: auto;">
                                        <!--tab-->
                                        <div class="tab-pane active" id="home">
                                            <div class="accordionsaad" style="width: 100%">
                                                <asp:Repeater ID="rptinternational" runat="server">
                                                    <ItemTemplate>
                                                        <h5>
                                                            <asp:HyperLink ID="hplinttitle" NavigateUrl='<%# Eval("title_link") %>' runat="server">
                                                            '<%# Eval("title") %>'</asp:HyperLink>
                                                        </h5>
                                                        <div>
                                                            <p>
                                                            <table cellspacing="5" cellpadding="5"><tr><td>
                                                                <asp:Image ID="Image14" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                                    AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" />
                                                                </td>
                                                                <td colspan="2"><asp:Label ID="lblcontent" runat="server" Text='<%# Eval("content") %>'  CssClass="newscontent10"></asp:Label>
                                                            </td></tr></table>
                                                            </p>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="foo">
                                            <div class="accordionsaad" style="width: 100%">
                                                <asp:Repeater ID="rptnational" runat="server">
                                                    <ItemTemplate>
                                                        <h5>
                                                            <asp:HyperLink ID="hplinttitle" NavigateUrl='<%# Eval("title_link") %>' runat="server">
                                                            '<%# Eval("title") %>'</asp:HyperLink>
                                                        </h5>
                                                        <div>
                                                           <p>
                                                            <table cellspacing="5" cellpadding="5"><tr><td>
                                                                <asp:Image ID="Image14" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                                    AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" />
                                                                </td>
                                                                <td colspan="2"><asp:Label ID="lblcontent" runat="server" Text='<%# Eval("content") %>'  CssClass="newscontent10"></asp:Label>
                                                            </td></tr></table>
                                                            </p>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="bar">
                                            <div class="accordionsaad" style="width: 100%">
                                                <asp:Repeater ID="rptsports" runat="server">
                                                    <ItemTemplate>
                                                        <h5>
                                                            <asp:HyperLink ID="hplinttitle" NavigateUrl='<%# Eval("title_link") %>' runat="server">
                                                            '<%# Eval("title") %>'</asp:HyperLink>
                                                        </h5>
                                                        <div>
                                                            <p>
                                                            <table cellspacing="5" cellpadding="5"><tr><td>
                                                                <asp:Image ID="Image14" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                                    AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" />
                                                                </td>
                                                                <td colspan="2"><asp:Label ID="lblcontent" runat="server" Text='<%# Eval("content") %>'  CssClass="newscontent10"></asp:Label>
                                                            </td></tr></table>
                                                            </p>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="pol">
                                            <div class="accordionsaad" style="width: 100%">
                                                <asp:Repeater ID="rptpolitics" runat="server">
                                                    <ItemTemplate>
                                                        <h5>
                                                            <asp:HyperLink ID="hplinttitle" NavigateUrl='<%# Eval("title_link") %>' runat="server">
                                                            '<%# Eval("title") %>'</asp:HyperLink>
                                                        </h5>
                                                        <div>
                                                              <p>
                                                            <table cellspacing="5" cellpadding="5"><tr><td>
                                                                <asp:Image ID="Image14" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                                    AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" />
                                                                </td>
                                                                <td colspan="2"><asp:Label ID="lblcontent" runat="server" Text='<%# Eval("content") %>'  CssClass="newscontent10"></asp:Label>
                                                            </td></tr></table>
                                                            </p>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                    </div>
                                    <!--tab-->
                                </div>
                                <!--tab menu-->
                            </div>
                            <!--well tab-->
                            <!--<div class="well centered">
        
</div>-->
                            <!--
<div class="well centered">-->
                            <!-- </div>
-->
                            <div class="centered">
                                <a href="https://s3.amazonaws.com/com.alexa.toolbar/atbp/OCzRlE/download/index.htm">
                                    <img src="http://www.bdmirror24.com/img/toolbar.jpg" alt="Get our toolbar!"></a>
                                    <div align='center'><img src='http://www.hit-counts.com/counter.php?t=1255298' border='0' alt='Web Counter' /></div>
                            </div>
                            <div class="wellbackground centered">
                                <div class="tabbable">
                                    <!--tab menu-->
                                    <ul class="nav nav-tabs" id="mTabs">
                                        <li><a href="#fo" class="active" data-toggle="tab">Weather</a></li>
                                        <li><a href="#hom" data-toggle="tab">Currency</a></li>
                                        <li><a href="#ba" data-toggle="tab">Forex Rates</a></li>
                                    </ul>
                                    <div class="tab-content row-fluid">
                                        <!--tab-->
                                        <div class="tab-pane active" id="fo">
                                            <div class="scrollable vertical">
                                                <div id="test" class="items">
                                                </div>
                                            </div>
                                            <div class="navi">
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="hom">
                                            <!-- Start of likeforex.com Code -->
                                            <!-- End of likeforex.com Code -->
                                        </div>
                                        <div class="tab-pane" id="ba">
                                            Not Available Now
                                        </div>
                                    </div>
                                    <!--tab-->
                                </div>
                                <!--tab menu-->
                            </div>
                            <div class="wellbackground centered">
                                <div class="tabbable">
                                    <!--tab menu-->
                                    <ul class="nav nav-tabs" id="Ul1">
                                        <li><a href="#most" class="active" data-toggle="tab">Most Read</a></li>
                                        <li><a href="#topnews" data-toggle="tab">Top News</a></li>
                                        <li><a href="#latest" data-toggle="tab">Latest News</a></li>
                                    </ul>
                                    <div class="tab-content row-fluid">
                                        <!--tab-->
                                        <div class="tab-pane active" id="most">
                                            <div id="accordion4" class="accordionsaad" style="width: 100%">
                                                <asp:Repeater ID="rptmostread" runat="server" OnItemDataBound="rptmostread_bound">
                                                    <ItemTemplate>
                                                        <h5>
                                                            <asp:HyperLink ID="hpltoptitle" runat="server"></asp:HyperLink></h5>
                                                        <div>
                                                            <p>
                                                            <table cellspacing="5" cellpadding="5"><tr><td>
                                                                <asp:Image ID="Image14" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                                    AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" />
                                                                </td>
                                                                <td colspan="2"><asp:Label ID="lblcontent" runat="server"   CssClass="newscontent10"></asp:Label>
                                                            </td></tr></table>
                                                            </p>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="topnews">
                                            <div id="accordion5" class="accordionsaad" style="width: 100%">
                                                <asp:Repeater ID="rptTopNews" runat="server" OnItemDataBound="rpttop_bound">
                                                    <ItemTemplate>
                                                        <h5>
                                                            <asp:HyperLink ID="hpltoptitle" runat="server"></asp:HyperLink></h5>
                                                        <div>
                                                              <p>
                                                            <table cellspacing="5" cellpadding="5"><tr><td>
                                                                <asp:Image ID="Image14" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                                    AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" />
                                                                </td>
                                                                <td colspan="2"><asp:Label ID="lblcontent" runat="server"  CssClass="newscontent10"></asp:Label>
                                                            </td></tr></table>
                                                            </p>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="latest">
                                            <div id="accordion7" class="accordionsaad" style="width: 100%">
                                                <asp:Repeater ID="rptLatestRight" runat="server" OnItemDataBound="rpttop_bound">
                                                    <ItemTemplate>
                                                        <h5>
                                                            <asp:HyperLink ID="hpltoptitle" runat="server"></asp:HyperLink></h5>
                                                        <div>
                                                              <p>
                                                            <table cellspacing="5" cellpadding="5"><tr><td>
                                                                <asp:Image ID="Image14" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                                                    AlternateText='<%# Eval("img_text") %>' Height="150px" Width="150px" />
                                                                </td>
                                                                <td colspan="2"><asp:Label ID="lblcontent" runat="server"  CssClass="newscontent10"></asp:Label>
                                                            </td></tr></table>
                                                            </p>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                    </div>
                                    <!--tab-->
                                </div>
                                <!--tab menu-->
                            </div>
                            <!--well centered-->
                            <div class="wellbackground">
                            </div>
                            <div class="advpaddingless">
                                <div class="fb-like-box" style="width: auto;" data-href="http://www.facebook.com/pages/BDmirror24com/496671283700420?skip_nax_wizard=true"
                                    data-width="370" data-height="250" data-show-faces="true" data-border-color="gray"
                                    data-stream="false" data-header="true">
                                </div>
                                <div id="myvid" class="carousel slide">
                                    <!--slide start-->
                                    <!-- Carousel items -->
                                    <div class="carousel-inner">
                                        <asp:Repeater runat="server" ID="rptvid" DataSourceID="dssvid">
                                            <ItemTemplate>
                                                <div class="item">
                                                    <iframe id="Iframe1" src='<%# Eval("link") %>' width="360px" height="300px" runat="server">
                                                    </iframe>
                                                    <%-- <div class="carousel-caption">
                                                        <p>
                                                            <asp:HyperLink ID="hplcap" runat="server"><%# Eval("caption_en") %></asp:HyperLink>
                                                        </p>
                                                    </div>--%>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                       
                                        <asp:SqlDataSource ID="dssvid" runat="server" ProviderName="System.Data.Odbc" ConnectionString="<%$ ConnectionStrings:bdMirrorDB %>"
                                            SelectCommand="select * from videos order by time DESC LIMIT 0,5" CacheDuration="20"
                                            EnableCaching="true"></asp:SqlDataSource>
                                    </div>
                                     <a class="carousel-control left" href="#myvid" data-slide="prev">&lsaquo;</a> <a
                                            class="carousel-control right" href="#myvid" data-slide="next">&rsaquo;</a>
                                </div>
                            </div>
                        </div>
                        <!--others column-->
                        <div class="span2point5">
                            <asp:Repeater ID="rptimgaddright1" runat="server">
                                <ItemTemplate>
                                    <div class="advpaddingless">
                                        <a href='<%# Eval("image_url") %>'>
                                            <img src='<%# Eval("image_link") %>' alt="Advertisements" style="width: 100%; border: 1px solid #333333" />
                                        </a>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                            <asp:Repeater ID="rptimgaddright2" runat="server">
                                <ItemTemplate>
                                    <div class="advpaddingless">
                                        <a href='<%# Eval("image_url") %>'>
                                            <img src='<%# Eval("image_link") %>' alt="Advertisements" style="width: 100%; border: 1px solid #333333" />
                                        </a>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
                <div class="span12 ">
                    <%--mid news--%>
                    <div class="row-fluid toppadding">
                        <asp:Repeater ID="rptmidadd1" runat="server">
                            <ItemTemplate>
                                <div class="span3">
                                    <a href='<%# Eval("image_url") %>' target="_blank">
                                        <img src='<%# Eval("image_link") %>' alt="Advertisements" style="width: 100%; height: 65px;
                                            border: 1px solid #333333" /></a>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="row-fluid well">
                        <asp:Repeater runat="server" ID="rptmidnews1" OnItemDataBound="rptmidnews1_bound">
                            <ItemTemplate>
                                <div class="span3 ">
                                    <div class="well">
                                        <h4>
                                            <asp:HyperLink ID="hplmidnews1cat" CssClass="dlink bcolor" NavigateUrl='<%# Eval("cat_link") %>'
                                                runat="server"><%# Eval("category") %></asp:HyperLink></h4>
                                        <hr />
                                        <h5>
                                            <asp:HyperLink ID="hplmidnews1" CssClass="alink" NavigateUrl='<%# Eval("title_link") %>'
                                                runat="server"><%# Eval("title") %></asp:HyperLink></h5>
                                        <asp:Image ID="Image18" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                            AlternateText='<%# Eval("img_text") %>' Height="200px" Width="200px" />
                                        <p>
                                            <asp:Label ID="lblmidnews1" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent_corbel"></asp:Label></p>
                                        <br />
                                        <asp:HyperLink ID="hplmidnews1rm" NavigateUrl='<%# Eval("title_link") %>' CssClass="lightsilver"
                                            runat="server">Read More</asp:HyperLink>
                                        <asp:HyperLink ID="hplmidnews1datepub" CssClass="pull-right lightsilver" NavigateUrl='<%# Eval("date_pub_link") %>'
                                            runat="server"><%# Eval("date_pub")%></asp:HyperLink>
                                        <br />
                                        <div class="accordionsaad" style="width: 100%">
                                            <asp:Repeater runat="server" ID="rptmidnews1inn">
                                                <ItemTemplate>
                                                    <h5>
                                                        <asp:HyperLink ID="hpltoptitle" runat="server" NavigateUrl='<%# Eval("title_link") %>'
                                                            Text='<%# Eval("Title") %>' ToolTip='<%# Eval("cat") %>'></asp:HyperLink></h5>
                                                    <div>
                                                        <p>
                                                            <asp:Label ID="lblcontent" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent_corbel"></asp:Label>
                                                        </p>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <div class="span3 ">
                                    <div class="wellbackground">
                                        <h4>
                                            <asp:HyperLink ID="hplmidnews1cat" CssClass="blink bcolor" NavigateUrl='<%# Eval("cat_link") %>'
                                                runat="server"><%# Eval("category") %></asp:HyperLink></h4>
                                        <hr />
                                        <h5>
                                            <asp:HyperLink ID="hplmidnews1" CssClass="alink" NavigateUrl='<%# Eval("title_link") %>'
                                                runat="server"><%# Eval("title") %></asp:HyperLink></h5>
                                        <asp:Image ID="Image19" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                            AlternateText='<%# Eval("img_text") %>' Height="200px" Width="200px" />
                                        <p>
                                            <asp:Label ID="lblmidnews1" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent_corbel"></asp:Label></p>
                                        <asp:HyperLink ID="hplmidnews1rm" NavigateUrl='<%# Eval("title_link") %>' CssClass="lightsilver"
                                            runat="server">Read More</asp:HyperLink>
                                        <asp:HyperLink ID="hplmidnews1datepub" CssClass="pull-right lightsilver" NavigateUrl='<%# Eval("date_pub_link") %>'
                                            runat="server"><%# Eval("date_pub")%></asp:HyperLink>
                                        <br />
                                        <div class="accordionsaad" style="width: 100%">
                                            <asp:Repeater runat="server" ID="rptmidnews1inn">
                                                <ItemTemplate>
                                                    <h5>
                                                        <asp:HyperLink ID="hpltoptitle" runat="server" NavigateUrl='<%# Eval("title_link") %>'
                                                            Text='<%# Eval("Title") %>' ToolTip='<%# Eval("cat") %>'></asp:HyperLink></h5>
                                                    <div>
                                                        <p>
                                                            <asp:Label ID="lblcontent" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent_corbel"></asp:Label>
                                                        </p>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                            </AlternatingItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="row-fluid well">
                        <asp:Repeater runat="server" ID="rptmidnews2" OnItemDataBound="rptmidnews1_bound">
                            <ItemTemplate>
                                <div class="span3">
                                    <div class="well">
                                        <h4>
                                            <asp:HyperLink ID="hplmidnews2cat" CssClass="dlink" NavigateUrl='<%# Eval("cat_link") %>'
                                                runat="server"><%# Eval("category") %></asp:HyperLink></h4>
                                        <hr />
                                        <h5>
                                            <asp:HyperLink ID="hplmidnews2" CssClass="alink" NavigateUrl='<%# Eval("title_link") %>'
                                                runat="server"><%# Eval("title") %></asp:HyperLink></h5>
                                        <asp:Image ID="Image20" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                            AlternateText='<%# Eval("img_text") %>' Height="200px" Width="200px" />
                                        <p>
                                            <asp:Label ID="lblmidnews2" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent_corbel"></asp:Label></p>
                                        <asp:HyperLink ID="hplmidnews2rm" NavigateUrl='<%# Eval("title_link") %>' runat="server"
                                            CssClass="lightsilver">Read More</asp:HyperLink>
                                        <asp:HyperLink ID="hplmidnews2datepub" CssClass="pull-right lightsilver" NavigateUrl='<%# Eval("date_pub_link") %>'
                                            runat="server"><%# Eval("date_pub")%></asp:HyperLink>
                                        <br />
                                        <div class="accordionsaad" style="width: 100%">
                                            <asp:Repeater runat="server" ID="rptmidnews1inn">
                                                <ItemTemplate>
                                                    <h5>
                                                        <asp:HyperLink ID="hpltoptitle" runat="server" NavigateUrl='<%# Eval("title_link") %>'
                                                            Text='<%# Eval("Title") %>' ToolTip='<%# Eval("cat") %>'></asp:HyperLink></h5>
                                                    <div>
                                                        <p>
                                                            <asp:Label ID="lblcontent" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent_corbel"></asp:Label>
                                                        </p>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <div class="span3 ">
                                    <div class="wellbackground">
                                        <h4>
                                            <asp:HyperLink ID="hplmidnews1cat" CssClass="blink bcolor" NavigateUrl='<%# Eval("cat_link") %>'
                                                runat="server"><%# Eval("category") %></asp:HyperLink></h4>
                                        <hr />
                                        <h5>
                                            <asp:HyperLink ID="hplmidnews1" CssClass="alink" NavigateUrl='<%# Eval("title_link") %>'
                                                runat="server"><%# Eval("title") %></asp:HyperLink></h5>
                                        <asp:Image ID="Image21" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                            AlternateText='<%# Eval("img_text") %>' Height="200px" Width="200px" />
                                        <p>
                                            <asp:Label ID="lblmidnews1" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent_corbel"></asp:Label></p>
                                        <asp:HyperLink ID="hplmidnews1rm" NavigateUrl='<%# Eval("title_link") %>' CssClass="lightsilver"
                                            runat="server">Read More</asp:HyperLink>
                                        <asp:HyperLink ID="hplmidnews1datepub" CssClass="pull-right lightsilver" NavigateUrl='<%# Eval("date_pub_link") %>'
                                            runat="server"><%# Eval("date_pub")%></asp:HyperLink>
                                        <br />
                                        <div class="accordionsaad" style="width: 100%">
                                            <asp:Repeater runat="server" ID="rptmidnews1inn">
                                                <ItemTemplate>
                                                    <h5>
                                                        <asp:HyperLink ID="hpltoptitle" runat="server" NavigateUrl='<%# Eval("title_link") %>'
                                                            Text='<%# Eval("Title") %>' ToolTip='<%# Eval("cat") %>'></asp:HyperLink></h5>
                                                    <div>
                                                        <p>
                                                            <asp:Label ID="lblcontent" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent_corbel"></asp:Label>
                                                        </p>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                            </AlternatingItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="row-fluid">
                        <asp:Repeater ID="rptmidadd2" runat="server">
                            <ItemTemplate>
                                <div class="span3 advpaddingless">
                                    <a href='<%# Eval("image_url") %>' target="_blank">
                                        <img src='<%# Eval("image_link") %>' alt="Advertisements" style="width: 90%; height: 65px;" /></a>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="row-fluid well">
                        <asp:Repeater runat="server" ID="rptmidnews3" OnItemDataBound="rptmidnews1_bound">
                            <ItemTemplate>
                                <div class="span3">
                                    <div class="well">
                                        <h4>
                                            <asp:HyperLink ID="hplmidnews3cat" CssClass="dlink" NavigateUrl='<%# Eval("cat_link") %>'
                                                runat="server"><%# Eval("category") %></asp:HyperLink></h4>
                                        <hr />
                                        <h5>
                                            <asp:HyperLink ID="hplmidnews3" CssClass="alink" NavigateUrl='<%# Eval("title_link") %>'
                                                runat="server"><%# Eval("title") %></asp:HyperLink></h5>
                                        <asp:Image ID="Image22" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                            AlternateText='<%# Eval("img_text") %>' Height="200px" Width="200px" />
                                        <p>
                                            <asp:Label ID="lblmidnews3" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent_corbel"></asp:Label></p>
                                        <asp:HyperLink ID="hplmidnews3rm" NavigateUrl='<%# Eval("title_link") %>' CssClass="lightsilver"
                                            runat="server">Read More</asp:HyperLink>
                                        <asp:HyperLink ID="hplmidnews3datepub" CssClass="pull-right lightsilver" NavigateUrl='<%# Eval("date_pub_link") %>'
                                            runat="server"><%# Eval("date_pub")%></asp:HyperLink>
                                        <br />
                                        <div class="accordionsaad" style="width: 100%">
                                            <asp:Repeater runat="server" ID="rptmidnews1inn">
                                                <ItemTemplate>
                                                    <h5>
                                                        <asp:HyperLink ID="hpltoptitle" runat="server" NavigateUrl='<%# Eval("title_link") %>'
                                                            Text='<%# Eval("Title") %>' ToolTip='<%# Eval("cat") %>'></asp:HyperLink></h5>
                                                    <div>
                                                        <p>
                                                            <asp:Label ID="lblcontent" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent_corbel"></asp:Label>
                                                        </p>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                             <AlternatingItemTemplate>
                                <div class="span3 ">
                                    <div class="wellbackground">
                                        <h4>
                                            <asp:HyperLink ID="hplmidnews1cat" CssClass="blink bcolor" NavigateUrl='<%# Eval("cat_link") %>'
                                                runat="server"><%# Eval("category") %></asp:HyperLink></h4>
                                        <hr />
                                        <h5>
                                            <asp:HyperLink ID="hplmidnews1" CssClass="alink" NavigateUrl='<%# Eval("title_link") %>'
                                                runat="server"><%# Eval("title") %></asp:HyperLink></h5>
                                        <asp:Image ID="Image21" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                            AlternateText='<%# Eval("img_text") %>' Height="200px" Width="200px" />
                                        <p>
                                            <asp:Label ID="lblmidnews1" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent_corbel"></asp:Label></p>
                                        <asp:HyperLink ID="hplmidnews1rm" NavigateUrl='<%# Eval("title_link") %>' CssClass="lightsilver"
                                            runat="server">Read More</asp:HyperLink>
                                        <asp:HyperLink ID="hplmidnews1datepub" CssClass="pull-right lightsilver" NavigateUrl='<%# Eval("date_pub_link") %>'
                                            runat="server"><%# Eval("date_pub")%></asp:HyperLink>
                                        <br />
                                        <div class="accordionsaad" style="width: 100%">
                                            <asp:Repeater runat="server" ID="rptmidnews1inn">
                                                <ItemTemplate>
                                                    <h5>
                                                        <asp:HyperLink ID="hpltoptitle" runat="server" NavigateUrl='<%# Eval("title_link") %>'
                                                            Text='<%# Eval("Title") %>' ToolTip='<%# Eval("cat") %>'></asp:HyperLink></h5>
                                                    <div>
                                                        <p>
                                                            <asp:Label ID="lblcontent" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent_corbel"></asp:Label>
                                                        </p>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                            </AlternatingItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="row-fluid well">
                        <asp:Repeater runat="server" ID="rptmidnews4" OnItemDataBound="rptmidnews1_bound">
                            <ItemTemplate>
                                <div class="span3">
                                    <div class="well">
                                        <h4>
                                            <asp:HyperLink ID="hplmidnews4cat" CssClass="dlink" NavigateUrl='<%# Eval("cat_link") %>'
                                                runat="server"><%# Eval("category") %></asp:HyperLink></h4>
                                        <hr />
                                        <h5>
                                            <asp:HyperLink ID="hplmidnews4" CssClass="alink" NavigateUrl='<%# Eval("title_link") %>'
                                                runat="server"><%# Eval("title") %></asp:HyperLink></h5>
                                        <asp:Image ID="Image23" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                            AlternateText='<%# Eval("img_text") %>' Height="200px" Width="200px" />
                                        <p>
                                            <asp:Label ID="lblmidnews4" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent_corbel"></asp:Label></p>
                                        <asp:HyperLink ID="hplmidnews4rm" NavigateUrl='<%# Eval("title_link") %>' runat="server"
                                            CssClass="lightsilver">Read More</asp:HyperLink>
                                        <asp:HyperLink ID="hplmidnews4datepub" CssClass="pull-right lightsilver" NavigateUrl='<%# Eval("date_pub_link") %>'
                                            runat="server"><%# Eval("date_pub")%></asp:HyperLink>
                                        <br />
                                        <div class="accordionsaad" style="width: 100%">
                                            <asp:Repeater runat="server" ID="rptmidnews1inn">
                                                <ItemTemplate>
                                                    <h5>
                                                        <asp:HyperLink ID="hpltoptitle" runat="server" NavigateUrl='<%# Eval("title_link") %>'
                                                            Text='<%# Eval("Title") %>' ToolTip='<%# Eval("cat") %>'></asp:HyperLink></h5>
                                                    <div>
                                                        <p>
                                                            <asp:Label ID="lblcontent" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent_corbel"></asp:Label>
                                                        </p>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                             <AlternatingItemTemplate>
                                <div class="span3 ">
                                    <div class="wellbackground">
                                        <h4>
                                            <asp:HyperLink ID="hplmidnews1cat" CssClass="blink bcolor" NavigateUrl='<%# Eval("cat_link") %>'
                                                runat="server"><%# Eval("category") %></asp:HyperLink></h4>
                                        <hr />
                                        <h5>
                                            <asp:HyperLink ID="hplmidnews1" CssClass="alink" NavigateUrl='<%# Eval("title_link") %>'
                                                runat="server"><%# Eval("title") %></asp:HyperLink></h5>
                                        <asp:Image ID="Image21" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                            AlternateText='<%# Eval("img_text") %>' Height="200px" Width="200px" />
                                        <p>
                                            <asp:Label ID="lblmidnews1" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent_corbel"></asp:Label></p>
                                        <asp:HyperLink ID="hplmidnews1rm" NavigateUrl='<%# Eval("title_link") %>' CssClass="lightsilver"
                                            runat="server">Read More</asp:HyperLink>
                                        <asp:HyperLink ID="hplmidnews1datepub" CssClass="pull-right lightsilver" NavigateUrl='<%# Eval("date_pub_link") %>'
                                            runat="server"><%# Eval("date_pub")%></asp:HyperLink>
                                        <br />
                                        <div class="accordionsaad" style="width: 100%">
                                            <asp:Repeater runat="server" ID="rptmidnews1inn">
                                                <ItemTemplate>
                                                    <h5>
                                                        <asp:HyperLink ID="hpltoptitle" runat="server" NavigateUrl='<%# Eval("title_link") %>'
                                                            Text='<%# Eval("Title") %>' ToolTip='<%# Eval("cat") %>'></asp:HyperLink></h5>
                                                    <div>
                                                        <p>
                                                            <asp:Label ID="lblcontent" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent_corbel"></asp:Label>
                                                        </p>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                            </AlternatingItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="row-fluid">
                        <asp:Repeater ID="rptmidAdd3" runat="server" OnItemDataBound="rptmidnews1_bound">
                            <ItemTemplate>
                                <div class="span3 advpaddingless">
                                    <a href='<%# Eval("image_url") %>' target="_blank">
                                        <img src='<%# Eval("image_link") %>' alt="Advertisements" style="width: 90%; height: 65px;" /></a>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="row-fluid well">
                        <asp:Repeater runat="server" ID="rptmidnews5" OnItemDataBound="rptmidnews1_bound">
                            <ItemTemplate>
                                <div class="span3">
                                    <div class="well">
                                        <h4>
                                            <asp:HyperLink ID="hplmidnews5cat" CssClass="dlink" NavigateUrl='<%# Eval("cat_link") %>'
                                                runat="server"><%# Eval("category") %></asp:HyperLink></h4>
                                        <hr />
                                        <h5>
                                            <asp:HyperLink ID="hplmidnews5" CssClass="alink" NavigateUrl='<%# Eval("title_link") %>'
                                                runat="server"><%# Eval("title") %></asp:HyperLink></h5>
                                        <asp:Image ID="Image24" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                            AlternateText='<%# Eval("img_text") %>' Height="200px" Width="200px" />
                                        <p>
                                            <asp:Label ID="lblmidnews5" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent_corbel"></asp:Label></p>
                                        <asp:HyperLink ID="hplmidnews5rm" NavigateUrl='<%# Eval("title_link") %>' runat="server"
                                            CssClass="lightsilver">Read More</asp:HyperLink>
                                        <asp:HyperLink ID="hplmidnews5datepub" CssClass="pull-right lightsilver" NavigateUrl='<%# Eval("date_pub_link") %>'
                                            runat="server"><%# Eval("date_pub")%></asp:HyperLink>
                                        <br />
                                        <div class="accordionsaad" style="width: 100%">
                                            <asp:Repeater runat="server" ID="rptmidnews1inn">
                                                <ItemTemplate>
                                                    <h5>
                                                        <asp:HyperLink ID="hpltoptitle" runat="server" NavigateUrl='<%# Eval("title_link") %>'
                                                            Text='<%# Eval("Title") %>' ToolTip='<%# Eval("cat") %>'></asp:HyperLink></h5>
                                                    <div>
                                                        <p>
                                                            <asp:Label ID="lblcontent" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent_corbel"></asp:Label>
                                                        </p>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                             <AlternatingItemTemplate>
                                <div class="span3 ">
                                    <div class="wellbackground">
                                        <h4>
                                            <asp:HyperLink ID="hplmidnews1cat" CssClass="blink bcolor" NavigateUrl='<%# Eval("cat_link") %>'
                                                runat="server"><%# Eval("category") %></asp:HyperLink></h4>
                                        <hr />
                                        <h5>
                                            <asp:HyperLink ID="hplmidnews1" CssClass="alink" NavigateUrl='<%# Eval("title_link") %>'
                                                runat="server"><%# Eval("title") %></asp:HyperLink></h5>
                                        <asp:Image ID="Image21" runat="server" ImageAlign="Middle" ImageUrl='<%# Eval("img_url") %>'
                                            AlternateText='<%# Eval("img_text") %>' Height="200px" Width="200px" />
                                        <p>
                                            <asp:Label ID="lblmidnews1" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent_corbel"></asp:Label></p>
                                        <asp:HyperLink ID="hplmidnews1rm" NavigateUrl='<%# Eval("title_link") %>' CssClass="lightsilver"
                                            runat="server">Read More</asp:HyperLink>
                                        <asp:HyperLink ID="hplmidnews1datepub" CssClass="pull-right lightsilver" NavigateUrl='<%# Eval("date_pub_link") %>'
                                            runat="server"><%# Eval("date_pub")%></asp:HyperLink>
                                        <br />
                                        <div class="accordionsaad" style="width: 100%">
                                            <asp:Repeater runat="server" ID="rptmidnews1inn">
                                                <ItemTemplate>
                                                    <h5>
                                                        <asp:HyperLink ID="hpltoptitle" runat="server" NavigateUrl='<%# Eval("title_link") %>'
                                                            Text='<%# Eval("Title") %>' ToolTip='<%# Eval("cat") %>'></asp:HyperLink></h5>
                                                    <div>
                                                        <p>
                                                            <asp:Label ID="lblcontent" runat="server" Text='<%# Eval("content") %>' CssClass="newscontent_corbel"></asp:Label>
                                                        </p>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                            </AlternatingItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <!--adv column-->
            </div>
            <!--row fluid lowerpart-->
        </div>
        <!--main left column-->
        <!--main right/adv column-->
    </div>
    <!--row fluid main-->
    <br />
    <%--</div>--%>
    <!--container start in header -->
    <!-- Footer
 ================================================== -->
    <hr />
    <hr />
    <footer class="footer">
        <div class="row-fluid botpadding">
          <div class="span2 leftpadding">
          <h5>BDMIRROR24.COM</h5>
          <ul style="list-style:none;">
          <li class="leftpadding5 botpadding5" ><a href="http://www.bdmirror24.com/aboutus/" target="_blank" class="wlink">About Us</a></li>
          <li class="leftpadding5 botpadding5"><a href="http://www.bdmirror24.com/mission/" target="_blank" class="wlink">Mission & Vision</a></li>
		  <li class="leftpadding5 botpadding5"><a href="http://www.bdmirror24.com/administration/" target="_blank" class="wlink">Administration</a></li>
		  <li class="leftpadding5 botpadding5"><a href="http://www.bdmirror24.com/social/" target="_blank" class="wlink">Social Welfare</a></li>
		  <li class="leftpadding5 botpadding5"><a href="http://www.bdmirror24.com/partners/" target="_blank" class="wlink">Partners</a></li>
          </ul>
          </div>
          <div class="span2 leftpadding">
          <h5>NEWS</h5>
          <ul  style="list-style:none;>
          <li class="leftpadding5 botpadding5" ><a href="http://www.bdmirror24.com/news/en/NATIONAL/" target="_blank" class="wlink">National</a></li>
          <li class="leftpadding5 botpadding5"><a href="http://www.bdmirror24.com/news/en/POLITICS/" target="_blank" class="wlink">Politics</a></li>
		  <li class="leftpadding5 botpadding5"><a href="http://www.bdmirror24.com/news/en/INTERNATIONAL/" target="_blank" class="wlink">International</a></li>
		  <li class="leftpadding5 botpadding5"><a href="http://www.bdmirror24.com/news/en/WORLDWIDE/" target="_blank" class="wlink">Worldwide</a></li>
		  <li class="leftpadding5 botpadding5"><a href="http://www.bdmirror24.com/news/en/BUSINESS/" target="_blank" class="wlink">Business</a></li>
		  <li class="leftpadding5 botpadding5"><a href="http://www.bdmirror24.com/news/en/ECONOMY/" target="_blank" class="wlink">Economy</a></li>
          </ul>
         
        
          </div>
          <div class="span2">
           <h5>ENTERTAIN</h5>
          <ul  style="list-style:none;>
          <li class="leftpadding5 botpadding5"><a href="http://www.bdmirror24.com/news/en/CRICKET/" target="_blank" class="wlink">Cricket</a></li>
          <li class="leftpadding5 botpadding5"><a href="http://www.bdmirror24.com/news/en/SPORTS/" target="_blank" class="wlink">Sports</a></li>
          <li class="leftpadding5 botpadding5" ><a href="http://www.bdmirror24.com/news/en/ENTERTAINMENT/" target="_blank" class="wlink">Entertainment</a></li>
		  <li class="leftpadding5 botpadding5" ><a href="http://www.bdmirror24.com/news/en/STORY/" target="_blank" class="wlink">Story</a></li>
          </ul>
          </div>
          <div class="span2"> <h5>KNOWLEDGE</h5>
          <ul  style="list-style:none;>
          <li class="leftpadding5 botpadding5"><a href="http://www.bdmirror24.com/news/en/TECHNOLOGY/" target="_blank" class="wlink">Technology</a></li>
          <li class="leftpadding5 botpadding5"><a href="http://www.bdmirror24.com/news/en/INFO-TECH/" target="_blank" class="wlink">Info-Tech</a></li>
          <li class="leftpadding5 botpadding5" ><a href="http://www.bdmirror24.com/news/en/SCIENCE/" target="_blank" class="wlink">Science</a></li>
		  <li class="leftpadding5 botpadding5" ><a href="http://www.bdmirror24.com/news/en/INTERNATIONAL/" target="_blank" class="wlink">Education</a></li>
		  <li class="leftpadding5 botpadding5" ><a href="http://www.bdmirror24.com/news/en/INTERNATIONAL/" target="_blank" class="wlink">Religion</a></li>
          </ul>
          </div>
          <div class="span2"> <h5>SPECIAL</h5>
          <ul  style="list-style:none;>
          <li class="leftpadding5 botpadding5"><a href="http://www.bdmirror24.com/news/en/EDITORIAL/" class="wlink">Editorial</a></li>
          <li class="leftpadding5 botpadding5"><a href="http://www.bdmirror24.com/news/en/LAW/" class="wlink">LAW</a></li>
		  <li class="leftpadding5 botpadding5"><a href="http://www.bdmirror24.com/news/en/BLOG/" class="wlink">Blog</a></li>
          <li class="leftpadding5 botpadding5"><a href="http://www.bdmirror24.com/gallery/en/all/" class="wlink">Gallery</a></li>
          <li class="leftpadding5 botpadding5" ><a href="http://www.bdmirror24.com/Advertise/list/" target="_blank" class="wlink">Advertise</a></li>
          </ul>
          </div>
          <div class="span2"> <h5>LIVE</h5>
          <ul  style="list-style:none;>
          <li class="leftpadding5 botpadding5"><a href="http://www.bdmirror24.com/livecricket.aspx" target="_blank" class="wlink">Live Cricket</a></li>
          <li class="leftpadding5 botpadding5"><a href="http://www.bdmirror24.com/livefootball.aspx" target="_blank" class="wlink">Live Football</a></li>
          <li class="leftpadding5 botpadding5" ><a href="http://www.bdmirror24.com/LiveTV.aspx" target="_blank" class="wlink">Live TV</a></li>
		  <li class="leftpadding5 botpadding5" ><a href="http://www.bdmirror24.com/news/en/INTERNATIONAL/" target="_blank" class="wlink">Live Radio</a></li>
          </ul>
          </div>
        
          <p class="pull-right">All Rights Reserved <strong>BDmirror24.com ©2013&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></p>
             </div>


      
        </footer>
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--
   <script src="js/jquery-1.8.1.min.js"></script>
   -->
    <script src="http://www.bdmirror24.com/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).scroll(function () {
            // If has not activated (has no attribute "data-top"
            if (!$('.subnav').attr('data-top')) {
                // If already fixed, then do nothing
                if ($('.subnav').hasClass('subnav-fixed')) return;
                // Remember top position
                var offset = $('.subnav').offset()
                $('.subnav').attr('data-top', offset.top);
            }

            if ($('.subnav').attr('data-top') - $('.subnav').outerHeight() <= $(this).scrollTop())
                $('.subnav').addClass('subnav-fixed');
            else
                $('.subnav').removeClass('subnav-fixed');
        });
    </script>
    <script src="http://www.bdmirror24.com/js/bootswatch.js"></script>
    <!-- zWeatherFeed plugin scripts (required) 

<script src="http://cdn.jquerytools.org/1.1.2/full/jquery.tools.min.js"></script>-->
    <script src="js/weather.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#test').weatherfeed(['BGXX0003'//,'BGXX0006','BGXX0002','BGXX0005','BGXX0010'
                    ], {}, function (e) {
                        $("div.scrollable").scrollable({
                            vertical: true,
                            size: 1
                        }).circular().navigator().autoscroll({
                            interval: 3000
                        });
                    });
        });
    </script>
    <%--  <script type="text/javascript">
        $(function () {
            var baseURL = 'bdmirror24.com';
            //load content for first tab and initialize
            $('#home').load(baseURL + 'home', function () {
                $('#myTabs').tab(); //initialize tabs
            });
            $('#myTabs').bind('show', function (e) {
                var pattern = /#.+/gi //use regex to get anchor(==selector)
                var contentID = e.target.toString().match(pattern)[0]; //get anchor
                //load content for selected tab
                $(contentID).load(baseURL + contentID.replace('#', ''), function () {
                    $('#myTabs').tab(); //reinitialize tabs
                });
            });
        });
    </script>--%>
    <script type="text/javascript">
        $('#mTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        })
    </script>
    <%--
    <link rel="stylesheet" href="css/jquery-ui.css" />--%>
    </form>
    <script type="text/javascript">
        var _urq = _urq || [];
        _urq.push(['initSite', 'cf91ab06-46ff-4981-bc03-af77da6ee388']);
        (function () {
            var ur = document.createElement('script'); ur.type = 'text/javascript'; ur.async = true;
            ur.src = 'http://sdscdn.userreport.com/userreport.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ur, s);
        })();
    </script>
    <style type="text/css">
        .ui-accordion-content
        {
            height: 220px;
        }
    </style>
    
    <style type="text/css">
    
        .Robi-Ad
        {
            padding: 0px;
            width: 500px;
            height: 307px;
            background-color: transparent;
            bottom: 20%;
            position: fixed;
        }
        .Robi-Ad a
        {
            border: none;
        }
        .Cell-Ad
        {
            padding: 0px;
            width: 187px;
            background-color: transparent;
            bottom: 45%;
            position: fixed;
            opacity: 0.9;
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=90)";
            filter: alpha(opacity=90);
        }
        .Cell-Ad a
        {
            border: none;
        }
    </style>
    <script src="js/Robi_Pop_Up.js"></script>
    <script>
        $(function () {
            $('.Robi-Ad').tabSlideOut({
                tabHandle: '.handle',
                pathToTabImage: 'img/logo.jpg',
                imageHeight: '27px',
                imageWidth: '77px',
                tabLocation: 'right',
                speed: 400,
                action: 'click',
                bottomPos: '0px',
                fixedPosition: true
            });
        });
        $(function () {
            $('.Cell-Ad').tabSlideOut({
                tabHandle: '.Cell',
                pathToTabImage: 'img/Khoborghor.jpg',
                imageHeight: '120px',
                imageWidth: '49px',
                tabLocation: 'left',
                speed: 250,
                action: 'click',
                bottomPos: '40%',
                fixedPosition: true
            });
        });
         </script>
    <div class="Robi-Ad" style="line-height: 1; position: fixed; height: 27px; right: -600px;">
        <img class="handle" style="width: 60px; height: 50px; display: block; text-indent: -99999px;
            outline: none; position: absolute; top: 0px; left: -60px; background-position: initial initial;
            background-repeat: no-repeat no-repeat;" />
        <a href="#" target="_blank" style="border: none;">
            <img src="img/Robi_lowerRightAd.jpg" height="307" width="500"></a>
    </div>
    <div style="line-height: 1; position: fixed; height: 120px; left: -187px;" class="Cell-Ad">
        <img class="Cell" style="width: 24px; height: 120px; display: block; text-indent: -99999px;
            outline: none; position: absolute; top: 0px; right: -24px;" />
        <a href="#" target="_blank" style="border: none;">
            <img src="img/khoborghor_Img.jpg" height="120" width="187"></a>
    </div>
</body>
</html>
