﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

using bdmirror24.DAL.DAL_Functions.Implementations;
using bdmirror24.DAL.Manager;
using bdmirror24.DAL.mappings;
using bdmirror24;
using bdmirror24.couchbase;
namespace bdmirror24
{
    public partial class news : System.Web.UI.Page
    {
        private static int i,start;
        private static string language;
        bdmirrorDB.news dbnews = new bdmirrorDB.news();
        Utility utiob = new Utility();     
        private bdmirrorDB.categories cat = new bdmirrorDB.categories();
        protected void getmore_click(object sender, EventArgs e)
        {
            loadRepeater(start + 9);
        }
        protected void carousel_image_load(bdmirrorDB.categories cat)
        {
            string query = (language != "bn") ? @"select id from news where title_en!='' and published='1' and category_id=" + cat.cat_id + " order by rand() limit 0,8;" :
                @"select id from news where title_bn!='' and published='1' and category_id="+cat.cat_id+" order by rand() limit 0,8;";
            IList<bdmirrorDB.news> topnews = getIlistNews(ndao.GetCategoryNews(query));
            DataTable dtCar = new DataTable("dtCar");
            dtCar.Columns.Add("id");
            dtCar.Columns.Add("caption_link");
            dtCar.Columns.Add("caption_text");
            dtCar.Columns.Add("img_link");
            dtCar.Columns.Add("img_text");
            foreach (bdmirrorDB.news nz in topnews)
            {
                bdmirrorDB.images img = get_img(nz);
                if (language != "bn")
                    dtCar.Rows.Add(new string[] { nz.news_id.ToString(), "~/news/" + language + "/" + nz.category_id.name_en + "/" + nz.news_id + "/" + utiob.replaceSpace(nz.title_en), nz.title_en, img.link, img.caption_en });
                else
                    dtCar.Rows.Add(new string[] { nz.news_id.ToString(), "~/news/" + language + "/" + nz.category_id.name_bn + "/" + nz.news_id + "/" + utiob.replaceSpace(nz.title_bn), nz.title_bn, img.link, img.caption_bn });

            }
            rptCarousal.DataSource = dtCar;
            rptCarousal.DataBind();
        }
        protected void rptimg_bound(object sender, RepeaterItemEventArgs e)
        {
            HyperLink caption = (HyperLink)e.Item.FindControl("hplcap");
            string category = string.Empty;
            string title = string.Empty;
            int news_id = 0;

            category = DataBinder.Eval(e.Item.DataItem, "cat_en").ToString() == null ? "NO Category".ToString() : DataBinder.Eval(e.Item.DataItem, "cat_en").ToString();

            news_id = int.Parse(DataBinder.Eval(e.Item.DataItem, "news_id").ToString());

            title = (language == "en") ? DataBinder.Eval(e.Item.DataItem, "title_en").ToString() : DataBinder.Eval(e.Item.DataItem, "title_bn").ToString();

            if (caption != null)
            {
                caption.NavigateUrl = "news/" + language + "/" + category + "/" + news_id + "/" + utiob.replaceSpace(title);
                caption.Text = title;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                i = 0;
                start = 0;
                if (Page.RouteData.Values.Count == 2)
                {
                    if (Page.RouteData.Values["category"].ToString() != ""
                        && (Page.RouteData.Values["lang"].ToString() == "bn" ||
                            Page.RouteData.Values["lang"].ToString() == "en"))
                    {
                        lblcat.Text = (Server.UrlDecode(Page.RouteData.Values["category"].ToString()));
                        language = Page.RouteData.Values["lang"].ToString();

                        cat =
                            new CategoryDAO().getcatbyName(Server.UrlDecode(Page.RouteData.Values["category"].ToString()));
                        dbnews = new NewsDAO().getLastNewsbyCat(cat,language);
//                        dssimg.SelectCommand =(language=="en")? @"SELECT i.caption_en,i.link,i.image_name,i.height,i.width,c.name_en as cat_en,c.name_bn as cat_bn,
//                                                            n.id as news_id,n.title_en,n.title_bn
//                                                            FROM news as n,images as i,categories as c
//                                                            WHERE n.id=i.news_id AND
//                                                            n.category_id=c.id and c.name_en='"+cat.name_en+"' LIMIT 0,5":
//                                                            @"SELECT i.caption_en,i.link,i.image_name,i.height,i.width,c.name_en as cat_en,c.name_bn as cat_bn,
//                                                            n.id as news_id,n.title_en,n.title_bn
//                                                            FROM news as n,images as i,categories as c
//                                                            WHERE n.id=i.news_id AND
//                                                            n.category_id=c.id and c.name_en='" + cat.name_bn + "' LIMIT 0,5";
                        carousel_image_load(cat);
                        //page title and keyword here
                        string keywords = (language != "bn") ? cat.name_en : cat.name_bn + ",";
                        if (language != "bn")
                        {
                            IList<bdmirrorDB.keywords> keys = new KeywrdsDAO().GetKeywordsbyNewsID(dbnews);
                            foreach (bdmirrorDB.keywords keywordse in keys)
                            {
                                keywords += keywordse.keyword + ",";
                            }
                        }
                        else
                        {
                            IList<bdmirrorDB.keywords_bn> keys = new KeywrdsbnDAO().GetKeywordsbyNewsID(dbnews);
                            foreach (bdmirrorDB.keywords_bn keywordse in keys)
                            {
                                keywords += keywordse.keyword + ",";
                            }
                        }
                        Page.MetaKeywords = keywords;
                        Page.Title = (language != "bn")
                                         ? cat.name_en+ " -- BDMirror24.com : Your Trusted Online Newspaper"
                                         : cat.name_bn + " -- BDMirror24.com : Your Trusted Online Newspaper";

                        hplmaintitle.Text = (language != "bn") ? dbnews.title_en : dbnews.title_bn;
                        hplmaintitle.NavigateUrl = "/news/" + Page.RouteData.Values["lang"] + "/" +
                                                   Page.RouteData.Values["category"] + "/" +
                                                   dbnews.news_id + "/" + utiob.replaceSpace(hplmaintitle.Text);
                        hplmainrd.NavigateUrl = hplmaintitle.NavigateUrl;
                        hplwritermain.Text = (dbnews.writer_name == null)
                                                 ? "Anonymous Writer".ToString()
                                                 : dbnews.writer_name.ToString();
                        if (dbnews.date.ToString() != null)
                        {
                            string[] date = dbnews.date.ToString().Split(' ')[0].Split('/');
                            //string[] date = all[0].Split('/');
                            hplmaindatepub.Text = date[1] + "-";
                            //string strMonthName = date[1];

                            string strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(int.Parse(date[0]));
                            hplmaindatepub.Text += strMonthName;
                            hplmaindatepub.Text += "-" + date[2];
                            hplmaindatepub.NavigateUrl = "date/" + Page.RouteData.Values["lang"] + "/" +
                                                         date[1]+"-"+date[0]+"-"+date[2]+"/";

                        }
                        lblmainnews.Text = (language != "bn") ? dbnews.content_en : dbnews.content_bn;
                        lblmainnews.Text += " ...";

                        //repeater for rows

                        loadRepeater(start);

                        load_allrepeaters();
                    }
                    else
                        Response.Redirect("http://www.bdmirror24.com");
                }
                //if (Page.RouteData.Values["title"] != null)
                //    Response.Write( Server.UrlDecode(Page.RouteData.Values["title"].ToString()));

            }
        }

        protected void loadRepeater(int started)
        {
            
            DataTable dt = new DataTable("Rows");
            dt.Columns.Add("limit1", typeof(string));
            dt.Columns.Add("limit2", typeof(string));
            for (Int32 r = started; r < 21 + started; r += 3)
            {
                dt.Rows.Add(new string[] { r.ToString(), 3.ToString() });
                //start = r;    
            }
            start = started;
            rptRow.DataSource = dt;
            rptRow.DataBind();
            
            
        }
        static IList<string> top_list = new List<string>();
        private bdmirrorDB.images get_img(bdmirrorDB.news nz)
        {
            bdmirrorDB.images img = imgdao.GetImagebyNewsID(nz);
            if (img == null)
            {
                img = new bdmirrorDB.images();
                img.link = "";
                img.caption_en = "";
            }
            return img;
        }
        #region repeater
        protected void rptinnNews_bound(object sender, RepeaterItemEventArgs e)
        {
            SqlDataSource dssNews = (SqlDataSource) e.Item.FindControl("dssNews");
            if (dssNews != null)
            {
                string sql = (language == "en") ? @"select id 
                                            from news 
                                            where published='1' and title_en!='' and category_id =" + dbnews.category_id.cat_id.ToString() +
                                            @" ORDER BY date DESC  
                                            limit " + int.Parse(DataBinder.Eval(e.Item.DataItem, "limit1").ToString()) + ",3"
                                                    :
                                                    @"select id 
                                            from news 
                                            where published='1' and title_bn!='' and category_id =" + dbnews.category_id.cat_id.ToString() +
                                            @" ORDER BY date DESC  
                                            limit " + int.Parse(DataBinder.Eval(e.Item.DataItem, "limit1").ToString()) + ",3";
                IList<int> newsid =ndao.GetCategoryNews( sql);
                IList<bdmirrorDB.news> news_list = getIlistNews(newsid);
                DataTable dtnz = new DataTable("dtnz");
                dtnz.Columns.Add("title_en");
                dtnz.Columns.Add("title_bn");
                dtnz.Columns.Add("id");
                dtnz.Columns.Add("content_en");
                dtnz.Columns.Add("content_bn");
                dtnz.Columns.Add("date");
                dtnz.Columns.Add("img_url");
                dtnz.Columns.Add("img_text");
                foreach (bdmirrorDB.news nz in news_list)
                {
                    bdmirrorDB.images img = get_img(nz);
                    dtnz.Rows.Add(new string[] { nz.title_en, nz.title_bn, nz.news_id.ToString(),nz.content_en,nz.content_bn,nz.date.ToShortDateString(),img.link,nz.title_en });
                }
                Repeater rptinnNews = (Repeater)e.Item.FindControl("rptinnNews");
                rptinnNews.DataSource = dtnz;
                rptinnNews.DataBind();
              
            }
        }
        protected IList<bdmirrorDB.news> getIlistNews(IList<int> res)
        {
            //IList<int> res = ndao.getHomeNews("en");
            couchbasemain cbm = new couchbasemain();
            IList<bdmirrorDB.news> news_list = new List<bdmirrorDB.news>();

            foreach (int id in res)
            {
                bdmirrorDB.news news = cbm.getNews(id);
                if (news == null)
                {
                    news = ndao.pickbyID(id); cbm.setnews(news);

                }
                news_list.Add(news);
            }
            return news_list;
        }
        NewsDAO ndao = new NewsDAO();
        int r = 0;
        protected void rptRow_bound(object sender, RepeaterItemEventArgs e)
        {
            Label lblnews = (Label) e.Item.FindControl("lblnews");
            HyperLink hplnews = (HyperLink) e.Item.FindControl("hplnews");
            HyperLink hplnewsrd = (HyperLink) e.Item.FindControl("hplnewsrd");
            Label lblpub = (Label) e.Item.FindControl("lblpub");
            HyperLink hplpub = (HyperLink) e.Item.FindControl("hplpub");

            //bdmirrorDB.news tempnews=new NewsDAO().pickbyID(int.Parse((DataBinder.Eval(e.Item.DataItem,"id")).ToString()));
            bool is_english = false;
            is_english = (language == "en") ? true : false;

            if (hplnews != null)
            {
                hplnews.Text = (is_english == true)
                    ? DataBinder.Eval(e.Item.DataItem, "title_en").ToString() : DataBinder.Eval(e.Item.DataItem, "title_bn").ToString();
                hplnews.NavigateUrl = "/news/" + language + "/" +
                                           Page.RouteData.Values["category"] + "/" +
                                           DataBinder.Eval(e.Item.DataItem, "id").ToString() + "/" + utiob.replaceSpace(hplnews.Text);
                hplnewsrd.NavigateUrl = hplnews.NavigateUrl;
                
            }
            if (lblnews != null)
            {
                  lblnews.Text = (is_english)
                        ? DataBinder.Eval(e.Item.DataItem, "content_en").ToString() : DataBinder.Eval(e.Item.DataItem, "content_bn").ToString();
                    lblnews.Text += " ...";
            }
            if (DataBinder.Eval(e.Item.DataItem, "date").ToString() != null)
            {
                string[] date = DataBinder.Eval(e.Item.DataItem, "date").ToString().Split('/');
                //string[] date = all[0].Split('/');
                hplpub.Text = date[1] + "-";
                //string strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(int.Parse(date[0]));
                hplpub.Text += date[0];
                hplpub.Text += "-" + date[2];
                hplpub.NavigateUrl = "date/" + Page.RouteData.Values["lang"] + "/" + date[1] + "-" + date[0] + "-" +  date[2] + "/";

            }
        }
        #endregion
       
        #region other repeater
        ImageDAO imgdao = new ImageDAO();
        
        public void load_allrepeaters()
        {
            DataTable dtTOp1 = new DataTable("dsslatest2");
            dtTOp1.Columns.Add("title_en", typeof(string));
            dtTOp1.Columns.Add("title_bn", typeof(string));
            dtTOp1.Columns.Add("news_id", typeof(string));
            dtTOp1.Columns.Add("name_en", typeof(string));
            dtTOp1.Columns.Add("name_bn", typeof(string));
            dtTOp1.Columns.Add("img_url", typeof(string));
            dtTOp1.Columns.Add("img_text", typeof(string));
            dtTOp1.Columns.Add("content_en", typeof(string));
            dtTOp1.Columns.Add("content_bn", typeof(string));
            //IList<bdmirrorDB.categories> allcat = new CategoryDAO().All();

            IList<int> latestNews = (language == "bn") ?
                ndao.GetCategoryNews(@"SELECT id from news where title_en!='' order by  date desc limit 0,5") :
                ndao.GetCategoryNews(@"SELECT id from news where title_bn!='' order by  date desc limit 0,5");
            IList<bdmirrorDB.news> NEWZ1 = getIlistNews(latestNews);
            foreach (bdmirrorDB.news nz in NEWZ1)
            {
                bdmirrorDB.images img = get_img(nz);
                dtTOp1.Rows.Add(new string[] { nz.title_en, nz.title_bn, nz.news_id.ToString(), nz.category_id.name_en, nz.category_id.name_bn, img.link, img.caption_en, nz.content_en, nz.content_bn });

            }

            rptLatestRight.DataSource = dtTOp1;
            rptLatestRight.DataBind();
            string query =(language=="en")? @"select news.id from news where published='1' and title_en!=''  ORDER BY mostread desc LIMIT 0,10":"select news.id from news where published='1' and title_bn!=''  ORDER BY mostread desc LIMIT 0,10";
            DataTable dtTOp = new DataTable("dssmostread");
            dtTOp.Columns.Add("title_en", typeof(string));
            dtTOp.Columns.Add("title_bn", typeof(string));
            dtTOp.Columns.Add("news_id", typeof(string));
            dtTOp.Columns.Add("name_en", typeof(string));
            dtTOp.Columns.Add("name_bn", typeof(string));
            dtTOp.Columns.Add("content_en", typeof(string));
            dtTOp.Columns.Add("content_bn", typeof(string));
            dtTOp.Columns.Add("img_url", typeof(string));
            dtTOp.Columns.Add("img_text", typeof(string));
            IList<int> ids = ndao.GetCategoryNews(query);
            IList<bdmirrorDB.news> NEWZ = getIlistNews(ids);
            foreach (bdmirrorDB.news nz in NEWZ)
            {
                bdmirrorDB.images img = get_img(nz);
                dtTOp.Rows.Add(new string[] { nz.title_en, nz.title_bn, nz.news_id.ToString(), nz.category_id.name_en, nz.category_id.name_bn, nz.content_en, nz.content_bn, img.link, img.caption_en });
            }
            rptmostread.DataSource = dtTOp;
            rptmostread.DataBind();
            DataTable dtTOp2 = new DataTable("dsstopnews");
            dtTOp2.Columns.Add("title_en", typeof(string));
            dtTOp2.Columns.Add("title_bn", typeof(string));
            dtTOp2.Columns.Add("news_id", typeof(string));
            dtTOp2.Columns.Add("name_en", typeof(string));
            dtTOp2.Columns.Add("name_bn", typeof(string));
            dtTOp2.Columns.Add("img_url", typeof(string));
            dtTOp2.Columns.Add("img_text", typeof(string));
            dtTOp2.Columns.Add("content_en", typeof(string));
            dtTOp2.Columns.Add("content_bn", typeof(string));
            int topcount = 0;

            IList<int> res = ndao.getMainNews(language);
            IList<bdmirrorDB.news> news_list = getIlistNews(res);
            foreach (bdmirrorDB.news nz in news_list)
            {
                topcount++;
                if (topcount > 5) break;
                bdmirrorDB.images img = get_img(nz);
                dtTOp2.Rows.Add(new string[] { nz.title_en, nz.title_bn, nz.news_id.ToString(), nz.category_id.name_en, nz.category_id.name_bn, img.link, img.caption_en, nz.content_en, nz.content_bn });

            }
            rptTopNews.DataSource = dtTOp2;
            rptTopNews.DataBind();
        }

        protected void rptmostread_bound(object sender, RepeaterItemEventArgs e)
        {
            bool is_english = (language == "en") ? true : false;

            HyperLink toplink = (HyperLink)e.Item.FindControl("hpltoptitle");
            if (toplink != null)
            {
                // bool is_english = (language == "en") ? true : false;
                if (is_english)
                {
                    toplink.NavigateUrl = "news/" + language + "/" + DataBinder.Eval(e.Item.DataItem, "name_en").ToString() + "/" + DataBinder.Eval(e.Item.DataItem, "news_id").ToString() + "/" + utiob.replaceSpace(DataBinder.Eval(e.Item.DataItem, "title_en").ToString());
                }
                else
                {
                    toplink.NavigateUrl = "news/" + language + "/" + DataBinder.Eval(e.Item.DataItem, "name_bn").ToString() + "/" + DataBinder.Eval(e.Item.DataItem, "news_id").ToString() + "/" + utiob.replaceSpace(DataBinder.Eval(e.Item.DataItem, "title_bn").ToString());
                }

                toplink.Text = (language == "bn") ? DataBinder.Eval(e.Item.DataItem, "title_bn").ToString()
                                   : DataBinder.Eval(e.Item.DataItem, "title_en").ToString();
            }

            Label lblcontent = (Label)e.Item.FindControl("lblcontent");
            if (lblcontent != null)
            {
                lblcontent.Text = (language == "bn") ? DataBinder.Eval(e.Item.DataItem, "content_bn").ToString() : DataBinder.Eval(e.Item.DataItem, "content_en").ToString();
            }
        }

        protected void rpttop_bound(object sender, RepeaterItemEventArgs e)
        {
            bool is_english = (language == "en") ? true : false;

            HyperLink toplink = (HyperLink)e.Item.FindControl("hpltoptitle");
            if (toplink != null)
            {
                // bool is_english = (language == "en") ? true : false;
                if (is_english)
                {
                    toplink.NavigateUrl = "news/" + language + "/" + DataBinder.Eval(e.Item.DataItem, "name_en").ToString() + "/" + DataBinder.Eval(e.Item.DataItem, "news_id").ToString() + "/" + utiob.replaceSpace(DataBinder.Eval(e.Item.DataItem, "title_en").ToString());
                }
                else
                {
                    toplink.NavigateUrl = "news/" + language + "/" + DataBinder.Eval(e.Item.DataItem, "name_bn").ToString() + "/" + DataBinder.Eval(e.Item.DataItem, "news_id").ToString() + "/" + utiob.replaceSpace(DataBinder.Eval(e.Item.DataItem, "title_bn").ToString());
                }

                toplink.Text = (language == "bn") ? DataBinder.Eval(e.Item.DataItem, "title_bn").ToString()
                                   : DataBinder.Eval(e.Item.DataItem, "title_en").ToString();
            }

            Label lblcontent = (Label)e.Item.FindControl("lblcontent");
            if (lblcontent != null)
            {
                lblcontent.Text = (language == "bn") ? DataBinder.Eval(e.Item.DataItem, "content_bn").ToString() : DataBinder.Eval(e.Item.DataItem, "content_en").ToString();
            }
        }
#endregion
        //[WebMethod]
        //public static string GetNews(int pageIndex)
        //{
        //    return loadRepeater(start);
        //}
    }
}