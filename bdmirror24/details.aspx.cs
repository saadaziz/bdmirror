﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.DAL_Functions.Implementations;
using bdmirror24.DAL.Manager;
using bdmirror24.DAL.mappings;
using bdmirror24;
using bdmirror24.DAL.DAL_Functions.Implementations;
using System.Web.UI.HtmlControls;
using System.Data;
using bdmirror24.couchbase;
namespace bdmirror24
{
    public partial class details : System.Web.UI.Page
    {
        private static int i;
        private static string language;
        NewsDAO ndao = new NewsDAO();
        protected IList<bdmirrorDB.news> getIlistNews(IList<int> res)
        {
            //IList<int> res = ndao.getHomeNews("en");
            couchbasemain cbm = new couchbasemain();
            IList<bdmirrorDB.news> news_list = new List<bdmirrorDB.news>();

            foreach (int id in res)
            {
                bdmirrorDB.news news = cbm.getNews(id);
                if (news == null)
                {
                    news = ndao.pickbyID(id); cbm.setnews(news);

                }
                news_list.Add(news);
            }
            return news_list;
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                i = 0;
                if (Page.RouteData.Values["category"] != null)
                    lblcat.Text = (Server.UrlDecode(Page.RouteData.Values["category"].ToString()));
                if (Page.RouteData.Values["id"] != null && Page.RouteData.Values["lang"] != null)
                {
                    language = Page.RouteData.Values["lang"].ToString();
                    try
                    {
                        loadControl(int.Parse(Page.RouteData.Values["id"].ToString()), language);

                    }
                    catch (Exception ex)
                    {

                    }
                    load_allrepeaters();
                    //hpltitle.Text=(Server.UrlDecode(Page.RouteData.Values["id"].ToString()));
                }
                //if (Page.RouteData.Values["title"] != null)
                //Response.Write(Server.UrlDecode(Page.RouteData.Values["title"].ToString()));
                if (rptimg.Items.Count < 1)
                {
                    pnlimage.Visible = false;
                    pnlcontents.CssClass = "toppadding10";
                }
            }
        }

        private bdmirrorDB.categories cat = new bdmirrorDB.categories();
        protected void loadControl(int newsid, string lang)
        {
            if (newsid != 0)
            {
                

                cat = new CategoryDAO().getcatbyName(Server.UrlDecode(Page.RouteData.Values["category"].ToString()));
                bdmirrorDB.news news = new NewsDAO().pickbyID(newsid);

                


                hpltitle.Text = (lang.Equals("en")) ? news.title_en : news.title_bn;
                bdmirrorDB.contents cont = new ContentDAO().pickbyID(news.contents.content_id);
                bool published = true;
                if (news.published != "1") 
                    published = false;
                Page.Title = (language != "bn") ? news.title_en : news.title_bn;
                HtmlMeta tag = new HtmlMeta();
                tag.Attributes.Add("property", "og:title");
                tag.Content = (language != "bn") ? news.title_en+" :: Bdmirror24.com" : news.title_bn+"বিডিমিরর.com";
                Page.Header.Controls.Add(tag);
                HtmlMeta tag2 = new HtmlMeta();
                tag2.Attributes.Add("property", "og:image");
                tag2.Content = new ImageDAO().GetImagebyNewsID(news).link;
                Page.Header.Controls.Add(tag2);
                //Page.Title += (!published) ? " -- Not Published Yet" : "";
                string keywords = ((language != "bn") ? cat.name_en : cat.name_bn) + ",";
                if (language != "bn")
                {
                    IList<bdmirrorDB.keywords> keys = new KeywrdsDAO().GetKeywordsbyNewsID(news);
                    foreach (bdmirrorDB.keywords keywordse in keys)
                    {
                        keywords += keywordse.keyword + ",";
                    }
                }
                else
                {
                    IList<bdmirrorDB.keywords_bn> keys = new KeywrdsbnDAO().GetKeywordsbyNewsID(news);
                    foreach (bdmirrorDB.keywords_bn keywordse in keys)
                    {
                        keywords += keywordse.keyword + ",";
                    }
                }
                Page.MetaKeywords = keywords;
                lblcontent.Text = (!published) ? "This News is Not Published yet<br/>" : "";
                string content = (lang.Equals("en")) ?
                    System.Text.RegularExpressions.Regex.Replace(cont.english_content, "style", "") : System.Text.RegularExpressions.Regex.Replace(cont.bangla_content, "style", "");
                lblcontent.Text +=  content;

                lblwriter.Text = (news.writer_name == null) ? "Anonymous writer".ToString() : news.writer_name;
                lbldistrict.Text = (news.district_id == null)
                                       ? "N/A".ToString()
                                       : news.district_id.DistrictName.ToString();

                lblcat.Text = (lang.Equals("en")) ? news.category_id.name_en : news.category_id.name_bn;
                hplcountry.Text = (news.country_id != null)
                                      ? new CountryDAO().pickbyID(news.country_id.country_id).country_name
                                      : "N/A".ToString();
                dssimg.SelectCommand = @"select image_name,caption_en,caption_bn,link,height,width
                                        from images
                                        where news_id=" + newsid;
                rptimg.DataBind();
                

                if (news.date.ToString() != null)
                {
                    string[] date = news.date.ToString().Split(' ')[0].Split('/');
                    //string[] date = all[0].Split('/');
                    hpltime.Text = date[0] + "-";
                    //string strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(int.Parse(date[1]));
                    hpltime.Text += date[1];
                    hpltime.Text += "-" + date[2];
                    hpltime.NavigateUrl = "Date/" + language + "/" + date[0] + "-" + date[1] + "-" + date[2] + "/";

                }
                new NewsDAO().increaseread(news.news_id);


                
            }

        }


        #region repeater
        protected void rptimg_bound(object sender, RepeaterItemEventArgs e)
        {
            //if (rptimg.Items.Count < 1)
            //{
            //    if (e.Item.ItemType == ListItemType.Footer)
            //    {
            //        Label lblFooter = (Label)e.Item.FindControl("lblEmptyData");
            //        if (lblFooter != null) lblFooter.Visible = true;
            //    }
            //}
            //else
            //{
            Label caption = (Label)e.Item.FindControl("lblcap");
            if (caption != null)
                caption.Text = (language == "bn") ? DataBinder.Eval(e.Item.DataItem, "caption_bn").ToString()
                    : DataBinder.Eval(e.Item.DataItem, "caption_en").ToString();
            Panel pnl = (Panel)e.Item.FindControl("pnlmain");
            if (pnl != null)
                pnl.CssClass = (i == 0) ? "active item" : "item";
            Image img = (Image)e.Item.FindControl("img");
            if (img != null)
            {
                int Size = 450;
                img.ImageUrl = DataBinder.Eval(e.Item.DataItem, "link").ToString();
                img.AlternateText = DataBinder.Eval(e.Item.DataItem, "image_name").ToString();
                double ht = double.Parse(DataBinder.Eval(e.Item.DataItem, "height").ToString());
                double wt = double.Parse(DataBinder.Eval(e.Item.DataItem, "width").ToString());

                ht = new bdmirror24.imageHandling().returnHeightWidth(ht, wt, Size);

                img.Width = Size;
                Int32 hei = Convert.ToInt32(ht);
                img.Height = (hei);

            }

            i++;
            //}

        }
        Utility utiob = new Utility();
        
        #endregion
        #region other repeater
        ImageDAO imgdao = new ImageDAO();
        private bdmirrorDB.images get_img(bdmirrorDB.news nz)
        {
            bdmirrorDB.images img = imgdao.GetImagebyNewsID(nz);
            if (img == null)
            {
                img = new bdmirrorDB.images();
                img.link = "";
                img.caption_en = "";
            }
            return img;
        }
        public void load_allrepeaters()
        {
            DataTable dtTOp1 = new DataTable("dsslatest2");
            dtTOp1.Columns.Add("title_en", typeof(string));
            dtTOp1.Columns.Add("title_bn", typeof(string));
            dtTOp1.Columns.Add("news_id", typeof(string));
            dtTOp1.Columns.Add("name_en", typeof(string));
            dtTOp1.Columns.Add("name_bn", typeof(string));
            dtTOp1.Columns.Add("img_url", typeof(string));
            dtTOp1.Columns.Add("img_text", typeof(string));
            dtTOp1.Columns.Add("content_en", typeof(string));
            dtTOp1.Columns.Add("content_bn", typeof(string));
            //IList<bdmirrorDB.categories> allcat = new CategoryDAO().All();

            IList<int> latestNews = (language == "bn") ?
                ndao.GetCategoryNews(@"SELECT id from news where title_en!='' order by  date desc limit 0,5") :
                ndao.GetCategoryNews(@"SELECT id from news where title_bn!='' order by  date desc limit 0,5");
            IList<bdmirrorDB.news> NEWZ1 = getIlistNews(latestNews);
            foreach (bdmirrorDB.news nz in NEWZ1)
            {
                bdmirrorDB.images img = get_img(nz);
                dtTOp1.Rows.Add(new string[] { nz.title_en, nz.title_bn, nz.news_id.ToString(), nz.category_id.name_en, nz.category_id.name_bn, img.link, img.caption_en, nz.content_en, nz.content_bn });

            }

            rptLatestRight.DataSource = dtTOp1;
            rptLatestRight.DataBind();
            string query =(language=="en")? @"select news.id from news where published='1' and title_en!=''  ORDER BY mostread desc LIMIT 0,10":"select news.id from news where published='1' and title_bn!=''  ORDER BY mostread desc LIMIT 0,10";
            DataTable dtTOp = new DataTable("dssmostread");
            dtTOp.Columns.Add("title_en", typeof(string));
            dtTOp.Columns.Add("title_bn", typeof(string));
            dtTOp.Columns.Add("news_id", typeof(string));
            dtTOp.Columns.Add("name_en", typeof(string));
            dtTOp.Columns.Add("name_bn", typeof(string));
            dtTOp.Columns.Add("content_en", typeof(string));
            dtTOp.Columns.Add("content_bn", typeof(string));
            dtTOp.Columns.Add("img_url", typeof(string));
            dtTOp.Columns.Add("img_text", typeof(string));
            IList<int> ids = ndao.GetCategoryNews(query);
            IList<bdmirrorDB.news> NEWZ = getIlistNews(ids);
            foreach (bdmirrorDB.news nz in NEWZ)
            {
                bdmirrorDB.images img = get_img(nz);
                dtTOp.Rows.Add(new string[] { nz.title_en, nz.title_bn, nz.news_id.ToString(), nz.category_id.name_en, nz.category_id.name_bn, nz.content_en, nz.content_bn, img.link, img.caption_en });
            }
            rptmostread.DataSource = dtTOp;
            rptmostread.DataBind();
            DataTable dtTOp2 = new DataTable("dsstopnews");
            dtTOp2.Columns.Add("title_en", typeof(string));
            dtTOp2.Columns.Add("title_bn", typeof(string));
            dtTOp2.Columns.Add("news_id", typeof(string));
            dtTOp2.Columns.Add("name_en", typeof(string));
            dtTOp2.Columns.Add("name_bn", typeof(string));
            dtTOp2.Columns.Add("img_url", typeof(string));
            dtTOp2.Columns.Add("img_text", typeof(string));
            dtTOp2.Columns.Add("content_en", typeof(string));
            dtTOp2.Columns.Add("content_bn", typeof(string));
            int topcount = 0;

            IList<int> res = ndao.getMainNews(language);
            IList<bdmirrorDB.news> news_list = getIlistNews(res);
            foreach (bdmirrorDB.news nz in news_list)
            {
                topcount++;
                if (topcount > 5) break;
                bdmirrorDB.images img = get_img(nz);
                dtTOp2.Rows.Add(new string[] { nz.title_en, nz.title_bn, nz.news_id.ToString(), nz.category_id.name_en, nz.category_id.name_bn, img.link, img.caption_en, nz.content_en, nz.content_bn });

            }
            rptTopNews.DataSource = dtTOp2;
            rptTopNews.DataBind();
        }

        protected void rptmostread_bound(object sender, RepeaterItemEventArgs e)
        {
            bool is_english = (language == "en") ? true : false;

            HyperLink toplink = (HyperLink)e.Item.FindControl("hpltoptitle");
            if (toplink != null)
            {
                // bool is_english = (language == "en") ? true : false;
                if (is_english)
                {
                    toplink.NavigateUrl = "news/" + language + "/" + DataBinder.Eval(e.Item.DataItem, "name_en").ToString() + "/" + DataBinder.Eval(e.Item.DataItem, "news_id").ToString() + "/" + utiob.replaceSpace(DataBinder.Eval(e.Item.DataItem, "title_en").ToString());
                }
                else
                {
                    toplink.NavigateUrl = "news/" + language + "/" + DataBinder.Eval(e.Item.DataItem, "name_bn").ToString() + "/" + DataBinder.Eval(e.Item.DataItem, "news_id").ToString() + "/" + utiob.replaceSpace(DataBinder.Eval(e.Item.DataItem, "title_bn").ToString());
                }

                toplink.Text = (language == "bn") ? DataBinder.Eval(e.Item.DataItem, "title_bn").ToString()
                                   : DataBinder.Eval(e.Item.DataItem, "title_en").ToString();
            }

            Label lblcontent = (Label)e.Item.FindControl("lblcontent");
            if (lblcontent != null)
            {
                lblcontent.Text = (language == "bn") ? DataBinder.Eval(e.Item.DataItem, "content_bn").ToString() : DataBinder.Eval(e.Item.DataItem, "content_en").ToString();
            }
        }

        protected void rpttop_bound(object sender, RepeaterItemEventArgs e)
        {
            bool is_english = (language == "en") ? true : false;

            HyperLink toplink = (HyperLink)e.Item.FindControl("hpltoptitle");
            if (toplink != null)
            {
                // bool is_english = (language == "en") ? true : false;
                if (is_english)
                {
                    toplink.NavigateUrl = "news/" + language + "/" + DataBinder.Eval(e.Item.DataItem, "name_en").ToString() + "/" + DataBinder.Eval(e.Item.DataItem, "news_id").ToString() + "/" + utiob.replaceSpace(DataBinder.Eval(e.Item.DataItem, "title_en").ToString());
                }
                else
                {
                    toplink.NavigateUrl = "news/" + language + "/" + DataBinder.Eval(e.Item.DataItem, "name_bn").ToString() + "/" + DataBinder.Eval(e.Item.DataItem, "news_id").ToString() + "/" + utiob.replaceSpace(DataBinder.Eval(e.Item.DataItem, "title_bn").ToString());
                }

                toplink.Text = (language == "bn") ? DataBinder.Eval(e.Item.DataItem, "title_bn").ToString()
                                   : DataBinder.Eval(e.Item.DataItem, "title_en").ToString();
            }

            Label lblcontent = (Label)e.Item.FindControl("lblcontent");
            if (lblcontent != null)
            {
                lblcontent.Text = (language == "bn") ? DataBinder.Eval(e.Item.DataItem, "content_bn").ToString() : DataBinder.Eval(e.Item.DataItem, "content_en").ToString();
            }
        }
        #endregion
    }
}