﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
using bdmirror24.couchbase;
using System.Data;

namespace bdmirror24
{
    public partial class current : System.Web.UI.Page
    {

        private static string language;

        NewsDAO ndao = new NewsDAO();
        protected IList<bdmirrorDB.news> getIlistNews(IList<int> res)
        {
            //IList<int> res = ndao.getHomeNews("en");
            couchbasemain cbm = new couchbasemain();
            IList<bdmirrorDB.news> news_list = new List<bdmirrorDB.news>();

            foreach (int id in res)
            {
                bdmirrorDB.news news = cbm.getNews(id);
                if (news == null)
                {
                    news = ndao.pickbyID(id); cbm.setnews(news);

                }
                news_list.Add(news);
            }
            return news_list;
        }
        #region other repeater
        ImageDAO imgdao = new ImageDAO();
        private bdmirrorDB.images get_img(bdmirrorDB.news nz)
        {
            bdmirrorDB.images img = imgdao.GetImagebyNewsID(nz);
            if (img == null)
            {
                img = new bdmirrorDB.images();
                img.link = "";
                img.caption_en = "";
            }
            return img;
        }
        public void load_allrepeaters()
        {
            DataTable dtTOp1 = new DataTable("dsslatest2");
            dtTOp1.Columns.Add("title_en", typeof(string));
            dtTOp1.Columns.Add("title_bn", typeof(string));
            dtTOp1.Columns.Add("news_id", typeof(string));
            dtTOp1.Columns.Add("name_en", typeof(string));
            dtTOp1.Columns.Add("name_bn", typeof(string));
            dtTOp1.Columns.Add("img_url", typeof(string));
            dtTOp1.Columns.Add("img_text", typeof(string));
            dtTOp1.Columns.Add("content_en", typeof(string));
            dtTOp1.Columns.Add("content_bn", typeof(string));
            //IList<bdmirrorDB.categories> allcat = new CategoryDAO().All();

            IList<int> latestNews = (language == "bn") ?
                ndao.GetCategoryNews(@"SELECT id from news where title_en!='' order by  date desc limit 0,5") :
                ndao.GetCategoryNews(@"SELECT id from news where title_bn!='' order by  date desc limit 0,5");
            IList<bdmirrorDB.news> NEWZ1 = getIlistNews(latestNews);
            foreach (bdmirrorDB.news nz in NEWZ1)
            {
                bdmirrorDB.images img = get_img(nz);
                dtTOp1.Rows.Add(new string[] { nz.title_en, nz.title_bn, nz.news_id.ToString(), nz.category_id.name_en, nz.category_id.name_bn, img.link, img.caption_en, nz.content_en, nz.content_bn });

            }

            rptLatestRight.DataSource = dtTOp1;
            rptLatestRight.DataBind();
            string query =(language=="en")? @"select news.id from news where published='1' and title_en!=''  ORDER BY mostread desc LIMIT 0,10":"select news.id from news where published='1' and title_bn!=''  ORDER BY mostread desc LIMIT 0,10";
            DataTable dtTOp = new DataTable("dssmostread");
            dtTOp.Columns.Add("title_en", typeof(string));
            dtTOp.Columns.Add("title_bn", typeof(string));
            dtTOp.Columns.Add("news_id", typeof(string));
            dtTOp.Columns.Add("name_en", typeof(string));
            dtTOp.Columns.Add("name_bn", typeof(string));
            dtTOp.Columns.Add("content_en", typeof(string));
            dtTOp.Columns.Add("content_bn", typeof(string));
            dtTOp.Columns.Add("img_url", typeof(string));
            dtTOp.Columns.Add("img_text", typeof(string));
            IList<int> ids = ndao.GetCategoryNews(query);
            IList<bdmirrorDB.news> NEWZ = getIlistNews(ids);
            foreach (bdmirrorDB.news nz in NEWZ)
            {
                bdmirrorDB.images img = get_img(nz);
                dtTOp.Rows.Add(new string[] { nz.title_en, nz.title_bn, nz.news_id.ToString(), nz.category_id.name_en, nz.category_id.name_bn, nz.content_en, nz.content_bn, img.link, img.caption_en });
            }
            rptmostread.DataSource = dtTOp;
            rptmostread.DataBind();
            DataTable dtTOp2 = new DataTable("dsstopnews");
            dtTOp2.Columns.Add("title_en", typeof(string));
            dtTOp2.Columns.Add("title_bn", typeof(string));
            dtTOp2.Columns.Add("news_id", typeof(string));
            dtTOp2.Columns.Add("name_en", typeof(string));
            dtTOp2.Columns.Add("name_bn", typeof(string));
            dtTOp2.Columns.Add("img_url", typeof(string));
            dtTOp2.Columns.Add("img_text", typeof(string));
            dtTOp2.Columns.Add("content_en", typeof(string));
            dtTOp2.Columns.Add("content_bn", typeof(string));
            int topcount = 0;

            IList<int> res = ndao.getMainNews(language);
            IList<bdmirrorDB.news> news_list = getIlistNews(res);
            foreach (bdmirrorDB.news nz in news_list)
            {
                topcount++;
                if (topcount > 5) break;
                bdmirrorDB.images img = get_img(nz);
                dtTOp2.Rows.Add(new string[] { nz.title_en, nz.title_bn, nz.news_id.ToString(), nz.category_id.name_en, nz.category_id.name_bn, img.link, img.caption_en, nz.content_en, nz.content_bn });

            }
            rptTopNews.DataSource = dtTOp2;
            rptTopNews.DataBind();
        }
        Utility utiob = new Utility();
        protected void rptmostread_bound(object sender, RepeaterItemEventArgs e)
        {
            bool is_english = (language == "en") ? true : false;

            HyperLink toplink = (HyperLink)e.Item.FindControl("hpltoptitle");
            if (toplink != null)
            {
                // bool is_english = (language == "en") ? true : false;
                if (is_english)
                {
                    toplink.NavigateUrl = "news/" + language + "/" + DataBinder.Eval(e.Item.DataItem, "name_en").ToString() + "/" + DataBinder.Eval(e.Item.DataItem, "news_id").ToString() + "/" + utiob.replaceSpace(DataBinder.Eval(e.Item.DataItem, "title_en").ToString());
                }
                else
                {
                    toplink.NavigateUrl = "news/" + language + "/" + DataBinder.Eval(e.Item.DataItem, "name_bn").ToString() + "/" + DataBinder.Eval(e.Item.DataItem, "news_id").ToString() + "/" + utiob.replaceSpace(DataBinder.Eval(e.Item.DataItem, "title_bn").ToString());
                }

                toplink.Text = (language == "bn") ? DataBinder.Eval(e.Item.DataItem, "title_bn").ToString()
                                   : DataBinder.Eval(e.Item.DataItem, "title_en").ToString();
            }

            Label lblcontent = (Label)e.Item.FindControl("lblcontent");
            if (lblcontent != null)
            {
                lblcontent.Text = (language == "bn") ? DataBinder.Eval(e.Item.DataItem, "content_bn").ToString() : DataBinder.Eval(e.Item.DataItem, "content_en").ToString();
            }
        }

        protected void rpttop_bound(object sender, RepeaterItemEventArgs e)
        {
            bool is_english = (language == "en") ? true : false;

            HyperLink toplink = (HyperLink)e.Item.FindControl("hpltoptitle");
            if (toplink != null)
            {
                // bool is_english = (language == "en") ? true : false;
                if (is_english)
                {
                    toplink.NavigateUrl = "news/" + language + "/" + DataBinder.Eval(e.Item.DataItem, "name_en").ToString() + "/" + DataBinder.Eval(e.Item.DataItem, "news_id").ToString() + "/" + utiob.replaceSpace(DataBinder.Eval(e.Item.DataItem, "title_en").ToString());
                }
                else
                {
                    toplink.NavigateUrl = "news/" + language + "/" + DataBinder.Eval(e.Item.DataItem, "name_bn").ToString() + "/" + DataBinder.Eval(e.Item.DataItem, "news_id").ToString() + "/" + utiob.replaceSpace(DataBinder.Eval(e.Item.DataItem, "title_bn").ToString());
                }

                toplink.Text = (language == "bn") ? DataBinder.Eval(e.Item.DataItem, "title_bn").ToString()
                                   : DataBinder.Eval(e.Item.DataItem, "title_en").ToString();
            }

            Label lblcontent = (Label)e.Item.FindControl("lblcontent");
            if (lblcontent != null)
            {
                lblcontent.Text = (language == "bn") ? DataBinder.Eval(e.Item.DataItem, "content_bn").ToString() : DataBinder.Eval(e.Item.DataItem, "content_en").ToString();
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            load_allrepeaters();
            if (Page.RouteData.Values["lang"] == "en")
                language = "en";
            else
                language = "bn";
        }
        protected void grvNews_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvNews.PageIndex = e.NewPageIndex;
            grvNews.DataBind();
        }
    }
}