﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using bdmirror24.DAL.mappings;
using MySql.Data.MySqlClient;

namespace bdmirror24.DAL
{
    public class Connection
    {
        private ISessionFactory _sessionFactory = null;

        private ISessionFactory SessionFactory
        {
            get
            {
                if (_sessionFactory == null)
                    InitializeSessionFactory();

                return _sessionFactory;
            }
        }

        public void CloseSession()
        {
            if (_sessionFactory != null)
            {
                if (!_sessionFactory.IsClosed)
                {
                    _sessionFactory.Close();
                    _sessionFactory.Dispose();
                }
            }
        }

        public ISession OpenSession()
        {
            return SessionFactory.OpenSession();
        }

        //private static AutoPersistenceModel CreateMappings()
        //{
        //    return AutoMap
        //        .Assembly(System.Reflection.Assembly.GetCallingAssembly())
        //        .Where(t => t.Namespace == "SimpleOrmApplication");
        //}
        private void InitializeSessionFactory()
        {
            string connectionString =
                "Server=tasnia-pc;Port=3306;Database=db_news;Uid=saad;Pwd=123qweasd; Allow Zero Datetime=true;Convert Zero Datetime=true;charSet=utf8";

            //AutoPersistenceModel model = CreateMappings();
            _sessionFactory = Fluently.Configure()
                                      .Database(MySQLConfiguration.Standard
                                      .ConnectionString(connectionString))
                                      .Mappings(m => m.FluentMappings.AddFromAssemblyOf<bdmirrorDB>())
                                      
                //.ExposeConfiguration(BuildSchema)
                //.ExposeConfiguration(cfg => new SchemaExport(cfg).Create(true, true))
                                      .BuildSessionFactory();
        }

        //private void BuildSchema(Configuration config)
        //{
        //    new SchemaExport(config).Create(false, true);
        //}
    }
}
