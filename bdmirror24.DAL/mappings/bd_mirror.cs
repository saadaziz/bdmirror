﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bdmirror24.DAL.mappings
{
    public class bdmirrorDB
    {
         [Serializable]
        public class categories
        {
            public virtual Int64 cat_id { get; set; }
            public virtual string name_bn { get; set; }
            public virtual string name_en { get; set; }

        }
        [Serializable]
        public class news
        {
            public virtual Int64 news_id { get; set; }
            public virtual string title_bn { get; set; }
            public virtual string title_en { get; set; }
            public virtual DateTime date { get; set; }
            public virtual string top_news { get; set; }
            public virtual string latest { get; set; }
            public virtual string published { get; set; }
            public virtual string news_link { get; set; }
            public virtual DateTime insert_time { get; set; }
            public virtual categories category_id { get; set; }
            public virtual admcountry country_id { get; set; }
            public virtual admdistrict district_id { get; set; }
            public virtual user writer { get; set; }
            public virtual string content_en { get; set; }
            public virtual string content_bn { get; set; }
            public virtual contents contents { get; set; }
            public virtual string anonymous { get; set; }
            public virtual string writer_name { get; set; }
            public virtual int mostread { get; set; }
        }
         [Serializable]
        public class contents
        {
            public virtual Int64 content_id { get; set; }
            public virtual string bangla_content { get; set; }
            public virtual string english_content { get; set; }
            public virtual news news_id { get; set; }

        }
         [Serializable]
        public class comments
        {
            public virtual Int64 comment_id { get; set; }
            public virtual news news_id { get; set; }
            public virtual string content { get; set; }
            public virtual string email_address { get; set; }
            public virtual DateTime com_time { get; set; }
            public virtual bool is_verified { get; set; }
        }
         [Serializable]
        public class images
        {
            public virtual Int64 image_id { get; set; }
            public virtual news news_id { get; set; }
            public virtual string caption_bn { get; set; }
            public virtual string image_name { get; set; }
            public virtual string caption_en { get; set; }
            public virtual string link { get; set; }
            public virtual DateTime time { get; set; }
            public virtual double height { get; set; }
            public virtual double width { get; set; }
        }
         [Serializable]
        public class advertisement_category
        {
            public virtual Int64 add_cat_id { get; set; }
            public virtual string add_cat_name { get; set; }
            public virtual string add_type { get; set; }
            public virtual string add_position { get; set; }
        }
         [Serializable]
        public class advertisements
        {
            public virtual Int64 add_id { get; set; }
            public virtual advertisement_category add_cat_id { get; set; }
            public virtual string adv_name { get; set; }
            public virtual string adv_link { get; set; }
            public virtual DateTime insert_time { get; set; }
            public virtual Int64 sort { get; set; }
        }
         [Serializable]
        public class address
        {
            public virtual Int64 address_id { get; set; }
            public virtual string street { get; set; }
            public virtual string district { get; set; }
            public virtual string division { get; set; }
        }
         [Serializable]
        public class user
        {
            public virtual Int64 user_id { get; set; }
            public virtual string first_name { get; set; }
            public virtual string last_name { get; set; }
            public virtual string email { get; set; }
            public virtual string password { get; set; }
            public virtual string contact_no { get; set; }
            public virtual char gender { get; set; }
            public virtual string national_id_no { get; set; }
            public virtual Int64 national_id_image { get; set; }
            public virtual string educational_qualification { get; set; }
            public virtual string occupation { get; set; }
            public virtual string writing_experience { get; set; }
            public virtual address present_addr { get; set; }
            public virtual address permanent_addr { get; set; }
        }
         [Serializable]
        public class admcountry
        {
            public virtual Int64 country_id { get; set; }
            public virtual string country_name { get; set; }
            public virtual string country_code { get; set; }
            public virtual string ISDcode { get; set; }

        }
         [Serializable]
        public class admdivision
        {
            public virtual Int64 division_id { get; set; }
            public virtual string divCode { get; set; }
            public virtual string divName { get; set; }
            public virtual string divCodeName { get; set; }
            public virtual admcountry countryid { get; set; }

        }
         [Serializable]
        public class admdistrict
        {
            
            public virtual Int64 DistrictID { get; set; }
            public virtual string DistrictCode { get; set; }
            public virtual string DistrictName { get; set; }
            public virtual string DistrictCodeName { get; set; }
            public virtual admdivision DivisionID { get; set; }
        }
         [Serializable]
        public class admupazilaps
        {
            public virtual Int64 UpazilaPSID { get; set; }
            public virtual string UpazilaPSCode { get; set; }
            public virtual string UpazilaPSName { get; set; }
            public virtual admdistrict DistrictID { get; set; }
        }
         [Serializable]
        public class keywords
        {
            public virtual Int64 keyword_id { get; set; }
            public virtual string keyword { get; set; }
            public virtual news news_id { get; set; }
        }
         [Serializable]
        public class keywords_bn
        {
            public virtual Int64 keyword_id { get; set; }
            public virtual string keyword { get; set; }
            public virtual news news_id { get; set; }
        }
         [Serializable]
        public class current_affairs
        {
            public virtual Int64 current_id { get; set; }
            public virtual DateTime insert_date { get; set; }
            public virtual string content_en { get; set; }
            public virtual string content_bn { get; set; } 
        }
         [Serializable]
         public class all_add
         {
             public virtual Int64 add_id { get; set; }
             public virtual String title { get; set; }
             public virtual String image_link { get; set; }
             public virtual String image_url { get; set; }
             public virtual Int64 addorder { get; set; }

         }
         [Serializable]
         public class videos
         {
             public virtual Int64 video_id { get; set; }
             public virtual string caption_en { get; set; }
             public virtual string caption_bn { get; set; }
             public virtual string link { get; set; }
             public virtual DateTime time { get; set; }
         }
         
    }
}
