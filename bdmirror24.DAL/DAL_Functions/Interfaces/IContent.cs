﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Interfaces;
namespace bdmirror24.DAL.DAL_Functions.Interfaces
{
    public interface IContent : IRepo<bdmirrorDB.contents>
    {
        bdmirrorDB.contents GetContentbyNewsID(int newsid);
    }
}
