﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Interfaces;
namespace bdmirror24.DAL.DAL_Functions.Interfaces
{
    public interface IKeywordsbn:IRepo<bdmirrorDB.keywords_bn>
    {
        IList<bdmirrorDB.keywords_bn> GetKeywordsbyNewsID(long newsid);
    }
}
