﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Interfaces;
namespace bdmirror24.DAL.DAL_Functions.Interfaces
{
    public interface INews:IRepo<bdmirrorDB.news>
    {
        bdmirrorDB.news getbyID(int id);
        IList<int> getHomeNews(string language);
        bdmirrorDB.news getLastNewsbyCat(bdmirrorDB.categories cat);
        IList<bdmirrorDB.news> getAllNewsbyCountry(bdmirrorDB.admcountry country);
        IList<bdmirrorDB.news> getAllNewsbyCatName(bdmirrorDB.categories cat);
        IList<int> getMainNews(string language);
        void updatelatest(string myExecuteQuery); 
        IList<bdmirrorDB.news> GetLatestNews();
        IList<int> GetCategoryNews(string query);
        void increaseread(Int64 newsid);
    }
}
