﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate.Criterion;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Implementations;
using bdmirror24.DAL.DALFunctions.Interfaces;
namespace bdmirror24.DAL.DAL_Functions.Implementations
{
    public class userDAO : BaseDAORepository<bdmirrorDB.user>
    {
        public bdmirrorDB.user getuserbyname(string user_name)
        {
            return FindAll("email", user_name).First();
        }
    }
}
