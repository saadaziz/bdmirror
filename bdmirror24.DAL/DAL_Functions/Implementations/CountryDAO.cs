﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using bdmirror24.DAL.DALFunctions.Implementations;
using bdmirror24.DAL.mappings;

namespace bdmirror24.DAL.DAL_Functions.Implementations
{
    public class CountryDAO : BaseDAORepository<bdmirrorDB.admcountry>
    {
        public bdmirrorDB.admcountry getCountrybyName(string country_name)
        {
            bdmirrorDB.admcountry res = FindAll("country_name", country_name).First();
            return res;
        }
    }
}
