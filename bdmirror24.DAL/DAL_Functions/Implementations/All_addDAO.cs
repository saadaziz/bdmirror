﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Implementations;
using NHibernate.Criterion;
using NHibernate;

namespace bdmirror24.DAL.DAL_Functions.Implementations
{
    public class All_addDAO : BaseDAORepository<bdmirrorDB.all_add>
    {
        public IList<bdmirrorDB.all_add> getAddbyTitle(string image_title)
        {
            IList<bdmirrorDB.all_add> res = FindAll("title", image_title);
           // gunner_freelancerer.job_user_login res = null;

            using (ISession session = new Connection().OpenSession())
            {
                res = session
                            .CreateCriteria(typeof(bdmirrorDB.all_add))
                            .Add(Restrictions.Eq("title", image_title))
                            .AddOrder(Order.Asc("addorder"))
                            .List<bdmirrorDB.all_add>();
            
                return res;
            }
            return null;

        }
    }
}
