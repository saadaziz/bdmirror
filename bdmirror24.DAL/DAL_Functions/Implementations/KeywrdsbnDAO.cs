﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Implementations;
using bdmirror24.DAL.DALFunctions.Interfaces;
using NHibernate;
using NHibernate.Criterion;
namespace bdmirror24.DAL.DAL_Functions.Implementations
{
    public class KeywrdsbnDAO:BaseDAORepository<bdmirrorDB.keywords_bn>
    {
        public IList<bdmirrorDB.keywords_bn> GetKeywordsbyNewsID(bdmirrorDB.news newsid)
        {
            Connection nb = new Connection();
            IList<bdmirrorDB.keywords_bn> list = null;
            using (ISession session = nb.OpenSession())
            {
                session.BeginTransaction();
                list = session.CreateCriteria(typeof(bdmirrorDB.keywords_bn))
                                      .Add(Restrictions.Eq("news_id", newsid))
                                      .List<bdmirrorDB.keywords_bn>();

                session.Transaction.Commit();
            }
            nb.CloseSession();
            return list;
        }
    }
}
