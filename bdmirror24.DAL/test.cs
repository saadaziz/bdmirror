﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Implementations;
namespace bdmirror24.DAL
{
    public class test : BaseDAORepository<bdmirrorDB.categories>
    {
        public void insert_cat(string catname)
        {
            bdmirrorDB.categories cat = new bdmirrorDB.categories();
            cat.name_en = catname;
            Add(cat);
        }
        public bdmirrorDB.categories getcat_test(string col_name, string catname)
        {
            return FindAll(col_name, catname).First();
        }
    }
}
