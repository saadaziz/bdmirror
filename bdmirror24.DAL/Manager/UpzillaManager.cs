﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Implementations;

namespace bdmirror24.DAL.Manager
{
    public class UpzillaManager:BaseDAORepository<bdmirrorDB.admupazilaps>
    {
        public IList<bdmirrorDB.admupazilaps> GetAllUpzillaByDistrict(int Districtid)
        {
            return (Districtid > 0) ? FindAll("DistrictID", Districtid.ToString()) : null;
        }
        public IList<bdmirrorDB.admupazilaps> GetAllUpzilla()
        {
            return All();
        }
    }
}
