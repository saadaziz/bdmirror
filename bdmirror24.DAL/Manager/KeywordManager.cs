﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using bdmirror24.DAL.DAL_Functions.Implementations;
using bdmirror24.DAL.mappings;
namespace bdmirror24.DAL.Manager
{
    public class KeywordManager
    {
        public void deleteAllKeyword(int newsid)
        {
            KeywrdsDAO kdao = new KeywrdsDAO();
            KeywrdsbnDAO kdaobn = new KeywrdsbnDAO();
            IList<bdmirrorDB.keywords> key_en = kdao.GetKeywordsbyNewsID(new NewsDAO().pickbyID(newsid));
            foreach(bdmirrorDB.keywords tmp in key_en)
            {
                kdao.Delete(tmp);
            }

            IList<bdmirrorDB.keywords_bn> key_bn = kdaobn.GetKeywordsbyNewsID(new NewsDAO().pickbyID(newsid));
            foreach (bdmirrorDB.keywords_bn tmp in key_bn)
            {
                kdaobn.Delete(tmp);
            }
        }
    }
}
