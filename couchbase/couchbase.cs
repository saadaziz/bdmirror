﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Couchbase;
using Enyim.Caching.Memcached;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
namespace couchbase
{
    public class couchbase
    {
        public bdmirrorDB.news getsetNews(int id)
        {
            bdmirrorDB.news news = null;
            using (var client = new CouchbaseClient())
            {
                if ((news=client.Get<bdmirrorDB.news>(id.ToString())) == null)
                {
                   // Response.Write("There is no news with id :"+news.news_id);
                    news = new NewsDAO().pickbyID(id);
                    client.Store(StoreMode.Set,
                                 news.news_id.ToString(),
                                 news,
                                 TimeSpan.FromMinutes(1));
                }
                else
                {
                    //Response.Write(news.title_en);
                    client.Touch(id.ToString(), TimeSpan.FromMinutes(1));
                }
            }
            return news;
        }
    }
}
